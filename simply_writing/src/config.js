// @flow
import type {FbLoginData} from './modules/login/reducer'
import type {GetState} from './modules/index'

export const URL = {
    BASE: 'http://localhost:8060',
}

type DefaultHeaders = {
    Accept: string,
    'Content-Type': string,
}

type AdditionalHeaders = any


export const GetHeaders = function (getState: GetState, additional:AdditionalHeaders) {
    const defaultHeaders: DefaultHeaders = {
        Accept      : 'application/json',
        'Content-Type': 'application/json',
    }

    const login = getState().Login
    const byFb  = !(login.facebook.token == undefined || login.facebook.token == '')
    if (byFb) {
        return {
            ...defaultHeaders,
            ...FacebookHeaders(login.facebook),
            ...additional,
        }
    }

    return {
        ...defaultHeaders,
        ...additional,
    }
}

type FacebookHeadersList = {
    'Fb-Token'           : string,
    'Fb-User-Id'         : string,
    'Fb-Token-Expiration': string,
    'Fb-Permissions'     : string,
}

export const FacebookHeaders = function (data: FbLoginData): FacebookHeadersList {

    //TODO: do i need declined permitions?
    //TODO: Check that all required permitions granded

    return {
        'Fb-Token'           : data.token,
        'Fb-User-Id'         : data.userID,
        'Fb-Token-Expiration': data.expirationTime.toISOString(),
        'Fb-Permissions'     : data.permissions.join(','),
    }
}

