package main

import (
	"bigcore/fb_bot/common/mongo"
	"bigcore/fb_bot/config"
	"bigcore/fb_bot/handler"
	"bigcore/fb_bot/model"
	"fmt"
	"log"
	"reflect"

	"bigcore/fb_bot/common"

	"bigcore/fb_bot/dao"

	"net/http"

	"github.com/labstack/echo"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
)

type App struct {
	Config       *config.Config
	onCloseStack []func()

	Components Components
	Models     Models
	Dao        Dao
}

type Components struct {
	Collections *Collections
}

type Collections struct {
	Counter *mongo.Collection
	User    *mongo.Collection
	Club    *mongo.Collection
	Teacher *mongo.Collection
	Event   *mongo.Collection
}

type Models struct {
	User *model.User
}

type Dao struct {
	User *dao.User
}

func NewApp() *App {
	return &App{}
}

func (this *App) Init() error {
	this.Config = config.NewConfig()

	return common.UntilError(
		this.InitDb,
		this.InitClasses,
		//this.InitGrpc,
	)
}

func (this *App) NewUserHandler() *handler.User {
	return handler.NewUserHandler(nil)
}

func (this *App) InitClasses() error {

	//Dao
	this.Dao = Dao{}

	this.Dao.User = dao.NewUser()

	//Models
	this.Models = Models{}

	this.Models.User = model.NewUser()

	//Workers

	return common.UntilError(
		this.Dao.User.EnsureIndices,
		this.DevClean,
		this.DevFixtures,
	)
}

func (this *App) DevClean() error {
	err := this.Dao.User.Collection.DropCollection()
	if err != nil {
		return err
	}

	return nil
}

func (this *App) DevFixtures() error {
	return nil
}

func (this *App) InitDb() error {
	session, err := mgo.Dial(this.Config.Mongo.Host)
	if err != nil {
		return errors.Wrap(err, "")
	}

	session.SetMode(mgo.Monotonic, true)
	//db.InitPool(session)

	return nil
}

func Handle(handler interface{}) func(c echo.Context) error {
	return func(c echo.Context) error {
		return Convertor(c, handler)
	}
}

//TODO: unit tests
func Convertor(c echo.Context, handler interface{}) error {
	//func(interface{}, interface{}) error
	t := reflect.TypeOf(handler)
	if t.NumIn() != 2 {
		panic("handler format: (*request, *response) error")
	}
	if t.NumOut() != 1 {
		panic("handler format: (*request, *response) error")
	}

	request := reflect.New(t.In(0).Elem())
	response := reflect.New(t.In(1).Elem())

	if !request.Elem().FieldByName("Ctx").CanAddr() {
		panic("can't addr Ctx field")
	}

	if !request.Elem().FieldByName("Ctx").CanSet() {
		panic("can't inject Ctx field")
	}

	err := c.Bind(request.Interface())
	if err != nil {
		return errors.Wrap(err, "")
	}
	request.Elem().FieldByName("Ctx").Set(reflect.ValueOf(c))

	values := reflect.ValueOf(handler).Call([]reflect.Value{request, response})
	if nil != values[0].Interface() {
		return values[0].Interface().(error)
	}

	err = c.JSON(http.StatusOK, response.Interface())
	if err != nil {
		return errors.Wrap(err, "")
	}

	return nil
}

func (this *App) Run() error {
	bind := fmt.Sprintf("%s:%d", this.Config.Http.Host, this.Config.Http.Port)

	e := echo.New()

	e.Debug = true
	e.POST("/", this.NewUserHandler().Test)
	e.GET("/", this.NewUserHandler().Validate)

	e.Run(bind)

	return nil
}

func (this *App) onClose(cb func()) {
	this.onCloseStack = append(this.onCloseStack, cb)
}

func (this *App) Close() {
	for _, cb := range this.onCloseStack {
		cb()
	}
	log.Println("Bye!")
}
