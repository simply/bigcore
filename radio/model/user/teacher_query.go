package user

import (
	"bigcore/radio/model/ds"

	"bigcore/radio/common/db"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type teacherQuery struct {
	*db.Query
}

func (this *teacherQuery) applyBecomeTeacher(result *ds.User) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{
		Update: bson.M{
			"$set": bson.M{
				"profile.teacher.status": ds.TEACHER_STATUS_PENDING,
			},
			"$addToSet": bson.M{
				"type": ds.USER_TYPE_TEACHER,
			},
		},
		ReturnNew: true,
	}, result)
}
