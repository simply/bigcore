package todo

import (
	"bigcore/todo/db"

	"context"
	"gopkg.in/mgo.v2/bson"
)

type todo struct{}

var TodoService = &todo{}

func (*todo) Create(sctx context.Context, request *CreateRequest) (*CreateReply, error) {

	item := &db.Item{
		Id:    bson.ObjectIdHex(request.Item.Id),
		Title: request.Item.Title,
	}

	err := db.Todo.Put(ctx, item)
	if err != nil {
		return nil, err
	}

	reply := &CreateReply{}
	return reply, nil
}

func (*todo) List(ctx context.Context, request *ListRequest) (*ListReply, error) {
	return &ListReply{}, nil
}
