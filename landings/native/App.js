import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { observable } from 'mobx';
import { Store, TodoList } from './todo/List'

import {
    createRouter,
    NavigationProvider,
    StackNavigation,
} from '@expo/ex-navigation';

const Router = createRouter(() => ({
    home: () => Do,
    alex: () => Alex,
}));

const store = {
    todo: new Store(),
}

class Do extends React.Component {
    static route = {
        navigationBar: {
            title: 'Home',
        }
    }

    render() {
        return (
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <TodoList store={store.todo} />

                <Text onPress={this._handlePress}>HomeScreen!</Text>
            </View>
        );
    }

    _handlePress = () => {
        this.props.navigator.push('alex');
    }
}

export default class App extends React.Component {

    render() {
        return (
            <NavigationProvider router={Router}>
                <StackNavigation initialRoute="home" />
            </NavigationProvider>
        );
    }
}

class Alex extends React.Component {
    static route = {
        navigationBar: {
            title: 'Alex',
        }
    }

    render() {
        return <Text>Alex!</Text>
    }
}
