import React from 'react'
import {Text, ListView} from 'react-native'
import {connect} from 'react-redux'
import * as actions from './actions'
import {Loading} from '../../components'
import List from './list'

const mapState   = state => ({
    dataSource: state.Club.dataSource,
    loaded    : state.Club.loaded,
})
const mapActions = dispatch => ({
    refresh: () => dispatch(actions.refresh()),
})

@connect(mapState, mapActions)
export default class Module extends React.Component {
    static displayName = 'Clubs'
    static propTypes   = {
        loaded    : React.PropTypes.bool.isRequired,
        dataSource: React.PropTypes.instanceOf(ListView.DataSource).isRequired,
        refresh   : React.PropTypes.func.isRequired,
    }

    render() {
        const {loaded, dataSource} = this.props
        if (!loaded) {
            return <Loading />
        }

        if (dataSource.getRowCount() == 0) {
            return (
                <Text>Oopss... no Clubs...</Text>
            )
        }

        return <List dataSource={dataSource}/>
    }
}
