package main

import (
	"bigcore/radio/common/grpc/codec"
	"bigcore/radio/transfer/radio"
	"context"
	"google.golang.org/grpc"
	"log"
)

const (
	address = "127.0.0.1:8062"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithCodec(codec.NewJsonCodec()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := radio.NewTeacherClient(conn)

	r, err := c.List(context.Background(), &radio.TeacherListRequest{1, 2})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Printf("Greeting: %s", r.Teachers)
}
