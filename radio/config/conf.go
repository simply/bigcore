package config

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path"
	"syscall"

	"path/filepath"

	"bigcore/radio/common"
	"context"
	"github.com/pkg/errors"
	yaml "gopkg.in/yaml.v2"
)

type keyType struct{}

var key = keyType{}

func NewContext(ctx context.Context, instance Config) context.Context {
	return context.WithValue(ctx, key, instance)
}

func FromContext(ctx context.Context) Config {
	value := ctx.Value(key)
	if value == nil {
		panic("no config in context")
	}
	return value.(Config)
}

const PROJECT_NAME = "bigcore"

type EnvType string

const (
	ENV_PROD  = EnvType("prod")
	ENV_STAGE = EnvType("stage")
	ENV_DEV   = EnvType("dev")
	ENV_TEST  = EnvType("test")
)

var (
	Dir = flag.String("conf_dir", "", "Folder with config files")
	Env = flag.String("env", "", "Environment")
)

type Config struct {
	Env             EnvType
	Http            Http            `yaml:"http"`
	Mongo           Mongo           `yaml:"mongo"`
	Sources         Sources         `yaml:"sources"`
	Facebook        Facebook        `yaml:"facebook"`
	DefaultLocation DefaultLocation `yaml:"default_location"`
	Auth            Auth            `yaml:"auth"`
}

type Http struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

type Facebook struct {
	AppId       string `yaml:"app_id"`
	Secret      string `yaml:"secret"`
	RedirectURL string `yaml:"redirect_url"`
	Simply      Simply `yaml:"simply"`
}

type Simply struct {
	PageId string `yaml:"page_id"`
}

type Longitude float64
type Latitude float64
type DefaultLocation struct {
	Longitude Longitude `yaml:"longitude"`
	Latitude  Latitude  `yaml:"latitude"`
}

type Auth struct {
	Enabled bool `yaml:"enabled"`
}

type Mongo struct {
	Port      int       `yaml:"port"`
	Host      string    `yaml:"host"`
	DbName    string    `yaml:"db_name"`
	MongoPool MongoPool `yaml:"pool"`
}

type MongoPool struct {
	Size int `yaml:"size"`
}

type Sources struct {
	SourcesRadioT SourcesRadioT `yaml:"radio_t"`
}

type SourcesRadioT struct {
	Host           string `yaml:"host"`
	CrawlerTimeout int    `yaml:"crawler_timeout"`
}

var (
	config Config
)

func Get() Config {
	return config
}

func init() {
	config = Config{}

	flag.Parse()
	if Env == nil || *Env == "" {
		panic("please specify --env flag")
	}
	config.Env = EnvType(*Env)

	loadConfig()

	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGUSR2)
	go func() {
		for {
			<-s
			loadConfig()
			log.Println("Reloaded")
		}
	}()
}

func ConfDir() string {
	var err error
	var dir string

	if *Dir == "" || Dir == nil {
		dir, err = filepath.Abs(os.Args[0])
		if err != nil {
			panic(err)
		}
		dir = filepath.Dir(dir)

	} else {
		if filepath.IsAbs(*Dir) {
			dir = *Dir
		} else {
			dir, err = filepath.Abs(*Dir)
			if err != nil {
				panic(err)
			}
		}
	}

	for i := 0; i < 10; i++ {
		if filepath.IsAbs(*Dir) {
			break
		}

		last := path.Base(dir)

		if last == "" {
			panic(errors.Errorf("PROJECT_NAME const value is invalide: %s", PROJECT_NAME))
		}
		if last != PROJECT_NAME {
			dir = path.Dir(dir)
		} else {
			dir = filepath.Join(dir, "radio", "config")
			break
		}
	}

	dir = path.Clean(dir)
	if dir == "/" {
		panic("please configure conf_dir")
	}

	return dir
}

func loadConfig() {
	dir := ConfDir()

	files := []string{dir + "/app.yaml"}
	if config.Env == ENV_DEV {
		files = append(files, dir+"/dev.yaml")
	}
	if config.Env == ENV_TEST {
		files = append(files, dir+"/test.yaml")
	}

	common.Must((&config).parseFiles(files))
}

func (this *Config) parseFiles(files []string) error {
	var (
		buf []byte
		err error
	)

	filesExists := len(files)
	for _, file := range files {
		_, err = os.Stat(file)
		if err != nil {
			if os.IsNotExist(err) {
				filesExists--
				continue
			}

			return errors.Wrap(err, "")
		}

		buf, err = ioutil.ReadFile(file)
		if err != nil {
			return errors.Wrap(err, "")
		}

		err = yaml.Unmarshal(buf, this)
		if err != nil {
			return errors.Wrap(err, "")
		}
	}
	if filesExists == 0 {
		panic(errors.Errorf("No config files found, %#v", files))
	}

	return nil
}
