import {REFRESH_LIST, REFRESH_LIST_FAILED, BEFORE_REFRESH} from './constants'
import {URL, GetHeaders} from '../../config'

const LIST = `${URL.BASE}/club/list`

//each action should have the following signiture.
//  {
//     type: <type of action>,        type is required
//     payload: <the actual payload>  payload is optional. if you don't
//                                    have anything to send to reducer,
//                                    you don't need the payload. for example
//                                    newCounter action
//  }

export const refresh = () => {
    return (dispatch, getState) => {
        dispatch(onBeforeRefresh())
        const geo = getState().Geo
        const params = {
            location: geo.available ? geo.location : null,
        }

        fetch(LIST, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify(params),
        }).then(response => response.json())
            .then(json => dispatch(receiveEvents(json)))
            .catch(error => dispatch(receiveEventsFailed(LIST, error)))
            .done()
    }
}

export const onBeforeRefresh = () => {
    return {
        type: BEFORE_REFRESH,
    }
}

export const receiveEvents = (data) => {
    return {
        type   : REFRESH_LIST,
        payload: {
            clubs: data.clubs,
        },
    }
}

export const receiveEventsFailed = (url, error) => {
    return {
        type   : REFRESH_LIST_FAILED,
        payload: error,
        error  : true,
        meta   : {
            url,
        },
    }
}
