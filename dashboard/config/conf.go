package config

import (
	"flag"
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

var (
	Dir = flag.String("conf_dir", "./dashboard/config/", "Folder with config files")
)

type Config struct {
	Http           *Http           `yaml:"http"`
	Engine         *Engine         `yaml:"engine"`
	CsrfProtection *CsrfProtection `yaml:"csrf"`
	Cookie         *Cookie         `yaml:"cookie"`
}

type Engine struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

type Http struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

type CsrfProtection struct {
	Key    string `yaml:"key"`
	Cookie string `yaml:"cookie"`
	Header string `yaml:"header"`
	Secure bool   `yaml:"secure"`
}

type Cookie struct {
	MacSecret string `yaml:"mac_secret"`
	Secure    bool   `yaml:"secure"`
}

func NewConf() (*Config, error) {
	conf := &Config{}

	err := conf.parseFiles([]string{*Dir + "app.yaml", *Dir + "dev.yaml"})
	if err != nil {
		return nil, err
	}

	return conf, nil
}

func (this *Config) parseFiles(files []string) error {
	var (
		buf []byte
		err error
	)

	for _, file := range files {
		_, err = os.Stat(file)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			}

			return err
		}

		buf, err = ioutil.ReadFile(file)
		if err != nil {
			return err
		}
		err = yaml.Unmarshal(buf, this)
		if err != nil {
			return err
		}
	}

	return nil
}
