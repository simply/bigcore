/// <reference path="../../typings/tsd.d.ts"/>

import * as React from 'react'
import { connect } from 'react-redux'

import Search from "../../search/containers/search"
import Migration from "./migration"
import Dispatch = Redux.Dispatch;
import { LeftNav, MenuItem, RaisedButton } from "material-ui"

interface AppProps {
    dispatch?: Dispatch,
}

class App extends React.Component<AppProps, any> {
    constructor(props: AppProps) {
        super(props);
    }

    render() {
        return (
            <div>
                <Migration/>
                <Search />
            </div>
        )
    }
}

function select(state) {
    return {}
}

export default connect(select)(App);