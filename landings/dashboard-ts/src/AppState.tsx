// import { TeacherListRequest } from "./transport/teacher-list_pb";
// import Teacher from "./server/teacher";
import { action, observable } from "mobx";
import * as Todo from "./transport/todo_pb";
import { State as HomepageState } from "./pages/HomePage";

class TodoState {
    @observable list: Todo.Item[] = [];

    @observable input: string = "";

    @action.bound
    handleInput(e: React.FormEvent<HTMLInputElement>): void {
        this.input = e.currentTarget.value;
    }

    @action.bound
    add(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();

        if (this.input === "") { // TODO: trim me
            return;
        }

        let item = new Todo.Item();
        item.setTitle(this.input);
        item.setId(Math.random().toString());
        this.list.unshift(item);

        this.input = "";
    }
}

class AppState {
    @observable error: string;
    todo: TodoState = new TodoState();

    homePage: HomepageState = new HomepageState();

    // loadTeachers() {
    //     const request = new TeacherListRequest();

    //     Teacher.list(request)
    //         .then((response) => this.teachers = response.getTeachersList())
    //         .catch((reason) => this.error = "Some error happened");
    // }
}

export default AppState;
