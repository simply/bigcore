package app

import (
	"bigcore/todo"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"runtime/debug"
	//"context"
	"bigcore/todo/config"
	"context"
	"fmt"
	"net"
	"os"
)

func init() {
	grpclog.SetLoggerV2(grpclog.NewLoggerV2(os.Stdout, os.Stdout, os.Stdout))
}

//func loggingMiddleware(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
//	resp, err = handler(ctx, req)
//	if err != nil {
//		grpclog.Errorln(err)
//	}
//	return resp, err
//}

func Start(ctx context.Context) chan error {
	errCh := make(chan error, 1)

	recoveryOpts := grpc_recovery.WithRecoveryHandler(func(p interface{}) (err error) {
		grpclog.Infof("%s\n%s\n", p, debug.Stack())
		return status.Error(codes.Internal, "Internal Service Error")
	})

	//var a = grpc.UnaryServerInterceptor(loggingMiddleware)
	grpcServer := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_recovery.StreamServerInterceptor(recoveryOpts),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_recovery.UnaryServerInterceptor(recoveryOpts),
			//a,
		)),
	)

	todo.RegisterOrdersServer(grpcServer, todo.OrderService)
	todo.RegisterTodoServer(grpcServer, todo.TodoService)

	// Register reflection service on gRPC server.
	reflection.Register(grpcServer)

	addr := fmt.Sprintf("%s:%d", config.Get().Http.Host, config.Get().Http.Port)

	go func() {
		lis, err := net.Listen("tcp", addr)
		if err != nil {
			errCh <- err
			return
		}

		grpclog.Infoln("Started: " + addr)

		if err := grpcServer.Serve(lis); err != nil {
			errCh <- err
		}
	}()

	go func() {
		select {
		case <-ctx.Done():
			grpcServer.Stop()
		case <-errCh:
		}
	}()

	return errCh
}
