package ds

import "gopkg.in/mgo.v2/bson"

type CourseStatus string

const (
	CourseStatusActive   = CourseStatus("active")
	CourseStatusInactive = CourseStatus("inactive")
)

type CourseType string

const (
	CourseTypeOnline  = CourseType("online")
	CourseTypeOffline = CourseType("offline")
)

type Course struct {
	ID         bson.ObjectId   `bson:"_id,omitempty" json:"id"`
	FksClub    []bson.ObjectId `bson:"fks_club" json:"fks_club"`
	FksTeacher []bson.ObjectId `bson:"fks_teacher" json:"fks_teacher"`
	Price      float64         `bson:"price" json:"price"`
	Name       string          `bson:"name,omitempty" json:"name,omitempty"`
	Type       CourseType      `bson:"type,omitempty" json:"type,omitempty"`
	Status     CourseStatus    `bson:"status,omitempty" json:"status,omitempty"`
	Lessons    []Lesson        `bson:"lessons" json:"lessons"`
}

type LessonStatus string

const (
	LessonStatusActive   = LessonStatus("active")
	LessonStatusInactive = LessonStatus("inactive")
)

type Lesson struct {
	Name   string       `bson:"name,omitempty"`
	Status LessonStatus `bson:"status,omitempty"`
}
