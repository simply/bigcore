package course

import (
	"bigcore/radio/model/ds"

	commonErrors "errors"

	"context"
	"github.com/pkg/errors"
)

var (
	ErrNoAvailableTeachers = commonErrors.New("Course doesn't have available teachers")
)

type Model struct {
	collection *collection
}

func New(ctx context.Context) *Model {
	return &Model{
		collection: newCollection(ctx),
	}
}

func (this *Model) find(scopes ...interface{}) *query {
	return &query{
		Query: this.collection.Find(scopes...),
	}
}

func (this *Model) EnsureIndices() error {
	return this.collection.ensureIndices()
}

func (this *Model) ActiveCourses() ([]ds.Course, error) {
	result := make([]ds.Course, 0, 0)
	err := this.collection.Find(active).All(&result)
	return result, err
}

func (this *Model) CreateCourseInstanceToSubscribe(idCourse string) (*ds.AppliedCourse, error) {
	instance := &ds.Course{}
	err := this.collection.Find(byId(idCourse)).One(instance)
	if err != nil {
		return nil, err
	}

	if len(instance.FksTeacher) < 0 {
		return nil, errors.Wrapf(ErrNoAvailableTeachers, "CourseId: %s, CourseName: %s", instance.ID, instance.Name)
	}

	appliedCourse := &ds.AppliedCourse{
		FkCourse:  instance.ID,
		FkTeacher: instance.FksTeacher[0],
		Status:    ds.AppliedCourseStatusAwaitingTeacherAccept,
		Name:      instance.Name,
		Type:      instance.Type,
		Price:     instance.Price,
	}

	return appliedCourse, nil
}
