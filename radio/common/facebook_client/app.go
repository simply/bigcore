package facebook_client

import (
	"bigcore/radio/config"

	"net"
	"net/http"
	"time"

	"github.com/huandu/facebook"
)

func GetFacebookSession() (*facebook.Session, error) {
	fbConfiguration := config.Get().Facebook
	var globalApp = facebook.New(fbConfiguration.AppId, fbConfiguration.Secret)

	// facebook asks for a valid redirect uri when parsing signed request.
	// it's a new enforced policy starting in late 2013.
	globalApp.RedirectUri = fbConfiguration.RedirectURL

	session := globalApp.Session(globalApp.AppAccessToken())

	t := &http.Transport{
		Dial: (&net.Dialer{
			Timeout:   60 * time.Second,
			KeepAlive: 30 * time.Second,
		}).Dial,
		// We use ABSURDLY large keys, and should probably not.
		TLSHandshakeTimeout: 120 * time.Second,
	}

	session.HttpClient = &http.Client{
		Transport: t,
	}

	return session, nil
}
