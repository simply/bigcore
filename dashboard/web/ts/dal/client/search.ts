/// <reference path="../../typings/tsd.d.ts"/>
/// <reference path="../axios.d.ts"/>

//import AxiosXHR = Axios.AxiosXHR;
//import AxiosStatic = Axios.AxiosStatic;

export type SearchResolve = (r: SearchResponse) => void

export interface IClient {
    search(request: SearchRequest): Promise<SearchResponse>
}

//"processed": progress.Processed, "total": progress.Total, "eta": progress.EtaInSeconds,
export class ProgressResponse {
    Progress: number
    EtaInSeconds: number
}

export class SearchRequest {
    query: string
}

export class SearchResponseDoc {
    id: string
    title: string
    pinned: boolean
}

export class SearchResponse {
    docs: SearchResponseDoc[]
}

export class Client implements IClient {
    constructor() {
    }

    search(request: SearchRequest): Promise<SearchResponse> {
        return new Promise((resolve, reject) => {
            axios.post("/test/_search", request).then((res) => {
                console.log("!!!!", res)
                resolve(res.data)
            }).catch((reason) => {
                reject(reason)
                return true
            })
        })
    }
}