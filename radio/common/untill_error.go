package common

import (
	"bytes"
	"fmt"
	"github.com/pkg/errors"
	"google.golang.org/grpc/grpclog"
)

func UntilError(fns ...func() error) error {
	for _, fn := range fns {
		err := fn()
		if err != nil {
			return err
		}
	}

	return nil
}

func Must(err error) {
	if err != nil {
		panic(err)
	}
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func LogError(err error) {
	var buffer bytes.Buffer
	buffer.Write([]byte(err.Error()))
	buffer.Write([]byte("\n"))
	if err, ok := err.(stackTracer); ok {
		for _, f := range err.StackTrace() {
			buffer.Write([]byte(fmt.Sprintf("%+v\n", f)))
		}
	}
	grpclog.Printf("%s\n", buffer.String())
}
