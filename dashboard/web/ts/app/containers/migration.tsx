/// <reference path="../../typings/tsd.d.ts"/>

import * as React from 'react'
import { connect } from 'react-redux'

import * as MigrationActions from './../actions/migration'
import * as MigrationTransfer from "../../dal/client/migration"
import MigrationStart from "../components/migration_start"
import Dispatch = Redux.Dispatch;

import { Tabs, Tab, LinearProgress, RaisedButton } from 'material-ui'

interface MigrationProps {
    progress?: number
    migrationClient?: MigrationTransfer.Client,
    dispatch?: Dispatch,
}

class Migration extends React.Component<MigrationProps, any> {
    constructor(props: MigrationProps) {
        super(props);
        // set initial state
        this.state = {progress: 0};
    }

    render() {
        let { progress, migrationClient } = this.props

        return (
            <div>
                <div>
                    <RaisedButton label="Start" onClick={() => {MigrationActions.migrationStart(migrationClient)}}/>
                    <LinearProgress mode="determinate" value={progress}/>
                </div>

                <br/>
            </div>
        )
    }
}

function select(state) {
    return {
        progress:        state.migration.progress,
        migrationClient: state.ws.migrationClient,
    }
}

export default connect(select)(Migration);