package datastruct

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//TODO: add index by fb_user_id
type OAuthFacebook struct {
	IdUserOnFacebook    uint64    `bson:"id_user_on_facebook,omitempty"`
	Token               string    `bson:"token,omitempty"`
	TokenExpirationDate time.Time `bson:"token_expire_date,omitempty"`
	Permissions         []string  `bson:"permissions"`
	TokenAuthorized     bool      `bson:"token_authorized,omitempty"`
}

type OAuth struct {
	Facebook OAuthFacebook `bson:"facebook,omitempty"`
}

type UserType string

const (
	USER_TYPE_TEACHER      = UserType("teacher")
	USER_TYPE_CLUB_MANAGER = UserType("club_manager")
	USER_TYPE_STUDENT      = UserType("student")
	USER_TYPE_GUEST        = UserType("guest")
)

type TeacherStatus string

const (
	TEACHER_STATUS_ACTIVE   = TeacherStatus("active")
	TEACHER_STATUS_PENDING  = TeacherStatus("pending")
	TEACHER_STATUS_INACTIVE = TeacherStatus("inactive")
)

type GuestStatus string

const (
	GUEST_STATUS_ACTIVE   = GuestStatus("active")
	GUEST_STATUS_INACTIVE = GuestStatus("inactive")
)

type TeacherSkill struct {
	Label   string `bson:"string,omitempty"`
	Percent int    `bson:"percent,omitempty"`
}

type Teacher struct {
	Status TeacherStatus  `bson:"status,omitempty"`
	Skills []TeacherSkill `bson:"skills,omitempty"`
}

type Guest struct {
	Status                        GuestStatus `bson:"status,omitempty"`
	PendingInterviewsWithTeachers []uint64    `bson:"pending_interviews_with_teachers"`
	PendingInterviewsWithClubs    []uint64    `bson:"pending_interviews_with_clubs"`
}

type AppliedCourseStatus string

const (
	AppliedCourseStatusAwaitingTeacherAccept = AppliedCourseStatus("awaiting_teacher_accept")
	AppliedCourseStatusAwaitingPayment       = AppliedCourseStatus("awaiting_payment")
	CourseStatusInProgress                   = AppliedCourseStatus("in_progress")
	CourseStatusDone                         = AppliedCourseStatus("done")
)

type AppliedCourse struct {
	FkCourse  bson.ObjectId       `bson:"fk_course"`
	FkTeacher bson.ObjectId       `bson:"fks_teacher" json:"fks_teacher"`
	Status    AppliedCourseStatus `bson:"status" json:"status"`
	Name      string              `bson:"name,omitempty" json:"name,omitempty"`
	Price     float64             `bson:"price" json:"price"`
}

type Student struct {
	AppliedCourses []AppliedCourse `bson:"applied_courses"`
}

type User struct {
	ID          bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Email       string        `bson:"email,omitempty"`
	FirstName   string        `bson:"first_name,omitempty"`
	LastName    string        `bson:"last_name,omitempty"`
	Picture     string        `bson:"picture_name,omitempty"`
	Description string        `bson:"description,omitempty"`
	Roles       []string      `bson:"roles"`
	OAuth       OAuth         `bson:"o_auth,omitempty"`
	Profile     Profile       `bson:"profile,omitempty"`
	Type        []UserType    `bson:"type"`
}

type Profile struct {
	Facebook map[string]interface{} `bson:"facebook"`
	Guest    Guest                  `bson:"guest"`
	Student  Student                `bson:"student"`
	Teacher  Teacher                `bson:"teacher"`
}
