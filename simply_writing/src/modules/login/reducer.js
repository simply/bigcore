import { handleActions } from 'redux-actions'
import {
    FB_LOGIN_BY_SERVER, FB_BEFORE_LOGIN_BY_SERVER, FB_LOGIN_NOT_FOUND,
    FB_LOGOUT, FB_CANCEL, FB_ERROR, FB_PERMISSIONS_MISSING,
} from './fb_constants'

import * as courseEvents from './../course/constants'

export type FbLoginData = {
    token: string,
    userID: string,
    expirationTime: Date,
    permissions: Array<string>,
    accessToken: any,
}

export type State = {
    initialized: bool,
    authenticated: bool,
    user: any,
    facebook: FbLoginData
}

const initialState: State = {
    initialized  : false,
    authenticated: false,
    user         : {},
    facebook     : {},
}

//you can do better here, I was just showing that you need to make a new copy
//of state. It is ok to deep copy of state. It will prevent unseen bugs in the future
//for better performance you can use immutableJS

//handleActions is a helper function to instead of using a switch case statement,
//we just use the regular map with function state attach to it.


//TODO: initialized must be difined in each reducer
export default handleActions({
    [FB_LOGIN_BY_SERVER]                  : (state: State, { payload }): State => {
        return {
            ...state,
            initialized  : true,
            authenticated: true,
            user         : payload.user,
        }
    },
    [FB_BEFORE_LOGIN_BY_SERVER]           : (state: State, { payload }): State => {
        return {
            ...state,
            facebook: payload.facebook,
        }
    },
    [FB_LOGIN_NOT_FOUND]                  : (state: State): State => {
        return state
    },
    [FB_LOGOUT]                           : (state: State): State => {
        return {
            ...state,
            authenticated: false,
            user         : {},
            facebook     : {},
        }
    },
    [FB_ERROR]                            : (state: State): State => {
        return state
    },
    [FB_CANCEL]                           : (state: State): State => {
        return state
    },
    [FB_PERMISSIONS_MISSING]              : (state: State): State => {
        return state
    },
    [courseEvents.APPLY_TO_COURSE_SUCCEED]: (state: State, { payload }): State => {
        return {
            ...state,
            user: {
                ...state.user,
                Profile: {
                    ...state.user.Profile,
                    Student: {
                        ...state.user.Profile.Student,
                        AppliedCourses: payload.applied_courses,
                    }
                },
            },
        }
    },
}, initialState)

