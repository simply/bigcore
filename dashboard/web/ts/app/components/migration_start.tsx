import * as React from 'react'
import { connect } from 'react-redux'

export default class MigrationStart extends React.Component<any, any> {

    render() {
        const {onStartClick} = this.props

        return (
            <button onClick={onStartClick}>
                Start
            </button>
        )
    }
}
