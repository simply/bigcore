package worker
//
//import (
//	"bigcore/radio/model/ds"
//	"testing"
//
//	"github.com/golang/mock/gomock"
//	assertLib "github.com/stretchr/testify/assert"
//	"github.com/stretchr/testify/mock"
//	"github.com/stretchr/testify/suite"
//)
//
//type fetch struct {
//	suite.Suite
//	events         []ds.FbEvent
//	facebookPageId string
//	job            *EventsFetcherJob
//}
//
//func _TestFetch(t *testing.T) {
//	suite.Run(t, new(fetch))
//}
//
//func (this *fetch) objects() (*MockIFetcher, *MockIClubFetcher, *MockISaver, *EventsFetcherWorker, *gomock.Controller, *assertLib.Assertions) {
//	mockCtrl := gomock.NewController(this.T())
//
//	fetcher := NewMockIFetcher(mockCtrl)
//	mockClubFetcher := NewMockIClubFetcher(mockCtrl)
//	clubFetcher = func() IClubFetcher {
//		return mockClubFetcher
//	}
//	mockSaver := NewMockISaver(mockCtrl)
//	saver = func() ISaver {
//		return mockSaver
//	}
//
//	fetcherService := NewEventsFetcherWorker(fetcher)
//
//	return fetcher, mockClubFetcher, mockSaver, fetcherService, mockCtrl, assertLib.New(this.T())
//}
//
//func (this *fetch) SetupSuite() {
//	this.events = []ds.FbEvent{}
//	this.facebookPageId = "12345"
//	this.job = &EventsFetcherJob{
//		PageId: this.facebookPageId,
//	}
//}
//
//func (this *fetch) TestAllSucceed_NoError() {
//	fetcher, _, saver, fetcherService, mockCtrl, assert := this.objects()
//	defer mockCtrl.Finish()
//
//	//Arrange
//	gomock.InOrder(
//		fetcher.EXPECT().FetchAll(this.facebookPageId).Return(this.events, nil),
//		saver.EXPECT().UpdateFromFacebook(mock.Anything).Return(nil),
//	)
//
//	//Act
//	err := fetcherService.fetch(this.job)
//
//	//Assert
//	assert.Nil(err)
//}
//
//func (this *fetch) TestFetchFailed_ErrorAndNoUpdate() {
//	fetcher, _, _, fetcherService, mockCtrl, assert := this.objects()
//	defer mockCtrl.Finish()
//
//	//Arrange
//	expectedErr := errors.New("fetch error")
//	fetcher.EXPECT().FetchAll(this.facebookPageId).Return(this.events, expectedErr)
//
//	//Act
//	err := fetcherService.fetch(this.job)
//
//	//Assert
//	assert.Equal(expectedErr, err)
//}
//
//func (this *fetch) TestUpdateFailed_ErrorOnUpdate() {
//	fetcher, _, saver, fetcherService, mockCtrl, assert := this.objects()
//	defer mockCtrl.Finish()
//
//	//Arrange
//	expectedErr := errors.New("update error")
//	gomock.InOrder(
//		fetcher.EXPECT().FetchAll(this.facebookPageId).Return(this.events, nil),
//		saver.EXPECT().UpdateFromFacebook(mock.Anything).Return(expectedErr),
//	)
//
//	//Act
//	err := fetcherService.fetch(this.job)
//
//	//Assert
//	assert.Equal(expectedErr, err)
//}
