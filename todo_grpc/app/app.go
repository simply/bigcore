package app

import (
	"bigcore/todo/config"
	"bigcore/todo/db/mongo"
	"context"
	"log"
)

type App struct {
}

func New() *App {
	return &App{}
}

func (this *App) MustInit() {
	err := this.Init()
	if err != nil {
		panic(err)
	}
}

func (this *App) Init() error {
	openCloser, err := mongo.Open(context.Background(), config.Get().Mongo)
	if err != nil {
		log.Println(err)
	}

	errorCh := mongo.AutoReconnect(context.Background(), openCloser)
	go func() {
		for err := range errorCh {
			if err != nil {
				log.Println(err)
			}
		}
	}()

	return nil
}

func UntilError(fns ...func() error) error {
	for _, fn := range fns {
		err := fn()
		if err != nil {
			return err
		}
	}

	return nil
}

func (this *App) Run() error {
	errCh := Start(context.Background())
	if err := <-errCh; err != nil {
		return err
	}
	return nil
}
