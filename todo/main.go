package main

import (
	"errors"
	"fmt"
)

func main() {
	var a C
	a.hello()
	fmt.Printf("%v \n", a)
}

func hello(in *A) {
	in.prop1 = 10
}

func mutiply(in int) (out int, err error) {
	if in > 10 {
		return 0, errors.New("too much")
	}
	return in * 2, nil
}

type A struct {
	prop1 int
	prop2 string
}

type B struct {
	prop3 int
	prop4 string
}
type C struct {
	A
	B
}

func (this *A) hello() {

}
