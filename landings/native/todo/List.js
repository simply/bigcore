import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { observer } from 'mobx-react/native';
import { observable } from 'mobx';
import autobind from 'autobind-decorator'

@autobind
class Store {
    @observable title = "A";

    boom() {
        this.title = "Boom!";
    }
}

const TodoList = ({ store }) =>
    <View style={styles.container}>
        <TodoView todo={store} />
        <Text>Open up App.js to start1 working on your app!</Text>
        <Text>Changes you make will automatically reload.</Text>
    </View>


const TodoView = observer(({ todo }) =>
    <View>
        <Text>{todo.title}</Text>
        <Button onPress={todo.boom} title="bobobo" />
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
})

export { Store, TodoList }

