package db

import (
	"bigcore/radio/config"

	"context"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func wrap(err error) error {
	if err != nil {
		return errors.Wrap(err, "")
	}
	return nil
}

type Collection struct {
	c *mgo.Collection
}

func NewCollection(ctx context.Context, collectionName string) *Collection {
	return &Collection{
		c: FromContext(ctx).DB(config.FromContext(ctx).Mongo.DbName).C(collectionName),
	}
}

func NewCollection2(db *mgo.Database, collectionName string) *Collection {
	return &Collection{
		c: db.C(collectionName),
	}
}

func (this *Collection) Find(scopes ...interface{}) *Query {
	query := bson.M{}
	for _, scope := range scopes {
		if scope == nil {
			continue
		}

		if sc, ok := scope.(bson.M); ok {
			for k, v := range sc {
				query[k] = v
			}
		} else {
			panic(errors.Errorf("can't cast to bson.M: %#v", scope))
		}
	}

	return &Query{
		internal: this.c.Find(query),
	}
}

//func (this *Collection) FindId(id interface{}) *Query {
//	return &Query{
//		internal: this.c.FindId(id),
//	}
//}

func (this *Collection) EnsureIndex(index mgo.Index) error {
	return wrap(this.c.EnsureIndex(index))
}

func (this *Collection) Insert(docs ...interface{}) error {
	return wrap(this.c.Insert(docs...))
}

func (this *Collection) Update(selector interface{}, update interface{}) error {
	return wrap(this.c.Update(selector, update))
}

func (this *Collection) UpdateId(id interface{}, update interface{}) error {
	return wrap(this.c.UpdateId(id, update))
}

func (this *Collection) UpdateAll(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = this.c.UpdateAll(selector, update)
	return info, wrap(err)
}

func (this *Collection) Upsert(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = this.c.Upsert(selector, update)
	return info, wrap(err)
}

func (this *Collection) UpsertId(id interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = this.c.UpsertId(id, update)
	return info, wrap(err)
}

func (this *Collection) RemoveId(id interface{}) error {
	return wrap(this.c.RemoveId(id))
}

func (this *Collection) Remove(selector interface{}) error {
	return wrap(this.c.Remove(selector))
}

func (this *Collection) RemoveAll(selector interface{}) (info *mgo.ChangeInfo, err error) {
	info, err = this.c.RemoveAll(selector)
	return info, wrap(err)
}
