import * as React from "react";
import { SFC } from "react";
import AppState from "./../AppState";
import * as Todo from "./../transport/todo_pb";
import { Form, Input, ListGroup, ListGroupItem } from "reactstrap";
import { observer } from "mobx-react";

// import { Button } from "reactstrap";
interface Props {
    state: AppState;
}

@observer
class List extends React.Component<Props, {}> {
    render() {
        const { todo } = this.props.state;
        return (
            <div>
                <TodoInput onSubmit={todo.add} value={todo.input} onChange={todo.handleInput} />
                <ListGroup>
                    {todo.list.map(item => <TodoItem key={item.getId()} item={item} />)}
                </ListGroup>
            </div>
        );
    }
}

interface PropsTodoItem {
    item: Todo.Item;
}

const TodoItem: SFC<PropsTodoItem> = ({ item }) => (
    <ListGroupItem>{item.getTitle()}</ListGroupItem>
);

interface PropsTodoInput {
    onSubmit: React.EventHandler<React.FormEvent<HTMLFormElement>>;
    value: string;
    onChange: React.EventHandler<React.ChangeEvent<HTMLInputElement>>;
}

const TodoInput: SFC<PropsTodoInput> = ({ onSubmit, value, onChange }) => (
    <Form inline={true} onSubmit={onSubmit}>
        <Input value={value} onChange={onChange} />
    </Form>
);

export { List };
