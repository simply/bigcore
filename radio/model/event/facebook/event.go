package facebook

import (
	"fmt"

	"github.com/pkg/errors"

	"bigcore/radio/model/ds"

	"bigcore/radio/common/facebook_client"
	"context"
	"github.com/huandu/facebook"
)

type EventDao struct {
	ctx context.Context
}

func NewEventDao(ctx context.Context) *EventDao {
	return &EventDao{
		ctx: ctx,
	}
}

func (this *EventDao) List(pageId string) (*ds.FbEventsResponse, error) {
	session, err := facebook_client.GetFacebookSession()
	if err != nil {
		return nil, err
	}

	result, err := session.Get(fmt.Sprintf("/%s/events", pageId), facebook.Params{
		"fields": "id,description,end_time,start_time,name,place,cover,type",
		"limit":  10,
	})
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	response := &ds.FbEventsResponse{}
	err = result.Decode(response)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	return response, nil
}
