//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME = 'Events'

//action types
export const INIT               = `${NAME}/INIT`
export const UPDATE_LIST        = `${NAME}/UPDATE_LIST`
export const UPDATE_LIST_FAILED = `${NAME}/UPDATE_LIST_FAILED`
export const BEFORE_REFRESH     = `${NAME}/BEFORE_REFRESH`

//as you can see above, each action is namespaced with module's name.