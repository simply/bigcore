package test

import (
	"bigcore/radio"
	"bigcore/radio/common"
)

func App() *radio.App {
	app := radio.NewApp()
	common.Must(app.Init())
	return app
}

func AppWithCleanDb(dbName string, fixtures Fixtures) *radio.App {
	app := App()
	app.Ctx = SetDbName(app.Ctx, dbName)
	CleanupDb(app.Ctx)
	LoadFixtures(app.Ctx, fixtures)
	return app
}
