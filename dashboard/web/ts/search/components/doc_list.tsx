/// <reference path="../../typings/tsd.d.ts"/>
import "../../../node_modules/dragula/dist/dragula.min.css"

import * as React from 'react'
import { connect } from 'react-redux'

import { reactDragula } from "./../../common/react-dragula"

import * as SearchActions from '../actions/search'

import * as transfer from '../../dal/client/search'
import Hit from './hit'

import * as ReactDOM from 'react-dom'

interface Props {
    search: transfer.SearchResponse
    onPin: Function
    dispatch?: Redux.Dispatch
}

class DocList extends React.Component<Props, any> {
    componentDidMount() {
        var container = ReactDOM.findDOMNode(this)
        let component = this

        reactDragula([container]).on("drop", function (el, target, source, sibling) {
            component._onSort(container, el, target, source, sibling)
        })
    }

    _onSort(container, el, target, source, sibling) {

        //TODO: calculate to which position need to move element, and then call action,
        let dropedId            = this.getId(el)
        var idsForPin: number[] = []

        var doBreakOnUnpin = false
        for (var i = 0; i < container.childNodes.length; i++) {
            var node = container.childNodes.item(i)
            let id   = this.getId(node)
            if (id == dropedId) {
                doBreakOnUnpin = true
                idsForPin.push(id)
                continue
            }

            let pinned = this.isPinned(node)
            if (!pinned && doBreakOnUnpin) {
                break
            }

            idsForPin.push(id)
        }

        this.props.onPin(idsForPin)
    }

    getId(node: Node): number {
        return parseInt(node.attributes.getNamedItem('data-id').value)
    }

    isPinned(node: Node): boolean {
        return node.attributes.getNamedItem('data-pinned').value == "true"
    }

    render() {
        const { search } = this.props
        let docs: transfer.SearchResponseDoc[] = search ? search.docs : []

        return (
            <div className='container'>
                {docs.map(function (doc) {
                    return (
                    <Hit hit={doc}/>
                        )
                    })}
            </div>
        )
    }
}

function select(state) {
    return {
        search: state.search.search,
    }
}

export default connect(select)(DocList)