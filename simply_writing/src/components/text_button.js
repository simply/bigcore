import React from 'react'
import {StyleSheet, View, Text} from 'react-native'
import Button from 'react-native-button'
import Icon from 'react-native-vector-icons/FontAwesome'
import LoaderIcon from './loader_icon'
import GlobalStyles from './../styles'

const styles = StyleSheet.create({
    wrapper: {
        flexDirection : 'row',
        justifyContent: 'flex-end',
        marginTop     : 5,
        marginRight   : 5,
    },
    button : {
        backgroundColor: 'rgb(246, 247, 249)',
        borderColor    : 'rgb(206, 208, 212)',
        borderRadius   : 2,
        borderWidth    : 1,
        paddingLeft    : 6,
        paddingRight   : 6,
        paddingTop     : 1,
        paddingBottom  : 1,
    },
    text   : {
        color   : 'rgb(75, 79, 86)',
        fontSize: 14,
        marginLeft: 6,
    },
    icon   : {},
})

const IconOnButton = ({showLoader, icon}) => {
    if (showLoader) {
        return <LoaderIcon />
    }

    return <Icon name={icon} color={GlobalStyles.blue}/>
}

IconOnButton.propTypes = {
    showLoader: React.PropTypes.bool,
    icon      : React.PropTypes.string.isRequired,
}

const TextButton = ({showLoader, icon, text, onPress}) =>
    <View style={styles.button}>
        <Button onPress={onPress} >
            <IconOnButton style={styles.icon} showLoader={showLoader} icon={icon}/>
            <Text style={styles.text} >{text}</Text>
        </Button>
    </View>

TextButton.propTypes = {
    showLoader: React.PropTypes.bool,
    text      : React.PropTypes.string.isRequired,
    icon      : React.PropTypes.string.isRequired,
    onPress   : React.PropTypes.func.isRequired,
}

export default TextButton
