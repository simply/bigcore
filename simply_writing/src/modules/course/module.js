import React from 'react'
import {Text, ListView, Navigator} from 'react-native'
import {connect} from 'react-redux'
import * as actions from './actions'
import {Loading} from '../../components'
import List from './list'
import autobind from 'autobind-decorator'

const mapState   = state => ({
    loaded       : state.Course.loaded,
    currentScreen: state.Course.currentScreen,
    dataSource   : state.Course.dataSource,

})
const mapActions = dispatch => ({
    refresh: () => dispatch(actions.refresh()),
    onPressItem: (idCourse) => dispatch(actions.applyToCourse(idCourse)),
})

@connect(mapState, mapActions)
export default class Module extends React.Component {
    static displayName = 'Courses'
    static propTypes   = {
        refresh   : React.PropTypes.func.isRequired,
        loaded    : React.PropTypes.bool.isRequired,
        dataSource: React.PropTypes.instanceOf(ListView.DataSource).isRequired,
    }

    @autobind
    renderScene(route) {
        if (route.name === 'list') {
            return this.renderList()
        }

        if (route.name === 'payment') {
            return this.renderPayment()
        }

        throw new Error('Unsupported screen with name: ' + route.name)
    }

    @autobind
    renderPayment() {
        return (
            <Text>Payment Info and contacts</Text>
        )
    }

    @autobind
    renderList() {
        const {loaded, dataSource, onPressItem} = this.props
        if (!loaded) {
            return <Loading />
        }

        if (dataSource.getRowCount() == 0) {
            return (
                <Text>Oopss... no coming Courses...</Text>
            )
        }

        const onPressItemWrapper = (idCourse) => {
            onPressItem(idCourse)
            this.navigator.push({name: 'payment'})
        }
        return <List onPressItem={onPressItemWrapper} />
    }

    render() {
        const {currentScreen} = this.props

        return (
            <Navigator
                initialRoute={{name: currentScreen}}
                ref={instance => this.navigator = instance}
                renderScene={this.renderScene}
            />
        )
    }
}
