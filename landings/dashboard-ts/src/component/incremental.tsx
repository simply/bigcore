import * as React from "react";
import { Button } from "reactstrap";
import { action, observable } from "mobx";
import { observer } from "mobx-react";

class State {
    @observable counter: number = 0;

    @action.bound
    increment() {
        this.counter++;
    }
}

interface Props {
    state: State;
}

const Btn = observer(({ state }: Props) => (
    <div>
        <Button onClick={state.increment}>Do it!</Button>
        <b>{state.counter}</b>
    </div>
));

export { Btn, State };
