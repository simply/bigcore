import {
    UPDATE_LIST, UPDATE_LIST_FAILED, BEFORE_REFRESH,
    APPLY_TO_COURSE_REQUEST, APPLY_TO_COURSE_SUCCEED, APPLY_TO_COURSE_FAIL,
} from './constants'
import {URL, GetHeaders} from '../../config'

const URL_COURSE_LIST  = `${URL.BASE}/course/list`
const URL_COURSE_APPLY = `${URL.BASE}/course/apply`

const Server = {
    courseList   : (getState) => {
        const params = {}

        return fetch(URL_COURSE_LIST, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify(params),
        }).then(response => response.json())
    },
    applyToCourse: (getState, idCourse) => {
        const params = {
            'id_course': idCourse,
        }

        return fetch(URL_COURSE_APPLY, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify(params),
        }).then(response => response.json())
    },
}


export const refresh = () => {
    return (dispatch, getState) => {
        dispatch(onBeforeRefresh())
        Server.courseList(getState)
            .then(json => dispatch(refreshSucceed(json)))
            .catch(error => dispatch(refreshFailed(error)))
            .done()
    }
}

const onBeforeRefresh = () => {
    return {
        type: BEFORE_REFRESH,
    }
}

const refreshSucceed = (data) => {
    return {
        type   : UPDATE_LIST,
        payload: {
            courses: data.courses,
        },
    }
}

const refreshFailed = (error) => {
    return {
        type   : UPDATE_LIST_FAILED,
        payload: error,
        error  : true,
        meta   : {},
    }
}

export const applyToCourse = (idCourse) => {
    return (dispatch, getState) => {
        dispatch(applyToCourseRequest(idCourse))
        Server.applyToCourse(getState, idCourse)
            .then(json => dispatch(applyToCourseSucceed(json)))
            .catch(error => dispatch(applyToCourseFailed(error)))
            .done()
    }
}

const applyToCourseRequest = (idCourse) => {
    return {
        type   : APPLY_TO_COURSE_REQUEST,
        payload: {
            idCourse,
        },
    }
}

const applyToCourseFailed = (error) => {
    return {
        type   : APPLY_TO_COURSE_FAIL,
        payload: error,
        error  : true,
    }
}

const applyToCourseSucceed = (serverResponse) => {
    return {
        type   : APPLY_TO_COURSE_SUCCEED,
        payload: serverResponse
    }
}

