package facebook

import (
	"fmt"
	"strings"

	"github.com/pkg/errors"

	"time"

	"bigcore/radio/config"

	"bigcore/radio/common/facebook_client"
	"context"
	"github.com/huandu/facebook"
)

type UserDao struct {
	ctx context.Context
}

func New(ctx context.Context) *UserDao {
	return &UserDao{
		ctx: ctx,
	}
}

func (this *UserDao) ValidateToken(userId uint64, token string) error {
	if !config.FromContext(this.ctx).Auth.Enabled {
		return nil
	}

	session, err := facebook_client.GetFacebookSession()
	if err != nil {
		return err
	}

	timer := time.Now()
	_, err = session.Get(fmt.Sprintf("/%d", userId), facebook.Params{
		"fields":       "id",
		"access_token": token,
	})
	if err != nil {
		fmt.Printf("%s\n", time.Since(timer))
		return errors.Wrap(err, "")
	}

	return nil
}

type Result struct {
	facebook.Result
}

func (this *UserDao) Fields(userId uint64, token string, fields []string) (*Result, error) {

	url := fmt.Sprintf("/%d", userId)
	params := facebook.Params{
		"fields":       strings.Join(fields, ","),
		"access_token": token,
	}

	result, err := facebook.Get(url, params)
	if err != nil {
		return nil, errors.Wrapf(err, "Url: %s, Params: %+v", url, params)
	}
	return &Result{result}, nil
}
