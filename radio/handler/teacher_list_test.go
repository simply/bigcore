package handler_test

import (
	"bigcore/radio/common/test"
	"bigcore/radio/handler"
	"bigcore/radio/model/ds"
	"github.com/stretchr/testify/assert"
	"testing"
)

var cases = []struct {
	name             string
	fixtures         test.Fixtures
	expectLength     int
	expectFirstEmail string
}{
	{
		name: "found_only_active_only_teachers",
		fixtures: test.Fixtures{
			"user": test.UserFixtures{
				{
					Email: "test@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_ACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_TEACHER},
				},
				{
					Email: "test2@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_ACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_TEACHER},
				},
				{
					Email: "dont_see_inactive@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_INACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_TEACHER},
				},
				{
					Email: "see_only_teachers@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_ACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_STUDENT},
				},
				{
					Email: "ignore_undefined_fields@teacher.com",
				},
			},
		},
		expectLength:     2,
		expectFirstEmail: "test@teacher.com",
	},
	{
		name: "found_only_active_only_teachers2",
		fixtures: test.Fixtures{
			"user": test.UserFixtures{
				{
					Email: "test@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_ACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_TEACHER},
				},
				{
					Email: "test2@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_ACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_TEACHER},
				},
				{
					Email: "dont_see_inactive@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_INACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_TEACHER},
				},
				{
					Email: "see_only_teachers@teacher.com",
					Profile: ds.Profile{
						Teacher: ds.Teacher{
							Status: ds.TEACHER_STATUS_ACTIVE,
						},
					},
					Type: []ds.UserType{ds.USER_TYPE_STUDENT},
				},
				{
					Email: "ignore_undefined_fields@teacher.com",
				},
			},
		},
		expectLength:     2,
		expectFirstEmail: "test@teacher.com",
	},
}

func TestTeacherList(t *testing.T) {
	for _, tc := range cases {
		tc := tc // capture range variable
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			app := test.AppWithCleanDb(tc.name, tc.fixtures)

			defer app.Close()
			defer test.CleanupDb(app.Ctx)

			req := &handler.TeacherListRequest{}
			resp := &handler.TeacherListResponse{}
			err := handler.TeacherList(app.Ctx, req, resp)
			assert.NoError(t, err)

			assert.Equal(t, tc.expectLength, len(resp.Teachers))
			if tc.expectFirstEmail != "" && len(resp.Teachers) > 0 {
				assert.Equal(t, resp.Teachers[0].Email, tc.expectFirstEmail)
			}
		})
	}
}

func BenchmarkTeacher_List(b *testing.B) {
	app := test.App()

	req := &handler.TeacherListRequest{}
	resp := &handler.TeacherListResponse{}

	b.ReportAllocs()
	b.ResetTimer()

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			err := handler.TeacherList(app.Ctx, req, resp)
			if err != nil {
				panic(err)
			}
		}
	})
}
