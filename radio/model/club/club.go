package club

import (
	"bigcore/radio/model/ds"
	"context"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
)

type Model struct {
	collection *collection
}

func New(ctx context.Context) *Model {
	return &Model{
		collection: newCollection(ctx),
	}
}

func (this *Model) EnsureIndices() error {
	return this.collection.EnsureIndices()
}

func (this *Model) find(scopes ...interface{}) *query {
	return &query{
		Query: this.collection.Find(scopes...),
	}
}

func (this *Model) FindAll(point ds.Point) ([]ds.Club, error) {
	result := make([]ds.Club, 0, 0)
	err := this.find(near(point), active).All(&result)
	return result, err
}

func (this *Model) FindById(id string) (*ds.Club, error) {
	var result *ds.Club
	err := this.find(byId(id)).One(result)
	return result, err
}

func (this *Model) FindByFacebookPageId(pageId string) (*ds.Club, error) {
	var result *ds.Club

	err := this.collection.Find(byFbPage(pageId)).One(result)

	if err != nil && errors.Cause(err) == mgo.ErrNotFound {
		return nil, nil
	}

	return result, err
}
