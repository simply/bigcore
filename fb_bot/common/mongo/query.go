package mongo

import (
	"github.com/dropbox/godropbox/errors"
	"gopkg.in/mgo.v2"
)

type Query struct {
	*mgo.Query
	closer func()
}

func (this *Query) All(result interface{}) error {
	defer this.closer()
	return wrap(this.Query.All(result))
}

func (this *Query) Distinct(key string, result interface{}) error {
	defer this.closer()
	return wrap(this.Query.Distinct(key, result))
}

func (this *Query) One(result interface{}) error {
	defer this.closer()
	err := this.Query.One(result)
	if err != nil && err == mgo.ErrNotFound {
		return errors.Wrapf(err, "not fond record for query: %#v", this.Query)
	}
	return wrap(err)
}

func (this *Query) Apply(change mgo.Change, result interface{}) (*mgo.ChangeInfo, error) {
	defer this.closer()
	info, err := this.Query.Apply(change, result)
	return info, wrap(err)
}

func (this *Query) MapReduce(job *mgo.MapReduce, result interface{}) (info *mgo.MapReduceInfo, err error) {
	defer this.closer()
	info, err = this.Query.MapReduce(job, result)
	return info, wrap(err)
}

func (this *Query) Count() (int, error) {
	defer this.closer()
	n, err := this.Query.Count()
	return n, wrap(err)
}
