//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME = 'Club'

//action types
export const INIT                = `${NAME}/INIT`
export const REFRESH_LIST        = `${NAME}/REFRESH_LIST`
export const REFRESH_LIST_FAILED = `${NAME}/REFRESH_LIST_FAILED`
export const BEFORE_REFRESH      = `${NAME}/BEFORE_REFRESH`

//as you can see above, each action is namespaced with module's name.