import React from 'react'
import { TextButton } from '../../components'
import { StyleSheet, View } from 'react-native'

const styles = StyleSheet.create({
    becomeTeacher: {
        flexDirection : 'row',
        justifyContent: 'flex-end',
        marginTop     : 7,
        marginRight   : 5,
    },
})

const BecomeTeacher = ({ onPress, showLoader }) =>
    <View style={styles.becomeTeacher}>
        <TextButton showLoader={showLoader}
                    icon="heart-o" text="Become Teacher"
                    onPress={onPress}
        />
    </View>

BecomeTeacher.propTypes = {
    onPress   : React.PropTypes.func.isRequired,
    showLoader: React.PropTypes.bool.isRequired,
}

export default BecomeTeacher