package main

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/zenazn/goji/web"
)

func (this *App) ApplyRpcClients() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return echo.HandlerFunc(func(c echo.Context) error {
			c.Set("RpcClients", this.RpcClients)
			return nil
		})
	}
}

//func (this *App) ApplyDbMap(c *web.C, h http.Handler) http.Handler {
//	fn := func(w http.ResponseWriter, r *http.Request) {
//		c.Env["DbMap"] = this.DbMap
//		h.ServeHTTP(w, r)
//	}
//	return http.HandlerFunc(fn)
//}

//func (this *App) ApplyAuth(c *web.C, h http.Handler) http.Handler {
//	fn := func(w http.ResponseWriter, r *http.Request) {
//		session := c.Env["Session"].(*sessions.Session)
//		if userId, ok := session.Values["UserId"]; ok {
//			dbMap := c.Env["DbMap"].(*gorp.DbMap)
//
//			user, err := dbMap.Get(models.UserService{}, userId)
//			if err != nil {
//				glog.Warningf("Auth error: %v", err)
//				c.Env["UserService"] = nil
//			} else {
//				c.Env["UserService"] = user
//			}
//		}
//		h.ServeHTTP(w, r)
//	}
//	return http.HandlerFunc(fn)
//}

func (this *App) ApplyIsXhr(c *web.C, h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("X-Requested-With") == "XMLHttpRequest" {
			c.Env["IsXhr"] = true
		} else {
			c.Env["IsXhr"] = false
		}
		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
