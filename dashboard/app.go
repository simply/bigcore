package main

import (
	"bigcore/dashboard/common/rpc"
	"bigcore/dashboard/config"
	"bigcore/dashboard/proto/engine"
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/dropbox/godropbox/errors"
	"google.golang.org/grpc"

	"path"

	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
)

var (
	PublicDir = flag.String("public_dir", "./dashboard/public/", "Folder with config files")
)

type App struct {
	onCloseQueue []func()
	Config       *config.Config
	StdLogger    *log.Logger
	RpcClients   *rpc.Clients
	Handlers     *Handlers
}

func NewApp() *App {
	return &App{}
}

func (this *App) Init() error {

	var err error
	err = this.InitConfig()
	if err != nil {
		return err
	}

	err = this.InitRpcClients()
	if err != nil {
		return err
	}

	err = this.InitElasticBasicInfo()
	if err != nil {
		return err
	}

	return nil
}

func (this *App) InitConfig() error {
	var err error
	this.Config, err = config.NewConf()
	if err != nil {
		return err
	}

	return nil
}

func (this *App) Run() error {
	models := this.NewModels()
	this.Handlers = this.NewHandlers(models)

	// Start http server
	e := echo.New()

	// Middleware
	e.Use(this.ApplyRpcClients())
	e.Use(mw.Logger())
	e.Use(mw.Recover())
	e.Use(mw.Gzip())

	e.File("/", path.Join(*PublicDir, "index.html"))

	// Serve static files
	e.Static("/js", *PublicDir+"/js")
	e.Static("/img", *PublicDir+"/img")

	e.GET("/:index/_search", this.Handlers.Rest.Search.Handle)
	e.POST("/:index/_search", this.Handlers.Rest.Search.Handle)

	listenUri := fmt.Sprintf("%s:%d", this.Config.Http.Host, this.Config.Http.Port)
	e.Start(listenUri)

	return nil
}

func (this *App) InitRpcClients() error {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial(fmt.Sprintf("%s:%d", this.Config.Engine.Host, this.Config.Engine.Port), opts...)
	if err != nil {
		return errors.Wrap(err, "")
	}

	this.RpcClients = &rpc.Clients{
		Migration:      engine.NewMigrationClient(conn),
		Search:         engine.NewSearchClient(conn),
		ProductService: engine.NewProductServiceClient(conn),
	}

	return nil
}

func (this *App) InitLog() error {
	this.StdLogger = log.New(os.Stderr, "", log.LstdFlags)

	return nil
}

func (this *App) InitElasticBasicInfo() error {

	this.RpcClients.ProductService.CreateDB(context.Background(), &engine.Account{DbName: "test"})
	docs := []*engine.Product{
		&engine.Product{
			Id:    uint64(1),
			Title: "phone very nice ",
		}, &engine.Product{
			Id:    uint64(2),
			Title: "i'm happy! ",
		}, &engine.Product{
			Id:    uint64(3),
			Title: "who stolen my phones ",
		}, &engine.Product{
			Id:    uint64(4),
			Title: "diversion in our park ",
		}, &engine.Product{
			Id:    uint64(5),
			Title: "let's dance ",
		}, &engine.Product{
			Id:    uint64(6),
			Title: "phone very nice ",
		}, &engine.Product{
			Id:    uint64(7),
			Title: "relax and call by phone to mom ",
		}, &engine.Product{
			Id:    uint64(8),
			Title: "i don't believe in phone color ",
		}, &engine.Product{
			Id:    uint64(9),
			Title: "bom bom ",
		},
	}

	ctx := context.Background()
	for _, doc := range docs {
		_, err := this.RpcClients.ProductService.Save(ctx, doc)
		if err != nil {
			return err
		}
	}

	return nil
}
