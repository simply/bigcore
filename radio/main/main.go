package main // import "github.com/nizsheanez/bigcore/radio"

import (
	"bigcore/radio"
	"bigcore/radio/common"
)

func main() {
	app := radio.NewApp()

	err := app.Init()
	if err != nil {
		common.LogError(err)
		return
	}
	err = app.Run()
	if err != nil {
		common.LogError(err)
		return
	}
}
