import {UPDATE_LIST, UPDATE_LIST_FAILED, BEFORE_REFRESH} from './constants'
import {URL, GetHeaders} from '../../config'

const LIST = `${URL.BASE}/event/list`

//each action should have the following signiture.
//  {
//     type: <type of action>,        type is required
//     payload: <the actual payload>  payload is optional. if you don't
//                                    have anything to send to reducer,
//                                    you don't need the payload. for example
//                                    newCounter action
//  }

export const init = () => {

    return (dispatch, getState) => {
        dispatch(onBeforeRefresh())
        const geo = getState().Geo
        const params = {
            location: geo.available ? geo.location : null,
        }

        fetch(LIST, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify(params),
        }).then(response => response.json())
            .then(json => dispatch(refreshSucceed(json)))
            .catch(error => dispatch(refreshFailed(LIST, error)))
            .done()
    }
}

const onBeforeRefresh = () => {
    return {
        type: BEFORE_REFRESH,
    }
}

const refreshSucceed = (data) => {
    return {
        type   : UPDATE_LIST,
        payload: {
            events: data.events,
        },
    }
}

const refreshFailed = (url, error) => {
    return {
        type   : UPDATE_LIST_FAILED,
        payload: error,
        error  : true,
        meta   : {
            url,
        },
    }
}
