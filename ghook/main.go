package main

import "fmt"

func main()  {
	server := NewServer(8080)
	server.On("Push Hook", onPush)
	server.On("Build Hook", onBuild)
	server.Run()
}

func onPush(payload interface{}) {
	fmt.Printf("%+v\n", payload)
}

func onBuild(payload interface{}) {
	fmt.Printf("%+v\n", payload)
}
