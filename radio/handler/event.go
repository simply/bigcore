package handler

import (
	"bigcore/radio/model/ds"

	"bigcore/radio/config"

	"bigcore/radio/model/event"
	"context"
)

type EventListRequest struct {
	request
	Location Location
}

type EventListResponse struct {
	Events []ds.Event `json:"events"`
}

func EventList(ctx context.Context, request *EventListRequest, response *EventListResponse) error {
	var err error

	response.Events, err = event.New(ctx).FindAll(pointFromLocation(request.Location))
	if err != nil {
		return err
	}

	return nil
}

type Location struct {
	Longitude *config.Longitude `json:"longitude"`
	Latitude  *config.Latitude  `json:"latitude"`
}

func newLocation(lon config.Longitude, lat config.Latitude) Location {
	return Location{
		Longitude: &lon,
		Latitude:  &lat,
	}
}

var pointFromLocation = func(location Location) ds.Point {
	defaultLocation := config.Get().DefaultLocation
	result := ds.NewPoint(defaultLocation.Longitude, defaultLocation.Latitude)

	if location.Latitude != nil && location.Longitude != nil {
		result = ds.NewPoint(*location.Longitude, *location.Latitude)
	}

	return result
}
