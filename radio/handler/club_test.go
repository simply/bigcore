package handler

/*
import (
	"bigcore/radio/model/ds"
	"testing"

	"bigcore/radio/config"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

type m map[string]interface{}

func mocksClubList(t *testing.T) (*gomock.Controller, *MockIClubModel, ds.Point) {
	ctrl := gomock.NewController(t)
	model := NewMockIClubModel(ctrl)

	point := ds.NewPoint(-1.3, 3.4)
	pointFromLocation = func(l Location) ds.Point {
		if l.Longitude == nil || *l.Longitude != -1.3 {
			t.Error("Longitude didn't parsed")
		}
		if l.Latitude == nil || *l.Latitude != 3.4 {
			t.Error("Latitude didn't parsed")
		}
		return point
	}

	return ctrl, model, point
}

func TestClubList(t *testing.T) {
	ctrl, model, point := mocksClubList(t)
	defer ctrl.Finish()

	//arrange
	expect := []ds.Club{{Name: "TestClubList"}}
	model.EXPECT().FindAll(point).Return(expect, nil)

	//act
	req := &ClubListRequest{
		Location: newLocation(config.Longitude(-1.3), config.Latitude(3.4)),
	}
	resp := &ClubListResponse{}
	err := NewClubHandler(model).List(req, resp)

	//assert
	assert.Nil(t, err)

	assert.Equal(t, "TestClubList", resp.Clubs[0].Name)
}
*/
