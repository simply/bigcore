package user

import (
	"bigcore/radio/model/ds"
	"gopkg.in/mgo.v2"
)

type TeacherModel struct {
	collection *collection
}

func NewTeacherModel(databaseConnection *mgo.Database) *TeacherModel {
	return &TeacherModel{
		collection: NewCollection(databaseConnection),
	}
}

func (this *TeacherModel) find(scopes ...interface{}) *teacherQuery {
	return &teacherQuery{
		Query: this.collection.Find(scopes...),
	}
}

func (this *TeacherModel) FindAll(page int, limit int) ([]ds.User, error) {
	result := make([]ds.User, limit, limit)
	err := this.find(activeTeacher).Limit(limit).All(&result)
	return result, err
}

func (this *TeacherModel) Become(idUser string) (*ds.User, error) {
	newUser := &ds.User{}
	_, err := this.find(byId(idUser), notTeacher).applyBecomeTeacher(newUser)
	return newUser, err
}
