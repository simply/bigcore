package mongo

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
)

type Config struct {
	Port      int        `yaml:"port"`
	Host      string     `yaml:"host"`
	DbName    string     `yaml:"db_name"`
	MongoPool ConfigPool `yaml:"pool"`
}

type ConfigPool struct {
	Size int `yaml:"size"`
}

var session *mgo.Session

type mongoMonitor struct {
	cnf *Config
}

type OpenCloser interface {
	Open(ctx context.Context) error
	Close(ctx context.Context)
}

type Pinger interface {
	Ping() error
	Timeout() time.Duration
}

func (this *mongoMonitor) Close(ctx context.Context) {
	if session != nil {
		session.Close()
	}
}

func (this *mongoMonitor) Open(ctx context.Context) error {
	var err error

	//mongodb://myuser:mypass@localhost:40001,otherhost:40001/mydb
	url := fmt.Sprintf("mongodb://%s:%d", this.cnf.Host, this.cnf.Port)
	session, err = mgo.Dial(url)
	if err != nil {
		return errors.Wrap(err, "can't connect to mongo")
	}

	session.SetMode(mgo.Eventual, true)

	Switch(session.DB(this.cnf.DbName))

	if err := session.Ping(); err != nil {
		return err
	}

	return nil
}

func (*mongoMonitor) Timeout() time.Duration {
	return 30 * time.Second
}

func Ping(ctx context.Context, m Pinger) <-chan error {
	resultCh := make(chan error)
	go func() {
		defer close(resultCh)
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(m.Timeout()):
				resultCh <- errors.New("Timeout")
			default:
				resultCh <- m.Ping()

				time.Sleep(1 * time.Second)
			}
		}
	}()

	return resultCh
}

func (this *mongoMonitor) Ping() error {
	if session != nil {
		return session.Ping()
	}
	return nil
}

func AutoReconnect(ctx context.Context, m *mongoMonitor) <-chan error {
	return AutoReopen(ctx, m, Ping(ctx, m))
}

func AutoReopen(ctx context.Context, m *mongoMonitor, heartBeat <-chan error) <-chan error {
	resultCh := make(chan error, 1)

	go func() {
		defer close(resultCh)
		defer m.Close(ctx)

		for err := range heartBeat {
			resultCh <- err
			if err != nil {
				m.Close(ctx)
				resultCh <- m.Open(ctx)
			}
		}
	}()

	return resultCh
}

func Open(ctx context.Context, cnf Config) (*mongoMonitor, error) {
	m := &mongoMonitor{&cnf}
	return m, m.Open(ctx)
}

func Pipe(ctx context.Context, heartBeat <-chan error) <-chan error {
	resultCh := make(chan error, 1)

	go func() {
		defer close(resultCh)

		for err := range heartBeat {
			resultCh <- err
		}
	}()

	return resultCh
}
