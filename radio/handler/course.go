package handler

import (
	"bigcore/radio/model/course"
	"bigcore/radio/model/ds"
	"context"
)

type CourseListRequest struct {
	request
}

type CourseListResponse struct {
	Courses []ds.Course `json:"courses"`
}

func CourseList(ctx context.Context, request *CourseListRequest, response *CourseListResponse) error {
	var err error
	response.Courses, err = course.New(ctx).ActiveCourses()
	if err != nil {
		return err
	}

	return nil
}

type CourseApplyRequest struct {
	request
	IdCourse string `json:"id_course"`
}

type CourseApplyResponse struct {
	AppliedCourses []ds.AppliedCourse `json:"applied_courses"`
}

/*
func CourseApply(ctx context.Context, request *CourseApplyRequest, response *CourseApplyResponse) error {
	var err error
	userId := request.Ctx.Get("user").(*ds.UserService).ID.Hex()

	if request.IdCourse == "" {
		return errors.New("invalid course id")
	}

	newCourse, err := course.New(ctx).CreateCourseInstanceToSubscribe(request.IdCourse)
	if err != nil {
		return err
	}

	response.AppliedCourses, err = user.New(ctx).SubscribeToCourse(userId, newCourse)
	if err != nil {
		return err
	}

	return nil
}
*/
