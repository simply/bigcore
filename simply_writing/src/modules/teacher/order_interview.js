import React from 'react'
import { TextButton } from '../../components'
import { StyleSheet, View } from 'react-native'

const styles = StyleSheet.create({
    orderInterview: {
        flexDirection: 'row',
        marginTop    : 7,
        marginRight  : 5,
    },
})

const OrderInterview = ({ onPress, showLoader }) =>
    <View style={styles.orderInterview}>
        <TextButton showLoader={showLoader}
                    icon="heart-o" text="Order Interview"
                    onPress={onPress}
        />
    </View>

OrderInterview.propTypes = {
    onPress   : React.PropTypes.func.isRequired,
    showLoader: React.PropTypes.bool.isRequired,
}

export default OrderInterview