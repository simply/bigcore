//go:generate mockgen -destination=worker/mocks_gen.go -package=worker bigcore/radio/worker IFetcher,ISaver,IClubFetcher
//go:generate mockgen -destination=model/mocks_gen.go -package=model bigcore/radio/model IEventDao,IUserDao,IFacebookUserDao,IClubDao,ITeacherDao,ICourse
//go:generate mockgen -destination=handler/mocks_gen.go -package=handler bigcore/radio/handler IClubModel

package main
