package config

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"bigcore/todo/db/mongo"
	"context"
	"flag"
	"github.com/spf13/viper"
)

type keyType struct{}

var key = keyType{}

func NewContext(ctx context.Context, instance Config) context.Context {
	return context.WithValue(ctx, key, instance)
}

func FromContext(ctx context.Context) Config {
	value := ctx.Value(key)
	if value == nil {
		panic("no config in context")
	}
	return value.(Config)
}

type Config struct {
	Http  Http         `yaml:"http"`
	Mongo mongo.Config `yaml:"mongo"`
}

type Http struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

var (
	config *Config
)

func Get() Config {
	return *config
}

func init() {
	viper.SetConfigType("yaml")
	viper.Set("Verbose", true)

	config = &Config{}

	flag.Parse()

	loadConfig()

	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGUSR2)
	go func() {
		for {
			<-s
			loadConfig()
			log.Println("Reloaded")
		}
	}()
}

func loadConfig() {
	err := config.parseFile("app")
	if err != nil {
		panic(err)
	}
}

func (this *Config) parseFile(file string) error {
	parser := viper.New()
	parser.SetConfigName(file) // no need to include file extension

	parser.AddConfigPath("config") // set the path of your config file

	err := parser.ReadInConfig()
	if err != nil {
		return err
	}
	err = parser.Unmarshal(this)
	if err != nil {
		return err
	}
	return nil
}
