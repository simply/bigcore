package main

import (
	"bigcore/dashboard/handler"
)

type restHandlers struct {
	Search *handler.SearchController
}

//type wsHandlers struct {
//	Search    *ws.Search
//	Migration *ws.Migration
//}

type Handlers struct {
	Rest restHandlers
}

func (this *App) NewHandlers(models *Models) *Handlers {
	return &Handlers{
		Rest: restHandlers{
			Search: handler.NewSearch(),
		},
	}
}
