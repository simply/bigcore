package test

import "bigcore/radio/model/ds"

type UserFixtures []ds.User

func (this UserFixtures) ToFixture() []interface{} {
	fixtureSet := make([]interface{}, len(this), len(this))
	for i := 0; i < len(this); i++ {
		fixtureSet[i] = this[i]
	}
	return fixtureSet
}
