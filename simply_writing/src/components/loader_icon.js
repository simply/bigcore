import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Animatable from 'react-native-animatable'
import GlobalStyles from './../styles'

const LoaderIcon = () =>
    <Animatable.View animation="rotate" iterationCount="infinite">
        <Icon name="spinner" color={GlobalStyles.blue}/>
    </Animatable.View>


LoaderIcon.propTypes = {
}

export default LoaderIcon
