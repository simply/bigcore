//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME = 'Geo'

//action types
export const BEFORE_REFRESH = `${NAME}/BEFORE_REFRESH`
export const REFRESH        = `${NAME}/REFRESH`
export const REFRESH_FAILED = `${NAME}/REFRESH_FAILED`

//as you can see above, each action is namespaced with module's name.