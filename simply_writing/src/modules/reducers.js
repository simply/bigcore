import {Login, Events, Geo, Club, Teacher, Course} from './index'
import {combineReducers} from 'redux'

export default combineReducers({
    //every modules reducer should be define here
    [Teacher.NAME]: Teacher.reducer,
    [Course.NAME] : Course.reducer,
    [Club.NAME]   : Club.reducer,
    [Geo.NAME]    : Geo.reducer,
    [Login.NAME]  : Login.reducer,
    [Events.NAME] : Events.reducer,
})

