/// <reference path="../typings/tsd.d.ts"/>

export function atoa(a: any): any[] {
    return Array.prototype.slice.call(a, 0);
}
