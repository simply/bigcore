package handler

import (
	"bigcore/radio/model/club"
	"bigcore/radio/model/ds"
	"context"
)

type request struct {
}

type ClubListRequest struct {
	request
	Location Location
}

type ClubListResponse struct {
	Clubs []ds.Club `json:"clubs"`
}

func ClubList(ctx context.Context, request *ClubListRequest, response *ClubListResponse) error {
	var err error
	response.Clubs, err = club.New(ctx).FindAll(pointFromLocation(request.Location))
	if err != nil {
		return err
	}

	return nil
}
