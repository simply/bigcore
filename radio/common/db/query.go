package db

import (
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
)

type Query struct {
	internal *mgo.Query
}

func (this *Query) All(result interface{}) error {
	return wrap(this.internal.All(result))
}

func (this *Query) Distinct(key string, result interface{}) error {
	return wrap(this.internal.Distinct(key, result))
}

func (this *Query) One(result interface{}) error {
	err := this.internal.One(result)
	if err != nil && err == mgo.ErrNotFound {
		return errors.Wrapf(err, "not fond record for query: %#v", this.internal)
	}
	return wrap(err)
}

func (this *Query) Apply(change mgo.Change, result interface{}) (*mgo.ChangeInfo, error) {
	info, err := this.internal.Apply(change, result)
	return info, wrap(err)
}

func (this *Query) MapReduce(job *mgo.MapReduce, result interface{}) (info *mgo.MapReduceInfo, err error) {
	info, err = this.internal.MapReduce(job, result)
	return info, wrap(err)
}

func (this *Query) Count() (int, error) {
	n, err := this.internal.Count()
	return n, wrap(err)
}

func (this *Query) Limit(n int) *Query {
	this.internal.Limit(n)
	return this
}

func (this *Query) Skip(n int) *Query {
	this.internal.Skip(n)
	return this
}
