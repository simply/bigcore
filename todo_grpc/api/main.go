package main

import (
	app2 "bigcore/todo_grpc/app"
	"bytes"
	"fmt"

	"github.com/pkg/errors"
	"google.golang.org/grpc/grpclog"
)

func main() {
	app := app2.New()

	err := app.Init()
	if err != nil {
		LogError(err)
		return
	}
	err = app.Run()
	if err != nil {
		LogError(err)
		return
	}
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func LogError(err error) {
	var buffer bytes.Buffer
	buffer.Write([]byte(err.Error()))
	buffer.Write([]byte("\n"))
	if err, ok := err.(stackTracer); ok {
		for _, f := range err.StackTrace() {
			buffer.Write([]byte(fmt.Sprintf("%+v\n", f)))
		}
	}
	grpclog.Errorf("%s\n", buffer.String())
}
