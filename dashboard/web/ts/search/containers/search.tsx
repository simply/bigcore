/// <reference path="../../typings/tsd.d.ts"/>

import * as React from 'react'
import { connect } from 'react-redux'

import * as SearchActions from '../actions/search'
import * as SearchTransfer from "../../dal/client/search"
import * as SearchReducer from "../reducers/search"
import Dispatch = Redux.Dispatch;
import DocList from "../components/doc_list"

import { TextField, CircularProgress } from 'material-ui'
import * as _ from "lodash"
import * as ReactDOM from 'react-dom'

interface Props {
    searchClient?: SearchTransfer.Client,
    dispatch?: Dispatch,
    search?: SearchReducer.State
}

class Search extends React.Component<Props, any> {
    constructor(props: Props) {
        super(props);
        // set initial state
        this.doSearch = _.debounce(this.doSearch, 300)
    }

    doSearch() {
        let { dispatch, searchClient } = this.props
        dispatch(SearchActions.search(searchClient))
    }

    onSearchFieldChange() {
        let { dispatch } = this.props
        let searchField = this.refs["searchField"] as TextField

        dispatch(SearchActions.changeQuery(searchField.getValue()))
        this.doSearch()
    }

    onPin(idsForPin: number[]) {
        let { dispatch } = this.props
        //TODO: mark all items as pinned throug reducer
        //dispatch(SearchActions.pin(this.props.searchClient, idsForPin))
        //TODO: POST :index_name/_pin

        //TODO: run search again, do we really need it?
        //this.doSearch()
    }

    render() {
        let searchState = this.props.search

        var SearchErrors

        if (searchState.error) {
            SearchErrors = <div>{searchState.error}</div>
        }

        return (
            <div>
                <TextField
                    floatingLabelText="Search Please"
                    ref="searchField"
                    value={searchState.query}
                    onChange={this.onSearchFieldChange.bind(this)}/>

                { searchState.inProgress ? <CircularProgress mode="indeterminate" size={0.5}/> : null }


                <br/>
                <br/>

                {SearchErrors}

                <DocList search={searchState.search} onPin={this.onPin.bind(this)}/>
            </div>
        )
    }
}

function select(state) {
    return {
        searchClient: state.ws.searchClient,
        search: state.search,
    }
}

export default connect(select)(Search);