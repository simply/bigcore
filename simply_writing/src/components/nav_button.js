import {
    StyleSheet,
    View,
    TouchableOpacity,
    Animated,
    Text,
} from 'react-native'
import React from 'react'

import Icon from 'react-native-vector-icons/Ionicons'

export default class NavButton extends React.Component {
    constructor(props) {
        super(props)
        this.tabIcons  = []
        this.tabLabels = []
    }

    componentDidMount() {
        this.setAnimationValue({value: this.props.activeTab})
        this._listener = this.props.scrollValue.addListener(({value}) => {
            this.setAnimationValue.call(this, value)
        })
    }

    setAnimationValue(value) {
        this.tabIcons.forEach((icon, i) => {
            const progress = (value - i >= 0 && value - i <= 1) ? value - i : 1
            const color    = this.iconColor(progress)
            icon.setNativeProps({
                style: {
                    color,
                },
            })

            this.tabIcons[i].setNativeProps({
                style: {
                    color,
                },
            })

        })
    }

    //color between rgb(59,89,152) and rgb(204,204,204)
    iconColor(progress) {
        const red   = 59 + (204 - 59) * progress
        const green = 89 + (204 - 89) * progress
        const blue  = 152 + (204 - 152) * progress
        return `rgb(${red}, ${green}, ${blue})`
    }

    render() {
        const tabWidth = this.props.containerWidth / this.props.tabs.length
        const left     = this.props.scrollValue.interpolate({
            inputRange: [0, 1], outputRange: [0, tabWidth],
        })

        let views = []
        this.props.tabs.map((tabSettings, i) => {
            const tabSettingsArr = tabSettings.split(';')
            const iconLabel      = tabSettingsArr[0]
            const tab            = tabSettingsArr[1]

            const color   = this.props.activeTab == i ? 'rgb(59,89,152)' : 'rgb(204,204,204)'
            const onClick = () => this.props.goToPage(i)
            const saveTab =  (icon) => { this.tabIcons[i] = icon }
            const saveLabel  = (label) => { this.tabLabels[i] = label }

            views.push(<TouchableOpacity key={tabSettings} onPress={onClick} style={styles.tab}>
                <Icon
                    color={color}
                    name={iconLabel}
                    ref={saveTab}
                    size={22}
                />
                <Text style={{color,fontSize:10}} ref={saveLabel}>{tab}</Text>
            </TouchableOpacity>)
        })

        return (
            <View>
                <View style={[styles.tabs, this.props.style ]}>
                    {views}
                </View>
                <Animated.View style={[styles.tabUnderlineStyle, { width: tabWidth }, { left } ]}/>
            </View>
        )
    }
}

NavButton.propTypes = {
    activeTab     : React.PropTypes.number,
    containerWidth: React.PropTypes.number,
    goToPage      : React.PropTypes.func,
    scrollValue   : React.PropTypes.objectOf(React.PropTypes.any),
    style         : React.PropTypes.objectOf(React.PropTypes.any),
    tabs          : React.PropTypes.arrayOf(React.PropTypes.any),
}

const styles = StyleSheet.create({
    tab              : {
        flex          : 1,
        alignItems    : 'center',
        justifyContent: 'center',
        paddingBottom : 5,
    },
    tabs             : {
        height           : 49,
        flexDirection    : 'row',
        paddingTop       : 4,
        borderWidth      : 1,
        borderBottomWidth: 0,
        borderLeftWidth  : 0,
        borderRightWidth : 0,
        borderTopColor   : 'rgba(0,0,0,0.05)',
    },
    tabUnderlineStyle: {
        position       : 'absolute',
        height         : 3,
        backgroundColor: '#3b5998',
        bottom         : 0,
    },
})

