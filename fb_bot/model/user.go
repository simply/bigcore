package model

import (
	"bigcore/fb_bot/dao"
)

type IUserDao interface {
}

type User struct {
	userDao IUserDao
}

func NewUser() *User {
	return &User{
		userDao: dao.NewUser(),
	}
}
