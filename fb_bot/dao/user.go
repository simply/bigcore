package dao

import (
	"bigcore/fb_bot/common/mongo"

	"gopkg.in/mgo.v2"
)

type User struct {
	c *mongo.Collection
}

func NewUser() *User {
	instance := &User{
		c: mongo.NewCollection("user"),
	}
	return instance
}

func (this *User) EnsureIndices() error {
	var err error

	if err != nil {
		return err
	}

	err = this.c.EnsureIndex(mgo.Index{
		Key:        []string{"status"},
		Background: true,
	})
	if err != nil {
		return err
	}

	return nil
}
