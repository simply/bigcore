// @flow

// facebook colors http://www.color-hex.com/color-palette/185
// http://devtacular.com/articles/bkonrad/how-to-style-an-application-like-facebook/
export default {
    blue: '#3b5998',
    text: {
        light : {
            color: '#999999',
        },
        medium: {
            color: '#666666',
        },
        dark  : {
            color: '#333333',
        },
    },
    link: {
        color: '#3b5998',
    },
    box : {
        gray : {
            backgroundColor: '#f7f7f7',
            borderColor    : '#cccccc',
        },
        blue : {
            backgroundColor: '#eceff6',
            borderColor    : '#d4dae8',
        },
        info : {
            backgroundColor: '#fff9d7',
            borderColor    : '#e2c822',
        },
        error: {
            backgroundColor: '#ffebe8',
            borderColor    : '#dd3c10',
        },
    },
}
