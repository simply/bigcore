package test

import (
	"bigcore/radio/common"
	"bigcore/radio/common/db"
	"bigcore/radio/config"
	"context"
	"gopkg.in/mgo.v2"
)

func CleanupDb(ctx context.Context) {
	common.Must(db.FromContext(ctx).DB(config.FromContext(ctx).Mongo.DbName).DropDatabase())
}

func GetDb(ctx context.Context) *mgo.Database {
	return db.FromContext(ctx).DB(config.FromContext(ctx).Mongo.DbName)
}

type FixtureSet interface {
	ToFixture() []interface{}
}
type Fixtures map[string]FixtureSet

func LoadFixtures(ctx context.Context, fixtures Fixtures) {
	dbInstance := GetDb(ctx)
	for collectionName, fixtureSet := range fixtures {
		collection := dbInstance.C(collectionName)
		collection.Insert(fixtureSet.ToFixture()...)
	}
}

func SetDbName(ctx context.Context, dbName string) context.Context {
	configInstance := config.FromContext(ctx)
	configInstance.Mongo.DbName = dbName
	return config.NewContext(ctx, configInstance)
}
