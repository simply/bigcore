/// <reference path="../../typings/tsd.d.ts"/>

import * as React from 'react'
import { connect } from 'react-redux'

import * as SearchActions from '../actions/search'

import { Card, CardTitle, CardActions, CardText, FontIcon } from 'material-ui'
import * as transfer from '../../dal/client/search'

import * as ReactDOM from 'react-dom'

interface Props {
    hit: transfer.SearchResponseDoc
}

class Hit extends React.Component<Props, any> {
    render() {
        const { hit } = this.props

        return (
            <div data-id={hit.id} data-pinned={hit.pinned}>
                <FontIcon className="muidocs-icon-action-home" />

                <Card initiallyExpanded={true}>
                    <CardTitle title={hit.title}/>
                    <CardText>PropTypesT
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                        Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                        Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                    </CardText>
                </Card>
                <br/>
            </div>
        )
    }
}

export default Hit