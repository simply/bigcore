package codec

import (
	"encoding/json"
	"google.golang.org/grpc"
)

type jsonCodec struct{}

func (jsonCodec) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (jsonCodec) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (jsonCodec) String() string {
	return "json"
}

func NewJsonCodec() grpc.Codec {
	return jsonCodec{}
}
