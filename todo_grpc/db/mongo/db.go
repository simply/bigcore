package mongo

import (
	"sync/atomic"

	"gopkg.in/mgo.v2"
)

var wrapper atomic.Value

var switchCh chan *mgo.Database

func init() {
	switchCh = make(chan *mgo.Database)
	go func() {
		for conn := range switchCh {
			wrapper.Store(conn)
		}
	}()
}

func db() *mgo.Database {
	return wrapper.Load().(*mgo.Database)
}

func Orders() *mgo.Collection {
	collection := db().C("orders")
	return collection
}

func Todo() *mgo.Collection {
	collection := db().C("todo")
	return collection
}

func Switch(newInstance *mgo.Database) {
	switchCh <- newInstance
}
