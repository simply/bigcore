package worker

import (
	"time"

	"bigcore/radio/model/ds"

	"bigcore/radio/config"

	"bigcore/radio/model/event"

	"bigcore/radio/model/club"
	"context"
	"log"
)

type EventsFetcherJob struct {
	PageId    string
	UpdateAll bool
}

type EventsFetcherWorker struct {
	Jobs        chan *EventsFetcherJob
	fetcher     *event.FetcherModel
	clubFetcher *club.Model
	saver       *event.Model
	ctx         context.Context
}

func NewEventsFetcherWorker(ctx context.Context) *EventsFetcherWorker {
	return &EventsFetcherWorker{
		Jobs:        make(chan *EventsFetcherJob),
		fetcher:     event.NewEventFetcherModel(ctx),
		clubFetcher: club.New(ctx),
		saver:       event.New(ctx),
		ctx:         ctx,
	}
}

func (this *EventsFetcherWorker) fetch(job *EventsFetcherJob) error {
	result, err := this.fetcher.FetchAll(job.PageId)
	if err != nil {
		return err
	}

	toUpdate := make([]*ds.Event, len(result), len(result))
	for i, fbEvent := range result {
		eventInstance := &ds.Event{}
		eventInstance.FromFbEvent(fbEvent)
		eventInstance.Status = ds.EventStatusActive
		eventInstance.FkPage = job.PageId

		//Fix empty street
		/* maybe no problem like this
		if toUpdate[i].Place.Street == "" {
			club, err := this.ClubFetcher.FindByFacebookPageId(job.PageId)
			if err != nil {
				return err
			}
			if club

			toUpdate[i].Place.Street = club.Address
		}

		//Fix empty point
		if !toUpdate[i].Place.Point.IsValid() {
			club, err := this.ClubFetcher.FindByFacebookPageId(job.PageId)
			if err != nil {
				return err
			}

			toUpdate[i].Place = club.Place
		}
		*/

		toUpdate[i] = eventInstance
	}

	//TODO: add index by id_facebook
	return this.saver.UpdateFromFacebook(toUpdate)
}

func (this *EventsFetcherWorker) Do() {
	for {
		var job *EventsFetcherJob
		select {
		case <-time.Tick(time.Hour):
		case job = <-this.Jobs:
		}

		if job == nil {
			job = &EventsFetcherJob{
				PageId: config.FromContext(this.ctx).Facebook.Simply.PageId,
			}
		}

		//TODO: must recover panic and continue loop
		err := this.fetch(job)
		if err != nil {
			log.Print(err.Error())
		}
	}
}
