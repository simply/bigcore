package handler

import (
	"bigcore/engine/model"
	"bigcore/engine/proto/engine"
	"time"
)

type Migration struct {
}

func NewMigration() *Migration {
	return &Migration{}
}

func (this *Migration) Start(request *engine.MigrationRequest, stream engine.Migration_StartServer) error {
	model := model.NewMigration()
	finishCh := model.Run()

	ticker := time.NewTicker(300 * time.Millisecond)

	for {
		select {
		case <-ticker.C:
			err := stream.Send(&engine.MigrationProgress{
				Processed:    model.Processed,
				Total:        model.Total,
				EtaInSeconds: 0,
			})

			if err != nil {
				return err
			}
		case <-finishCh:
			ticker.Stop()
			return nil
		}
	}

}
