//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME = 'Course'

//action types
export const UPDATE_LIST             = `${NAME}/UPDATE_LIST`
export const UPDATE_LIST_FAILED      = `${NAME}/UPDATE_LIST_FAILED`
export const BEFORE_REFRESH          = `${NAME}/BEFORE_REFRESH`
export const APPLY_TO_COURSE_REQUEST = `${NAME}/APPLY_TO_COURSE_REQUEST`
export const APPLY_TO_COURSE_FAIL    = `${NAME}/APPLY_TO_COURSE_FAIL`
export const APPLY_TO_COURSE_SUCCEED = `${NAME}/APPLY_TO_COURSE_SUCCEED`

//as you can see above, each action is namespaced with module's name.