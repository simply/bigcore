dashboard = $(shell pwd)/dashboard
radio = $(shell pwd)/radio
engine = $(shell pwd)/engine
transfer = $(shell pwd)/transfer
cache = $(shell pwd)/.cache
ansible = $(shell pwd)/ansible
containers = $(shell pwd)/containers
cli = docker run --rm -it
img = registry.gitlab.com/simply/bigcore

all: deps grpc

##### Deps ########

deps: dashboard-deps engine-deps ansible-deps
	go get golang.org/x/tools/cmd/goimports

deps-up: dashboard-deps-up engine-deps-up ansible-deps
	go get -u golang.org/x/tools/cmd/goimports

dashboard-frontend-container = $(cli) -v $(dashboard)/web:/src -v $(dashboard)/public:/out -v $(cache):/cache bigcore/js
radio-app-container = $(cli) -v $(radio)/app:/src -v $(radio)/public:/out -v $(cache):/cache bigcore/js

dashboard-deps: dashboard-deps-go #dashboard-deps-npm dashboard-deps-tsd
dashboard-deps-up: dashboard-deps-go-up #dashboard-deps-npm-up dashboard-deps-tsd-up

dashboard-deps-go:
	cd $(radio) && glide install --strip-vcs

dashboard-deps-go-up:
	cd $(radio) && rm -rf vendor && glide up

# Npm install
#dashboard-deps-npm:
#	$(dashboard-frontend-container) npm install --no-bin-links --loglevel warn

#dashboard-deps-npm-up:
#	$(dashboard-frontend-container) npm update --no-bin-links --loglevel warn

# TypeScript definitions install
dashboard-deps-tsd:
	$(dashboard-frontend-container) bash -c "cd /src/ts && tsd install"

#dashboard-deps-tsd-up:
#   $(dashboard-frontend-container) bash -c "cd /src/ts && tsd update"

#dashboard-frontend-cli:
#	$(dashboard-frontend-container) bash

radio-app-cli:
	$(radio-app-container) bash

engine-deps: engine-deps-go
engine-deps-up: engine-deps-go-up

engine-deps-go:
	cd $(engine) && glide install --strip-vcs

engine-deps-go-up:
	cd $(engine) && glide up --strip-vcs

ansible-deps:
	cd $(ansible) && ./extensions/setup/role_update.sh


######### Build Frontend ###########

frontend: dashboard-frontend

dashboard-frontend:
	$(dashboard-frontend-container) bash -c "webpack"

######### Go Codegeneration ###########
gen: grpc

# grpc
# TODO: i must have single .proto file for all migration purposes
grpc: engine-grpc

engine-dashboard-protos = /protos/migration.proto /protos/search.proto /protos/product.proto
dashboard-engine-protos = /protos/migration.proto /protos/search.proto /protos/product.proto

engine-grpc:
	$(cli) -v $(engine)/proto:/protobuf -v $(engine)/proto/engine:/out $(img):grpc
	$(cli) -v $(engine)/proto:/protobuf -v $(dashboard)/proto/engine:/out $(img):grpc


######### Docker Build ###########

docker-clean:
	docker ps -aq --no-trunc --filter "status=exited" | xargs docker rm
	docker images --filter 'dangling=true' -q --no-trunc | xargs docker rmi
	docker system prune -f

docker-build: images-go-grpc images-ci images-frontend images-infra images-mongo

images-up:
	docker images | grep -v bigcore | grep -v none | grep -v gitlab | grep -v REPOSITORY | grep -v lzd.co | awk '{print $$1":"$$2}' | xargs -I{} -P8 docker pull {}
	docker-compose -f containers/docker-compose.yml pull

# Specific Images
image-base:
	docker build -t $(img):base $(containers)/base
	docker push $(img):base
	docker build -t $(img):go -f $(containers)/go/Dockerfile $(containers)/go
	docker push $(img):go

images-go-grpc: image-base
	docker build -t $(img):grpc -f $(containers)/go/grpc/Dockerfile $(containers)/go/grpc
	docker push $(img):grpc

images-frontend: image-base
	docker build -t $(img):js-base -f $(containers)/js/base/Dockerfile $(containers)/js/base
	docker push $(img):js-base
	docker build -t $(img):js -f $(containers)/js/Dockerfile $(containers)/js
	docker push $(img):js

images-ci: images-go-grpc
	docker build -t $(img):ci $(containers)/ci
	docker push $(img):ci

images-infra:
	docker build -t $(img):consul $(containers)/consul
	docker push $(img):consul
	docker build -t $(img):aerospike $(containers)/as/server
	docker push $(img):aerospike
	#docker build -t $(img):amc $(containers)/as/amc
	#docker push $(img):amc

images-mongo: image-base
	docker build -t $(img):mongo $(containers)/mongo
	docker push $(img):mongo

images-es:
	docker build -t $(img):elasticsearch_base $(containers)/es/base
	docker push $(img):elasticsearch_base

	docker build -t $(img):marvel $(containers)/es/marvel
	docker push $(img):marvel

	docker build -t $(img):elasticsearch $(containers)/es/data
	docker push $(img):elasticsearch

	docker build -t $(img):kibana $(containers)/kibana
	docker push $(img):kibana



