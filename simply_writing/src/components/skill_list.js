import React from 'react'
import {View, Animated} from 'react-native'
import Skill from './skill'
import autobind from 'autobind-decorator'

export default class SkillList extends React.Component {
    static propTypes = {
        skills: React.PropTypes.arrayOf(React.PropTypes.any).isRequired,
    }

    state = {
        width: this.props.skills.map(it => new Animated.Value(it.Percent)),
    }

    componentDidMount() {
        setTimeout(this.measureRoot)
    }

    @autobind
    measureRoot() {
        this.refs.root.measure((ox, oy, width) => {
            this.handleAnimation(width)
        })
    }

    @autobind
    handleAnimation(maxWidth) {
        const {skills}  = this.props
        const timing = Animated.timing
        Animated.parallel(skills.map((it, i) => {
            const newWidth = maxWidth * (it.Percent / 100)
            return timing(this.state.width[i], {toValue: newWidth})
        })).start()
    }

    @autobind
    renderSkill(item, i) {
        const key   = `skill-${i}`
        const width = this.state.width[i]
        return <Skill key={key} label={item.Label} width={width}/>
    }

    render() {
        return (
            <View ref="root">
                {this.props.skills.map(this.renderSkill)}
            </View>
        )
    }
}

