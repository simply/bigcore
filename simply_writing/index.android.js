/**
 * React Native Webpack Starter Kit
 * https://github.com/jhabdas/react-native-webpack-starter-kit
 */
import React, {AppRegistry} from 'react-native'
import Root from './src'

AppRegistry.registerComponent('simply_writing', () => Root)