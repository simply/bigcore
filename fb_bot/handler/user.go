package handler

import (
	"bigcore/fb_bot/config"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/dropbox/godropbox/errors"
	"github.com/labstack/echo"
	"github.com/lazada/awg"
	"io/ioutil"
	"net/http"
)

//For subscribe new domain: https://developers.facebook.com/apps/1902371826656593/messenger/settings/

type User struct {
	userModel IUserModel
}

type IUserModel interface {
}

func NewUserHandler(userModel IUserModel) *User {
	return &User{
		userModel: userModel,
	}
}

type request struct {
	Ctx echo.Context
}

//{"object":"page","entry":[{
// 	"id":"182996165179717","time":1472725050101,
// 	"messaging":[{
// 		"sender":{
// 			"id":"1089369294444178"
// 		},
// 		"recipient":{
// 			"id":"182996165179717"
// 		},
// 		"timestamp":1472725049947,
// 		"message":{
// 			"mid":"mid.1472725049939:b33669e6da4b00c585",
// 			"seq":18,"text":"sd"
// 		}
// 	}]
// }]}
type HookRequest struct {
	request
	Object string  `json:"object"`
	Entry  []Entry `json:"entry"`
}

type Sender struct {
	ID string `json:"id"`
}
type Recipient struct {
	ID string `json:"id"`
}

//https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-received
type PayloadMedia struct {
	Url string `json:"url"`
}
type PayloadLocation struct {
	Lat  uint64 `json:"coordinates.lat"`
	Long uint64 `json:"coordinates.long"`
}

// TODO: implement json unmarshal, if type==location then unmarshal into PayloadLocation, otherwise in PayloadMedia, also implement TemplateAttach, see https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-echo#text
type AttachmentPayload struct {
	PayloadMedia
	PayloadLocation
}

type Attachment struct {
	Type    string            `json:"type"`    //image, audio, video, file or location
	Payload AttachmentPayload `json:"payload"` // media or location
}

type QuickReply struct {
	Payload string `json:"payload"` // Custom data provided by the app
}

// https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-echo#text
type Message struct {
	IsEcho      bool         `json:"is_echo"`
	QuickReply  *QuickReply  `json:"quick_reply"`
	AppId       uint64       `json:"app_id"`
	Metadata    string       `json:"metadata"` // developer defined metadata string
	MID         string       `json:"mid"`
	Seq         uint64       `json:"seq"`
	Text        string       `json:"text"`
	Attachments []Attachment `json:"attachments"`
}

//https://developers.facebook.com/docs/messenger-platform/webhook-reference/postback-received
type Postback struct {
	Payload string `json:"payload"`
}

// https://developers.facebook.com/docs/messenger-platform/webhook-reference/authentication
type Optin struct {
	Ref string `json:"ref"` //data-ref parameter that was defined with the entry point
}

// https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-delivered
type Delivery struct {
	Mids      []string `json:"mids"`
	Watermark uint64   `json:"watermark"`
	Seq       uint64   `json:"seq"`
}

// https://developers.facebook.com/docs/messenger-platform/webhook-reference/message-read
type Read struct {
	Watermark uint64 `json:"watermark"`
	Seq       uint64 `json:"seq"`
}

// https://developers.facebook.com/docs/messenger-platform/webhook-reference/account-linking
type AccountLinking struct {
	Status            string `json:"status"` //linked or unlinked
	AuthorizationCode string `json:"authorization_code"`
}

type MessagingEvent struct {
	Sender         Sender          `json:"sender"`
	Recipient      Recipient       `json:"recipient"`
	Timestamp      uint64          `json:"timestamp"`
	Message        *Message        `json:"message"`         // message
	Optin          *Optin          `json:"optin"`           // auth
	Delivery       *Delivery       `json:"delivery"`        // delivery confirmation
	Postback       *Postback       `json:"postback"`        // postback
	Read           *Read           `json:"read"`            // read
	AccountLinking *AccountLinking `json:"account_linking"` // read
}

type Entry struct {
	Id        string           `json:"id"`
	Time      uint64           `json:"time"`
	Messaging []MessagingEvent `json:"messaging"`
}

type Empty struct {
}

/**
query params:
hub.mode
hub.verify_token
hub.challenge
*/
func (this *User) Validate(ctx echo.Context) error {
	if ctx.QueryParam("hub.verify_token") != config.Get().Facebook.Bot.VerifyToken {
		err := errors.New("Error, wrong validation token")
		return ctx.String(echo.ErrUnauthorized.Code, err.Error())
	}

	_, err := ctx.Response().Write([]byte(ctx.QueryParam("hub.challenge")))
	if err != nil {
		return err
	}

	return nil
}

/**
It is extremely important to return a 200 OK HTTP as fast as possible.
Facebook will wait for a 200 before sending you the next message.
In high volume bots, a delay in returning a 200 can cause significant delays in
Facebook delivering messages to your webhook.
*/

func parseRequest(ctx echo.Context) (*HookRequest, error) {
	body, err := ioutil.ReadAll(ctx.Request().Body)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	ctx.Set("body", string(body))

	request := &HookRequest{}
	err = json.Unmarshal(body, request)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	request.Ctx = ctx
	return request, nil
}

//func (this *UserService) Test(request *HookRequest, response *Empty) error {
func (this *User) Test(ctx echo.Context) error {
	defer func() {
		if r := recover(); r != nil {
			ctx.Logger().Error(errors.Newf("%s", r))
		}
	}()
	request, err := parseRequest(ctx)
	if err != nil {
		request.Ctx.Logger().Error(err)
		return nil
	}

	if request.Object != "page" {
		request.Ctx.Logger().Error(errors.Newf("Only 'page' subscription supports now, %s given", request.Object))
		return nil
	}

	//TODO: probaly goroutines must be in first for
	awg := awg.AdvancedWaitGroup{}
	for _, entry := range request.Entry {
		for _, event := range entry.Messaging {

			switch true {
			case event.Optin != nil:
				awg.Add(func() error {
					return receivedAuthentication(request.Ctx, event)
				})
			case event.Message != nil:
				awg.Add(func() error {
					return receivedMessage(request.Ctx, event)
				})
			case event.Delivery != nil:
				awg.Add(func() error {
					return receivedDeliveryConfirmation(request.Ctx, event)
				})
			case event.Postback != nil:
				awg.Add(func() error {
					return receivedPostback(request.Ctx, event)
				})
			case event.AccountLinking != nil:
				awg.Add(func() error {
					return receivedAccountLink(request.Ctx, event)
				})
			default:
				request.Ctx.Logger().Error(errors.Newf("unknown messaging event. original request body: %s, parsed: %#v", ctx.Get("body").(string), request))
			}

			continue
		}
	}

	go func() {
		errors := awg.SetStopOnError(false).Start().GetAllErrors()
		for _, err := range errors {
			request.Ctx.Error(err)
		}

	}()

	return nil
}

func receivedAuthentication(ctx echo.Context, event MessagingEvent) error {
	panic("not implemented")
	return nil
}

func receivedMessage(ctx echo.Context, event MessagingEvent) error {
	ctx.Logger().Debugf("Received message: sender=%d, recipiend=%d, time=%d, message=%#v", event.Sender.ID, event.Recipient.ID, event.Timestamp, event.Message)

	switch true {
	case event.Message.IsEcho:
		// Just logging message echoes to console
		ctx.Logger().Debugf("Received echo for message %s and app %d with metadata %s", event.Message.MID, event.Message.AppId, event.Message.Metadata)
		return errors.Newf("Received echo for message %s and app %d with metadata %s", event.Message.MID, event.Message.AppId, event.Message.Metadata)
	case event.Message.QuickReply != nil:
		ctx.Logger().Debugf("Quick reply for message %s with payload %s", event.Message.MID, event.Message.QuickReply.Payload)
		sendTextMessage(ctx, event.Sender.ID, "Quick reply tapped")
	case event.Message.Text != "":
		// If we receive a text message, check to see if it matches any special
		// keywords and send back the corresponding example. Otherwise, just echo
		// the text we received.

		if event.Message.Text == "hi" {
			sendTextMessage(ctx, event.Sender.ID, "Hi, Alex")
		}

		if event.Message.Text == "How are you, man?" {
			sendTextMessage(ctx, event.Sender.ID, "Boring and cold on my server :-(")
		}

		ctx.Logger().Debugf("Nothing to say for: %s", event.Message.Text)

	/*
		switch (messageText) {
		case 'image':
			sendImageMessage(senderID);
			break;

		case 'gif':
			sendGifMessage(senderID);
			break;

		case 'audio':
			sendAudioMessage(senderID);
			break;

		case 'video':
			sendVideoMessage(senderID);
			break;

		case 'file':
			sendFileMessage(senderID);
			break;

		case 'button':
			sendButtonMessage(senderID);
			break;

		case 'generic':
			sendGenericMessage(senderID);
			break;

		case 'receipt':
			sendReceiptMessage(senderID);
			break;

		case 'quick reply':
			sendQuickReply(senderID);
			break;

		case 'read receipt':
			sendReadReceipt(senderID);
			break;

		case 'typing on':
			sendTypingOn(senderID);
			break;

		case 'typing off':
			sendTypingOff(senderID);
			break;

		case 'account linking':
			sendAccountLinking(senderID);
			break;

		default:
			sendTextMessage(senderID, messageText);
		}
	*/
	case event.Message.Attachments != nil:
		sendTextMessage(ctx, event.Sender.ID, "Message with attachment received")
	default:
		return errors.Newf("Unsupported message format. Request Body: %s, Parsed: %#v", ctx.Get("body").(string), event.Message)
	}
	return nil
}

func sendTextMessage(ctx echo.Context, recipientId string, text string) error {

	messageData := map[string]interface{}{
		"recipient": map[string]interface{}{
			"id": recipientId,
		},
		"message": map[string]interface{}{
			"text":     text,
			"metadata": "DEVELOPER_DEFINED_METADATA",
		},
	}

	return callSendAPI(ctx, messageData)
}

type SendResult struct {
	MessageId   string `json:"message_id"`
	RecipientId string `json:"recipient_id"`
}

func callSendAPI(ctx echo.Context, messageData map[string]interface{}) error {
	url := fmt.Sprintf("https://graph.facebook.com/v2.6/me/messages?access_token=%s", config.Get().Facebook.Bot.PageToken)
	//TODO: stream marshaling
	response, err := json.Marshal(messageData)
	if err != nil {
		return errors.Wrap(err, "")
	}

	resp, err := http.Post(url, "application/json", bytes.NewReader(response))
	if err != nil {
		return errors.Wrap(err, "")
	}

	if resp.StatusCode != 200 {
		return errors.Newf("Can't send message: %#v", resp)
	}

	sendResult := SendResult{}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrap(err, "")
	}

	err = json.Unmarshal(body, &sendResult)
	if err != nil {
		return errors.Wrap(err, "")
	}

	// If successful, we'll get the message id in a response
	if sendResult.MessageId != "" {
		ctx.Logger().Debugf("Successfully sent message %s to recipient %s", sendResult.MessageId, sendResult.RecipientId)
	}

	return nil
}

/*
function callSendAPI(messageData) {
  request({
    uri:
    qs: ,
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;

      if (messageId) {
        console.log("Successfully sent message with id %s to recipient %s",
          messageId, recipientId);
      } else {
      console.log("Successfully called Send API for recipient %s",
        recipientId);
      }
    } else {
      console.error(response.error);
    }
  });
}
*/

func receivedDeliveryConfirmation(ctx echo.Context, event MessagingEvent) error {
	ctx.Logger().Debug("Delivered!")
	return nil
}
func receivedPostback(ctx echo.Context, event MessagingEvent) error {
	panic("not implemented")
	return nil
}
func receivedMessageRead(ctx echo.Context, event MessagingEvent) error {
	panic("not implemented")
	return nil
}

func receivedAccountLink(ctx echo.Context, event MessagingEvent) error {
	panic("not implemented")
	return nil
}
