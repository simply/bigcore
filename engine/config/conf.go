package config

import (
	"flag"
	"github.com/dropbox/godropbox/errors"
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

var (
	Dir = flag.String("conf_dir", "./engine/config/", "Folder with config files")
)

type Config struct {
	Rpc     *Rpc     `yaml:"rpc"`
	Db      *Db      `yaml:"db"`
	Elastic *Elastic `yaml:"elastic"`
}

type Rpc struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

type ElasticNode struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

type Elastic struct {
	Pool []ElasticNode `yaml:"pool"`
}

type DbItem struct {
	Name string `yaml:"name"`
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type Db struct {
	Read  []*DbItem `yaml:"read"`
	Write []*DbItem `yaml:"write"`
}

func NewConf() (*Config, error) {
	conf := &Config{}

	err := conf.parseFiles([]string{*Dir + "app.yaml", *Dir + "dev.yaml"})
	if err != nil {
		return nil, err
	}

	return conf, nil
}

func (this *Config) parseFiles(files []string) error {
	var (
		buf []byte
		err error
	)

	for _, file := range files {
		_, err = os.Stat(file)
		if err != nil {
			if os.IsNotExist(err) {
				continue
			}

			return errors.Wrap(err, "")
		}

		buf, err = ioutil.ReadFile(file)
		if err != nil {
			return errors.Wrap(err, "")
		}
		err = yaml.Unmarshal(buf, this)
		if err != nil {
			return errors.Wrap(err, "")
		}
	}

	return nil
}
