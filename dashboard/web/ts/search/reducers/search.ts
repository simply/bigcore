import { SEARCH_DO, SEARCH_DO_REQUEST, SEARCH_DO_RECIEVE, SEARCH_DO_FAIL } from '../actions/search'
import { SEARCH_CHANGE_QUERY, SEARCH_CHANGE_FILTER } from '../actions/search'
import { SEARCH_PIN } from '../actions/search'

import * as transfer from '../../dal/client/search'

export class State {
    search: transfer.SearchResponse
    inProgress: boolean
    error: any
    query: string
}

export function Reducer(state: State = new State, action: any = false): State {
    switch (action.type) {
        case SEARCH_DO:
            return state
        case SEARCH_DO_REQUEST:
            return Object.assign(new State, state, {
                inProgress: true
            })
        case SEARCH_DO_RECIEVE:
            console.log("reducer: ", action.payload)

            return Object.assign(new State, state, {
                search: action.payload,
                inProgress: false
            })
        case SEARCH_DO_FAIL:
            return Object.assign(new State, state, {
                error: action.payload,
                inProgress: false
            })
        case SEARCH_CHANGE_QUERY:
            return Object.assign(new State, state, {
                query: action.payload
            })
        default:
            return state
    }
}
