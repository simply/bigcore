package todo

import (
	"bigcore/todo/db"
	"context"
)

type IUser interface {
	Exists(ctx context.Context, userId string) (bool, error)
}

type IOrder interface {
	Create(ctx context.Context, order *db.OrderType) error
	Count(ctx context.Context) (int, error)
}
