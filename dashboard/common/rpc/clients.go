package rpc

import (
	"bigcore/dashboard/proto/engine"
)

type Clients struct {
	Migration      engine.MigrationClient
	Search         engine.SearchClient
	ProductService engine.ProductServiceClient
}
