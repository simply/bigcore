package db

import (
	"context"
	"gopkg.in/mgo.v2"
)

type keyType struct{}

var key = keyType{}

func NewContext(ctx context.Context, db *mgo.Session) context.Context {
	return context.WithValue(ctx, key, db)
}

func FromContext(ctx context.Context) *mgo.Session {
	value := ctx.Value(key)
	if value == nil {
		panic("no session in context")
	}
	return value.(*mgo.Session)
}
