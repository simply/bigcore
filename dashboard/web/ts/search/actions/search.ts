/// <reference path="../../typings/tsd.d.ts"/>

export const SEARCH_DO         = "SEARCH_DO"
export const SEARCH_DO_REQUEST = "SEARCH_DO_REQUEST"
export const SEARCH_DO_RECIEVE = "SEARCH_DO_START"
export const SEARCH_DO_FAIL    = "SEARCH_DO_FAIL"

export const SEARCH_CHANGE_QUERY = "SEARCH_CHANGE_QUERY"
export const SEARCH_CHANGE_FILTER = "SEARCH_CHANGE_FILTER"

export const SEARCH_PIN = "SEARCH_PIN"

import * as redux from 'redux'
import * as transfer from '../../dal/client/search'

//TODO: need to remove dependency from root reducer, otherwise we will not able to extract component from project
import * as root from '../../app/reducers/root'

export function search(searchClient: transfer.Client): any {
    return (dispatch: redux.Dispatch, getStore: Function): any => {
        dispatch(request())

        let store = <root.State>getStore()
        var searchRequest = <transfer.SearchRequest>{
            query: store.search.query,
        }

        searchClient.search(searchRequest).then(onRecieve(dispatch)).catch(fail(dispatch))

        return
    }
}

export function pin(searchClient: transfer.Client, idsForPin: number[]): any {
    return (dispatch: redux.Dispatch, getStore: Function): any => {
        //dispatch(request())

        let store = <root.State>getStore()
        var searchRequest = <transfer.SearchRequest>{
            query: store.search.query,
        }

        searchClient.search(searchRequest).then(onRecieve(dispatch)).catch(fail(dispatch))

        return
    }
}

function onRecieve(dispatch: redux.Dispatch): transfer.SearchResolve {
    return (response: transfer.SearchResponse): void => {
        dispatch(receive(response))
    }
}

function onFail(dispatch: redux.Dispatch): any {
    return (reason: any): void => {
        dispatch(fail(reason))
    }
}

export function request(): any {
    return {
        type: SEARCH_DO_REQUEST
    }
}

function receive(response: transfer.SearchResponse): any {
    return {
        type:    SEARCH_DO_RECIEVE,
        payload: response
    }
}

function fail(reason: any): any {
    return {
        type:    SEARCH_DO_FAIL,
        payload: reason
    }
}

function changeQueryRequestInternal(query: string): any {
    return {
        type:    SEARCH_CHANGE_QUERY,
        payload: query
    }
}

export function changeQuery(query: string): any {
    return {
        type:    SEARCH_CHANGE_QUERY,
        payload: query
    }
}

