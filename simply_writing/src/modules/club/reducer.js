import {ListView} from 'react-native'
import {handleActions} from 'redux-actions'
import {REFRESH_LIST, REFRESH_LIST_FAILED, BEFORE_REFRESH} from './constants'

const initialState = {
    loaded    : false,
    clubs     : [],
    dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
}

//you can do better here, I was just showing that you need to make a new copy
//of state. It is ok to deep copy of state. It will prevent unseen bugs in the future
//for better performance you can use immutableJS

//handleActions is a helper function to instead of using a switch case statement,
//we just use the regular map with function state attach to it.

export default handleActions({
    [REFRESH_LIST]       : (state, {payload}) => {
        return {
            ...state,
            loaded    : true,
            clubs     : payload.clubs,
            dataSource: state.dataSource.cloneWithRows(payload.clubs),
        }
    },
    [REFRESH_LIST_FAILED]: (state) => {
        return state
    },
    [BEFORE_REFRESH]     : (state) => {
        return state
    },
}, initialState)