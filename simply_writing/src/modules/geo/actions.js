import {BEFORE_REFRESH, REFRESH, REFRESH_FAILED} from './constants'

export const refresh = () => {
    return (dispatch) => {
        dispatch(onBeforeRefresh())

        navigator.geolocation.getCurrentPosition(
            location => dispatch(onRefresh(location)),
            error => dispatch(onRefreshFailed(error)),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        )


        //TODO: add watch
        //TODO: gc with navigator.geolocation.clearWatch(this.watchID)
    }
}

export const onBeforeRefresh = () => {
    return {
        type: BEFORE_REFRESH,
    }
}

export const onRefresh = (location) => {
    return {
        type   : REFRESH,
        payload: {
            location  : location.coords,
            lastUpdate: location.timestamp,
        },
    }
}

export const onRefreshFailed = (error) => {
    return {
        type   : REFRESH_FAILED,
        payload: error,
        error  : true,
    }
}
