import React from 'react'
import {
    StyleSheet,
    View,
    ScrollView,
} from 'react-native'
import {connect} from 'react-redux'
import * as eventActions from './../events/actions'
import * as courseActions from './../course/actions'
import * as clubActions from './../club/actions'
import * as geoActions from './../geo/actions'
import * as loginActions from './../login/fb_actions'
import * as teacherActions from './../teacher/actions'

import {NavButton} from './../../components'
import ScrollableTabView from 'react-native-scrollable-tab-view'

import {Login, Course, Club, Teacher} from '../../modules'
import Styles from '../../styles'
import autobind from 'autobind-decorator'

import type {State} from "./../index"

const styles = StyleSheet.create({
    navigation: {},
    container : {
        flex           : 1,
        flexDirection  : 'column',
        backgroundColor: '#f7f7f7',
    },
    login     : {
        flex          : 1,
        alignItems    : 'center',
        justifyContent: 'center',
    },
    header    : {
        flexDirection  : 'row',
        justifyContent : 'center',
        backgroundColor: Styles.blue,
        height         : 25,
    },
    tab       : {
        flex: 1,
    },
})


const mapState   = (state: State) => ({
    authenticated: state.Login.authenticated,
})
const mapActions = (dispatch: Dispatch) => ({
    onChooseCourse  : () => dispatch(courseActions.refresh()),
    onChooseEvents  : () => dispatch(eventActions.init()),
    onChooseClubs   : () => dispatch(clubActions.refresh()),
    onChooseTeachers: () => dispatch(teacherActions.refresh()),
    doLogin         : () => dispatch(loginActions.doLogin()),
    geoRefresh      : () => dispatch(geoActions.refresh()),
})

@connect(mapState, mapActions)
export default class App extends React.Component {
    static displayName = 'App'
    static propTypes   = {
        authenticated: React.PropTypes.bool,

        onChooseEvents  : React.PropTypes.func,
        onChooseCourse  : React.PropTypes.func,
        onChooseClubs   : React.PropTypes.func,
        onChooseTeachers: React.PropTypes.func,
        doLogin         : React.PropTypes.func,
        loginRefresh    : React.PropTypes.func,
    }

    constructor(props) {
        props.geoRefresh()

        props.doLogin()

        super(props)
    }

    //TODO: implement all buttons
    @autobind
    onChangeTab({i:number}) {
        const {onChooseCourse, onChooseClubs, onChooseTeachers} = this.props
        i == 0 && onChooseCourse()
        i == 1 && onChooseTeachers()
        // i == 2 && onChooseEvents()
        i == 3 && onChooseClubs()
    }

    render() {
        //TODO: render preload while no state.Geo.initialized and state.Login.initialized
        const {authenticated} = this.props

        //TODO: when i do restart app, Button remember token but my app doesn't remember about authenticated
        //TODO: i think need to make it async and do special dispatch for fetching data from button
        const loginButton = <Login.Login />
        if (!authenticated) {
            return (
                <View style={styles.container}>
                    <View style={styles.login}>
                        {loginButton}
                    </View>
                </View>
            )
        }

        this.onChangeTab({i: 0})

        //TODO: header must contain tab name
        const renderTabBar = () => <NavButton />
        return (
            <View style={styles.container}>
                <View style={styles.header}/>

                <ScrollableTabView initialPage={0} onChangeTab={this.onChangeTab}
                                   renderTabBar={renderTabBar} tabBarPosition="bottom"
                >
                    <ScrollView contentContainerStyle={styles.tab} tabLabel="ios-school;Courses">
                        <Course.Module />
                    </ScrollView>
                    <ScrollView contentContainerStyle={styles.tab} tabLabel="ios-person;Teachers">
                        <Teacher.Module />
                    </ScrollView>
                    <ScrollView contentContainerStyle={styles.tab} tabLabel="ios-people;Clubs">
                        <Club.Module />
                    </ScrollView>
                    <ScrollView contentContainerStyle={styles.tab} style={styles.tabView}
                                tabLabel="ios-planet;Something"
                    >
                        <View />
                    </ScrollView>
                    <ScrollView contentContainerStyle={styles.tab} tabLabel="ios-menu;Settings">
                        {loginButton}
                    </ScrollView>
                </ScrollableTabView>
            </View>
        )
    }
}
