import {handleActions} from 'redux-actions'
import {BEFORE_REFRESH, REFRESH, REFRESH_FAILED} from './constants'


/*
 Position:

 accuracy: 50
 altitude: 0
 altitudeAccuracy: -1
 heading: -1
 latitude: 37.33165083
 longitude: -122.03029752
 speed: 1.28
 timestamp: 1465457944641.621
 */

const initialState = {
    initialized : false, //already tired to fetch location, but something broken, app must work without location knowledge
    available   : false, //is
    location    : {},
    lastUpdate  : 0,
    watchEnabled: false,
    watchId     : 0,
}

//you can do better here, I was just showing that you need to make a new copy
//of state. It is ok to deep copy of state. It will prevent unseen bugs in the future
//for better performance you can use immutableJS

//handleActions is a helper function to instead of using a switch case statement,
//we just use the regular map with function state attach to it.

export default handleActions({
    [REFRESH]       : (state, {payload}) => {
        return {
            ...state,
            initialized: true,
            available  : true,
            location   : payload.location,
            lastUpdate : payload.lastUpdate,
        }
    },
    [BEFORE_REFRESH]: (state) => {
        return state
    },
    [REFRESH_FAILED]: (state, {payload}) => {
        return {
            ...state,
            initialized: true,
            available  : false,
            error      : payload,
        }
    },
}, initialState)