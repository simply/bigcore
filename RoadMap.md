Full modeling of: https://swiftype.com/documentation/searching#faceting

# V0.3
[] Remove --conf_dir flag
[] Deploy: https://gitlab.com/help/ci/ssh_keys/README.md
[] Move from echo to https://github.com/pressly/chi


[x] [tsd] Replace tsd to typings https://github.com/typings/typings
[x] [fe] Move ts from Client to Dashboard, Remove client
[x] [app] Tests for everything! .ts, .go


[] [cloudflare] learn, think how to cache REST, start to use, go client, etc
    [] DDoS protection for static subdomain - http://habrahabr.ru/post/245165/
    [] Use it for SimplyEnglish https://www.digitalocean.com/community/tutorials/how-to-mitigate-ddos-attacks-against-your-website-with-cloudflare
    [] Improve connection btw CF and Nginx https://support.cloudflare.com/hc/en-us/articles/212794707-General-Best-Practices-for-Load-Balancing-with-CloudFlare
    [] Read CF articles https://support.cloudflare.com/hc/en-us and blog https://blog.cloudflare.com/ /;";.l567/;'

[] [dashboard] Show list of documents per url
[] [engine] We must have external_id and id in products table

[] [concept] Does it make sense to implement "drop and replace" ElasticSearch interface ?


# V0.4 - 2015.02.28 - Pin items #

[] [engine] Split engine and search to different microservices
[] [dashboard] Avoid double requests and race condition
[] [dashboard] Pin functionality
[] [dashboard] https://github.com/rackt/redux-router or https://github.com/jlongster/redux-simple-router

[] [fw] Generate .ts transfers
[] [engine] Write parallel indexer

[] [server] aliases/indices with version from db

[] [pg/tidb] Simple configuration table (can i configure memory usage?)

[] [client] /products - POST changed ids
[] [client] /products/:id - GET by id
[] [server] fake product endpoint for update

# V0.4 - 2015.03.07 - MultiLingual Monster, migration of index #

[] [app] While migrations Updates should be sent to old and new version of index.
What if we sent update, and then getting old records from old index?
[] [app] Engine runing full data migration(scan with limit/offset) and return progress/estimation to client.

[] 404 json response
[] any error/panic json response
[] ES mapping/analysys/languages
[] [client] move lzd search logic
[] [client] Port search model from API
[] [client] /products - DELETE
[]

# V0.5 2015.03.14 Security Token, Blood Seeker #

[] [client] Move from dragula to react-dnd. do we really need it?
[] [engine] if es or db down, still need to run but try to reconnect 
[] [engine] improve x100 times throughput of product/save 
[] [client] /status - more status, statistic
[] [client] gracefull shutdown without loosing updated product ids

# V0.6 - 2015.03.21 - Crawling, Black Pearl #

[] [crawl] Support relative links
[] [crawl] Support links without scheme: //lazada.vn/
[] [crawl] Aerospike or Mysql for detect link duplicates
[] [crawl] split errors to errors of indexing, and errors of application. 1-st error save to DB. On 2-nd send msg back to nsq.

[] [monitoring] https://github.com/rcrowley/go-metrics + graphite + something else

# V0.7 - 2015.03.28 - Stable Joe #

[]

# V0.8 - 2015.04.07 - Dialectic #

[]

# V0.9 - 2015.04.14 - Growing Core #

[]

# V1.0 - 2015.04.14 - Big Party #


# V0.2 - 2015.12.30 - Search from browser, CRUD with tests #

[x] [fw] update OsX docker tools
[x] [fw] rebuild all docker containers with last versions
[x] [fw] https://christianalfoni.github.io/react-webpack-cookbook/Hot-loading-components.html
[x] [fw] https://github.com/cujojs/when/blob/master/docs/installation.md#browser-environments-via-browserify

[x] [fw] Redux http://rackt.org/redux/docs/basics/UsageWithReact.html
[x] [fw] https://christianalfoni.github.io/react-webpack-cookbook/Hot-loading-components.html hot reloading

[x] [app] User doing WS request for update index version, Dashboard sending request to engine (with stream response)
[x] [crawl] https://github.com/sjwhitworth/plod - nsqd based distributed crawler, and push to s
[x] [ui] https://github.com/callemall/material-ui
[x] [engine] move Save to engine
[x] [engine] move Search to engine
[x] [app] use https://github.com/enginyoyen/ansible-best-practises/ with vagrant
[x] [ansible] install elastic 2 as apt package

[x] [dashboard] Implement https://github.com/raphael/goa endpoint and try to use it from client, no reason to have WS support, if swift doesn't, maybe it's useless feature.
    [x] adopt js generator to ts - generate definitions or smth.
[x] [engine] implement Mget from Postgres in engine

[x] [dashboard] Search input field with auto search on typing
[x] [dashboard] Switch to REST

[x] [fw] https://github.com/borisyankov/DefinitelyTyped/blob/master/autobahn/autobahn-tests.ts
[x] [ws] Try http://blog.jce.io/showing-off-go-and-websockets/
[x] [es] data=false low memory frontend-nodes (64mb)
[x] [es] nginx in front of front-nodes (64mb)
[x] [es] main cluster (3*64mb)
[x] [es] remote marvel node (64mb)
[x] [as] Small instance in docker with file-based cache (16mb)

