/// <reference path="../../typings/tsd.d.ts"/>

export type ProgressResolve = (r: ProgressResponse) => void
export type StartResolve = (r: StartResponse) => void

export interface IClient {
    start(request: StartRequest): Promise<StartResponse>
    onProgress(cb: ProgressResolve): void
}

//"processed": progress.Processed, "total": progress.Total, "eta": progress.EtaInSeconds,
export class ProgressResponse {
    Progress: number
    EtaInSeconds: number
}

export class StartRequest {
    Start: boolean
}

export class StartResponse {
    AlreadyStarted: boolean
}

export class Client implements IClient {
    constructor() {}

    start(request: StartRequest): Promise<StartResponse> {
        return new Promise<StartResponse>((resolve, reject) => {
            //axios.get('Migration.Start', [request]).then((res) => {
            //    resolve(new StartResponse)
            //})
        })
    }

    onProgress(cb: ProgressResolve): void {
        //this.session.subscribe('Migration.Start.stream', (args: any[]): void => {
        //    cb(<ProgressResponse>args[0])
        //})
    }
}