package handler

import (
	"net/http"

	"github.com/labstack/echo"

	"bigcore/dashboard/common/rpc"
	"bigcore/dashboard/proto/engine"
	"context"

	"github.com/dropbox/godropbox/errors"
)

type Search struct {
	// array of documents
	Docs []*Doc `json:"docs"`
}

// Doc type
type Doc struct {
	// Document title
	Id     uint64 `json:"id"`
	Title  string `json:"title"`
	Pinned bool   `json:"pinned"`
}

// SearchController implements the index resource.
type SearchController struct{}

// NewIndexController creates a index controller.
func NewSearch() *SearchController {
	return &SearchController{}
}

// Search runs the search action.
func (this *SearchController) Handle(ctx echo.Context) error {
	rpcClients := ctx.Get("RpcClients").(*rpc.Clients)

	//TODO: get pin id's from Aerospike
	searchRequest := &engine.SearchRequest{}
	err := ctx.Bind(searchRequest)
	if err != nil {
		return errors.Wrap(err, "")
	}

	resp, err := rpcClients.Search.Search(context.Background(), searchRequest)
	if err != nil {
		return errors.Wrap(err, "")
	}

	//TODO: Search must return empty array
	if resp.Ids == nil {
		resp.Ids = make([]int64, 0)
	}
	mget, err := rpcClients.ProductService.Mget(context.Background(), &engine.MgetRequest{
		Ids: resp.Ids,
	})
	if err != nil {
		return errors.Wrap(err, "")
	}

	res := &Search{
		Docs: make([]*Doc, len(mget.Hits), len(mget.Hits)),
	}

	for i, doc := range mget.Hits {
		res.Docs[i] = &Doc{
			Id:     doc.Id,
			Title:  doc.Title,
			Pinned: false,
		}
	}

	return ctx.JSON(http.StatusOK, res)
}
