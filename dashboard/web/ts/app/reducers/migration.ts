import { MIGRATION_START, MIGRATION_PROGRESS, MIGRATION_FINISH } from '../actions/migration'

export class State {
    progress: number
}

export function Reducer(state: State = new State, action: any = false): State {
    switch (action.type) {
        case MIGRATION_START:
            return state
        case MIGRATION_PROGRESS:
            if (state.progress == action.payload.Progress) {
                return state
            }
            return Object.assign(new State, state, {
                progress: action.payload.Progress
            })
        default:
            return state
    }
}
