// @flow
import { Provider } from 'react-redux'
import React from 'react'
import App from './modules/app/app'

import createStore from './create_store'
const store = createStore()

const Root = () =>
    <Provider store={store}>
        <App />
    </Provider>

export default Root