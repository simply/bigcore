import React from 'react'
import {StyleSheet, Text, ListView} from 'react-native'
import {Card} from '../../components'

//TODO: Use ListView with refresh
const styles = StyleSheet.create({
    item: {},
})

//TODO: make clickable address
const ListItem = ({club}) =>
    <Card key={`club-${club.IdClub}`}>
        <Text style={styles.item}>{club.Name}</Text>
    </Card>

ListItem.propTypes = {
    club: React.PropTypes.any.isRequired,
}

const List = ({dataSource}) =>
    <ListView
        dataSource={dataSource}
        renderRow={(club) => <ListItem club={club} />}
    />

List.propTypes = {
    dataSource: React.PropTypes.instanceOf(ListView.DataSource).isRequired,
}

export default List