package user

/*
import (
	"strconv"
	"time"

	"strings"

	"bigcore/radio/model/ds"

	"fmt"

	"bigcore/radio/model/user/facebook"

	"context"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)
type AuthByFacebookDataFromClient struct {
	Token               string
	UserId              uint64
	TokenExpirationDate time.Time
	Permissions         []string
}

func ParseFacebookHeaders(header http.Header) (AuthByFacebookDataFromClient, error) {
	var err error
	dataFromClient := AuthByFacebookDataFromClient{
		Token:       header.Get("Fb-Token"),
		Permissions: strings.Split(header.Get("Fb-Permissions"), ","),
	}

	expiration := header.Get("Fb-Token-Expiration")
	if expiration == "" {
		return AuthByFacebookDataFromClient{}, errors.New("Token Expiration must not be empty")
	}

	dataFromClient.TokenExpirationDate, err = ds.ParseFacebookTime(time.RFC3339, expiration)
	if err != nil {
		return AuthByFacebookDataFromClient{}, err
	}

	fbUserIdStr := header.Get("Fb-UserService-Id")
	dataFromClient.UserId, err = strconv.ParseUint(fbUserIdStr, 10, 0)
	if err != nil {
		return AuthByFacebookDataFromClient{}, errors.Wrapf(err, "can't parse %+v", fbUserIdStr)
	}

	return dataFromClient, nil
}

type FacebookApiResponse struct {
	Id    string
	Email string
}

type IdentityModel struct {
	facebookUserDao *facebook.UserDao
	userCollection  *collection
}

func NewFacebookIdentityModel(ctx context.Context) *IdentityModel {
	return &IdentityModel{
		facebookUserDao: facebook.New(ctx),
		userCollection:  newCollection(ctx),
	}
}

func (this *IdentityModel) find(scopes ...interface{}) *query {
	return &query{
		Query: this.userCollection.Find(scopes...),
	}
}

// if token is valid, not expired and already authorized by Facebook
func (this *IdentityModel) validateToken(clientData AuthByFacebookDataFromClient, serverData ds.OAuthFacebook) bool {
	if serverData.Token != clientData.Token {
		return false
	}

	if !serverData.TokenExpirationDate.Before(time.Now()) {
		return false
	}

	return true
}

func (this *IdentityModel) SetOAuth(idUser string, data ds.OAuthFacebook) error {
	_, err := this.find(byId(idUser)).applyFacebookOAuth(data)
	return err
}

//if token is valid, then login is done
//if token was denied by
func (this *IdentityModel) Login(dataFromClient AuthByFacebookDataFromClient, user *ds.UserService) error {
	if this.validateToken(dataFromClient, user.OAuth.Facebook) {
		if !user.OAuth.Facebook.TokenAuthorized {
			return errors.New("token was force expired")
		}

		return nil
	}

	err := this.facebookUserDao.ValidateToken(dataFromClient.UserId, dataFromClient.Token)
	if err != nil {
		return err
	}

	user.OAuth.Facebook.TokenExpirationDate = dataFromClient.TokenExpirationDate
	user.OAuth.Facebook.IdUserOnFacebook = dataFromClient.UserId
	user.OAuth.Facebook.Token = dataFromClient.Token
	user.OAuth.Facebook.TokenAuthorized = true
	return this.SetOAuth(user.ID.Hex(), user.OAuth.Facebook)
}

func (this *IdentityModel) Register(dataFromClient AuthByFacebookDataFromClient) (*ds.UserService, error) {

	result, err := this.facebookUserDao.Fields(dataFromClient.UserId, dataFromClient.Token, []string{
		"id", "email", "first_name", "last_name", "picture",
	})
	if err != nil {
		return nil, err
	}

	instance := ds.UserService{
		ID:        bson.NewObjectId(),
		Email:     result.Get("email").(string),
		FirstName: result.Get("first_name").(string),
		LastName:  result.Get("last_name").(string),
		Picture:   result.Get("picture").(map[string]interface{})["data"].(map[string]interface{})["url"].(string),
		Profile: ds.Profile{
			Guest: ds.Guest{
				Status: ds.GUEST_STATUS_ACTIVE,
			},
		},
		OAuth: ds.OAuth{
			Facebook: ds.OAuthFacebook{
				Token:               dataFromClient.Token,
				IdUserOnFacebook:    dataFromClient.UserId,
				TokenExpirationDate: dataFromClient.TokenExpirationDate,
				TokenAuthorized:     true,
			},
		},
	}

	err = this.userCollection.Insert(instance)
	if err != nil {
		fmt.Printf("2%#v\n", err)
		return nil, err
	}

	err = this.SyncDataFromFacebook(instance.ID.Hex())
	if err != nil {
		fmt.Printf("3%#v\n", err)
		return nil, err
	}

	return nil, nil
}

func getAvailableFields(permissions []string) []string {
	//TODO: add permissions for birthday and other things https://developers.facebook.com/docs/facebook-login/permissions

	fields := []string{"id"}
	for _, permission := range permissions {
		switch permission {
		case "public_profile":
			fields = append(fields,
				"about", "work", "age_range", "bio", "picture", "birthday",
				"context", "cover", "currency", "devices", "gender",
				"languages", "last_name", "education", "favorite_athletes",
				"favorite_teams", "first_name", "hometown", "inspirational_people",
				"interested_in", "link", "locale", "location", "relationship_status",
				"religion", "sports", "test_group", "website", "timezone",

				//"labels",
				//"token_for_business",
				//"admin_notes",

			)
		case "email":
			fields = append(fields, "email")
		case "user_friends":

		}
	}
	return fields
}

func (this *IdentityModel) SyncDataFromFacebook(userId string) error {
	instance := &ds.UserService{}
	err := this.find(byId(userId)).One(instance)
	if err != nil {
		fmt.Printf("4%#v\n", err)
		return err
	}

	fbProfile := instance.OAuth.Facebook
	if fbProfile.IdUserOnFacebook <= 0 {
		return errors.Errorf("invalide user id on Facebook: %d", fbProfile.IdUserOnFacebook)
	}

	result, err := this.facebookUserDao.Fields(fbProfile.IdUserOnFacebook, fbProfile.Token,
		getAvailableFields(fbProfile.Permissions))
	if err != nil {
		fmt.Printf("5%#v\n", err)
		return err
	}

	_, err = this.find(byId(userId)).applyFacebookProfile(result)
	return err
}

func (this *IdentityModel) FindByFacebookId(userIdOnFacebook uint64) (*ds.UserService, error) {
	instance := &ds.UserService{}
	err := this.find(byFbId(userIdOnFacebook)).One(instance)
	if err != nil && errors.Cause(err) == mgo.ErrNotFound {
		return nil, nil
	}

	return instance, err
}
*/
