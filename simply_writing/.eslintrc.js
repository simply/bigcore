module.exports = {
    env: {
        es6: true,
        node: true
    },
    extends: 'eslint:recommended',
    globals: { "fetch": true, "navigator": true, "alert":true  },
    parserOptions: {
        ecmaVersion: 7,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true,
            experimentalObjectRestSpread: true,
        }
    },
    'parser': 'babel-eslint',
    plugins: [
        "flow-vars",
        'react',
        'immutable',
    ],
    rules: {
        'indent': ['error', 4],
        'quotes': ['error', 'single'],
        'linebreak-style': ['error', 'unix'],
        'semi': ['error', 'never'],
        'comma-dangle': ['warn', 'always-multiline'],
        'constructor-super': 'error',
        'no-confusing-arrow': 'error',
        'no-constant-condition': 'error',
        'no-class-assign': 'error',
        'no-const-assign': 'error',
        'no-dupe-class-members': 'error',
        'no-var': 'warn',
        'no-this-before-super': 'error',
        'object-shorthand': ['error', 'always'],
        'prefer-spread': 'warn',
        'prefer-template': 'warn',
        'require-yield': 'error',
        'jsx-quotes': 'warn',
        'react/forbid-prop-types': 'warn',
        'react/jsx-boolean-value': 'warn',
        'react/jsx-closing-bracket-location': 'warn',
        'react/jsx-curly-spacing': 'warn',
        'react/jsx-indent-props': 0,
        'react/jsx-max-props-per-line': ['warn', {"maximum":3}],
        'react/jsx-no-bind': ['warn', {
            "ignoreRefs": false,
            "allowArrowFunctions": true,
            "allowBind": false
        }],
        'react/jsx-no-duplicate-props': 'warn',
        'react/jsx-no-undef': 'warn',
        'react/jsx-uses-react': 'warn',
        'react/jsx-uses-vars': 'warn',
        'react/no-danger': 'warn',
        'react/no-did-mount-set-state': 'warn',
        'react/no-did-update-set-state': 'warn',
        'react/no-direct-mutation-state': 'warn',
        'react/no-multi-comp': ['warn', { "ignoreStateless": true}],
        'react/no-set-state': 'warn',
        'react/no-unknown-property': 'warn',
        'react/prefer-es6-class': 'warn',
        'react/prop-types': 'warn',
        'react/react-in-jsx-scope': 'warn',
        'react/self-closing-comp': 'warn',
        'react/sort-comp': 'warn',
        'react/jsx-wrap-multilines': 'warn',
        "flow-vars/define-flow-type": 1,
        "flow-vars/use-flow-type": 1,
        "no-console": 0,
        "immutable/no-let":0,
        "immutable/no-this":0,
        "immutable/no-mutation":0,
    },
}