package config

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path"
	"syscall"

	"path/filepath"

	"github.com/dropbox/godropbox/errors"
	yaml "gopkg.in/yaml.v2"
)

const PROJECT_NAME = "fb_bot"

var (
	Dir = flag.String("conf_dir", "", "Folder with config files")
)

type Config struct {
	Http            *Http            `yaml:"http"`
	Mongo           *Mongo           `yaml:"mongo"`
	Facebook        *Facebook        `yaml:"facebook"`
	DefaultLocation *DefaultLocation `yaml:"default_location"`
}

type Http struct {
	Port int    `yaml:"port"`
	Host string `yaml:"host"`
}

type Facebook struct {
	Bot         *Bot    `yaml:"bot"`
	AppId       string  `yaml:"app_id"`
	Secret      string  `yaml:"secret"`
	RedirectURL string  `yaml:"redirect_url"`
	Simply      *Simply `yaml:"simply"`
}

type Simply struct {
	PageId string `yaml:"page_id"`
}

type Longitude float64
type Latitude float64
type DefaultLocation struct {
	Longitude Longitude `yaml:"longitude"`
	Latitude  Latitude  `yaml:"latitude"`
}

type Bot struct {
	VerifyToken string `yaml:"verify_token"`
	PageToken   string `yaml:"page_token"`
}

type Mongo struct {
	Port      int        `yaml:"port"`
	Host      string     `yaml:"host"`
	DbName    string     `yaml:"db_name"`
	MongoPool *MongoPool `yaml:"pool"`
}

type MongoPool struct {
	Size int `yaml:"size"`
}

var (
	config *Config
	//configLock = new(sync.RWMutex)
)

// go calls init on start
func NewConfig() *Config {
	flag.Parse()
	loadConfig(true)
	s := make(chan os.Signal, 1)
	signal.Notify(s, syscall.SIGUSR2)
	go func() {
		for {
			<-s
			loadConfig(false)
			log.Println("Reloaded")
		}
	}()

	return config
}

func ConfDir() string {
	var err error
	var dir string

	if *Dir == "" || Dir == nil {
		dir, err = filepath.Abs(os.Args[0])
		if err != nil {
			log.Fatal(err)
		}
		dir = filepath.Dir(dir)

	} else {
		if filepath.IsAbs(*Dir) {
			dir = *Dir
		} else {
			dir, err = filepath.Abs(*Dir)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	for i := 0; i < 10; i++ {
		if filepath.IsAbs(dir) {
			break
		}

		last := path.Base(dir)

		if last == "" {
			log.Fatal(errors.Newf("PROJECT_NAME const value is invalide: %s", PROJECT_NAME))
		}
		if last != PROJECT_NAME {
			dir = path.Dir(dir)
		} else {
			dir = filepath.Join(dir, "fb_bot", "config")
			break
		}
	}

	dir = path.Clean(dir)
	if dir == "/" {
		log.Fatal("please configure conf_dir")
	}

	return dir
}

func loadConfig(fail bool) {
	var err error
	dir := ConfDir()

	temp := &Config{}
	err = temp.parseFiles([]string{dir + "/app.yaml", dir + "/dev.yaml"})
	if err != nil {
		log.Println("open config: ", err)
		if fail {
			os.Exit(1)
		}
	}

	//configLock.Lock()
	config = temp
	//configLock.Unlock()
}

func Get() *Config {
	//configLock.RLock()
	//defer configLock.RUnlock()
	return config
}

func (this *Config) parseFiles(files []string) error {
	var (
		buf []byte
		err error
	)

	filesExists := len(files)
	for _, file := range files {
		_, err = os.Stat(file)
		if err != nil {
			if os.IsNotExist(err) {
				filesExists--
				continue
			}

			return errors.Wrap(err, "")
		}

		buf, err = ioutil.ReadFile(file)
		if err != nil {
			return errors.Wrap(err, "")
		}

		err = yaml.Unmarshal(buf, this)
		if err != nil {
			return errors.Wrap(err, "")
		}
	}
	if filesExists == 0 {
		log.Fatal(errors.Newf("No config files found, %#v", files))
	}

	return nil
}
