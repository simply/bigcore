package db

import (
	"context"

	"gopkg.in/mgo.v2/bson"
)

type Item struct {
	Id    bson.ObjectId
	Title string
}

type todo struct{}

var Todo = &todo{}

func (*todo) Put(ctx context.Context, item *Item) error {
	err := mogo.Todo.Insert(item)
	if err != nil {
		return err
	}
	return nil
}

func (*todo) Pull(ctx context.Context) error {
	return nil
}
