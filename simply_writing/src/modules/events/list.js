import React from 'react'
import {StyleSheet, View, Text, Image, ListView} from 'react-native'
import {Card} from '../../components'

//TODO: Use ListView with refresh
const styles = StyleSheet.create({
    data       : {
        flexDirection: 'row',
    },
    date       : {
        width: 70,
    },
    day        : {
        textAlign : 'center',
        fontSize  : 18,
        fontWeight: 'bold',
    },
    month      : {
        textAlign : 'center',
        fontSize  : 16,
        fontWeight: 'bold',
    },
    time       : {
        textAlign : 'center',
        fontSize  : 16,
        fontWeight: 'bold',
    },
    details    : {
        flex        : 1,
        paddingRight: 6,
    },
    title      : {},
    description: {
        fontSize   : 10,
        paddingTop : 15,
        paddingLeft: 15,
    },
    address    : {
        fontSize: 10,
        color   : 'rgba(0,0,0,0.4)',
    },
    listView   : {
        paddingTop     : 20,
        backgroundColor: '#F5FCFF',
    },
    cover      : {
        marginTop: 5,
        width    : 400,
        height   : 200,
    },
})

const ListItem = ({event}) =>
    <Card key={`event-${event.IdEvent}`}>
        <View style={styles.data}>
            <View style={styles.date}>
                <Text style={styles.day}>{event.StartTimeParsed.day}</Text>
                <Text style={styles.month}>{event.StartTimeParsed.month}</Text>
                <Text style={styles.time}>{event.StartTimeParsed.hours}:{event.StartTimeParsed.minutes}</Text>
            </View>
            <View style={styles.details}>
                <Text style={styles.title}>{event.Name}</Text>
                <Text style={styles.address}>{event.Place.Street}</Text>
                <Text style={styles.description}>{event.Description}</Text>
            </View>
        </View>
        <Image style={styles.cover} source={{uri: event.Cover}}/>
    </Card>

ListItem.propTypes = {
    event: React.PropTypes.any.isRequired,
}

//TODO: display distance to user, https://docs.mongodb.com/manual/tutorial/calculate-distances-using-spherical-geometry-with-2d-geospatial-indexes/
//TODO: make clickable address
const List = ({dataSource}) =>
    <ListView
        dataSource={dataSource}
        renderRow={(event) => <ListItem event={event} />}
    />

List.propTypes = {
    dataSource: React.PropTypes.instanceOf(ListView.DataSource).isRequired,
}

export default List