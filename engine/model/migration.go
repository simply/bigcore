package model

import (
	"time"
)

type Migration struct {
	Processed    int32
	Total        int32
	EtaInSeconds time.Duration
}

func NewMigration() *Migration {
	return &Migration{}
}

func (this *Migration) Run() chan bool {
	//TODO: if migration already started, return shared finishCh

	finishCh := make(chan bool)

	go func() {
		this.Total = int32(10000000)
		for this.Processed = int32(0); this.Processed < this.Total; this.Processed++ {
			if this.Processed%100000 != 0 {
				continue
			}

			time.Sleep(100 * time.Millisecond)
		}

		finishCh <- true
		close(finishCh)
	}()

	return finishCh
}
