import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

const middleware = applyMiddleware(thunk, createLogger())

export default (data: any = {}) => {

    let rootReducer = require('./modules/reducers').default

    const store = createStore(rootReducer, data, middleware)

    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = require('./modules/reducers').default
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}