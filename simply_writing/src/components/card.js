import React from 'react'
import {StyleSheet, View} from 'react-native'

// facebook colors http://www.color-hex.com/color-palette/185
const styles = StyleSheet.create({
    card: {
        backgroundColor: '#FFFFFF',

        borderStyle      : 'solid',
        borderTopWidth   : 1,
        borderBottomWidth: 1,
        borderTopColor   : '#e5e6e9',
        borderBottomColor: '#d0d1d5',
        //borderRadius     : 3,

        paddingTop   : 5,
        paddingBottom: 5,

        marginTop    : 4,
        flexDirection: 'column',
    },
})

const Card = ({children}) =>
    <View style={styles.card} {...this.props}>
        {children}
    </View>

Card.propTypes = {
    children: React.PropTypes.node,
}

export default Card