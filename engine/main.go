package main

import ()

func main() {
	app := NewApp()
	err := app.Init()
	if err != nil {
		panic(err)
	}
	app.Run()
}
