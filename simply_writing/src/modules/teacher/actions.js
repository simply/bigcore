// @flow

import {
    REFRESH_LIST, REFRESH_LIST_FAILED, BEFORE_REFRESH,
    PRESS_LIST_ITEM, NAVIGATE_TO,
    BECOME_TEACHER_REQUEST, BECOME_TEACHER_SUCCEED, BECOME_TEACHER_FAILED,
    ORDER_INTERVIEW_REQUEST, ORDER_INTERVIEW_SUCCEED, ORDER_INTERVIEW_FAILED,
} from './constants'

import { URL, GetHeaders } from '../../config'
import type { GetState } from './../index'

const URL_ORDER_INTERVIEW = `${URL.BASE}/teacher/order_interview`
const URL_BECOME_TEACHER  = `${URL.BASE}/teacher/become`
const URL_TEACHER_LIST    = `${URL.BASE}/teacher/list`

const Server = {
    orderInterview: (getState: GetState, idTeacher) => {
        const params = {
            id_teacher: idTeacher,
        }

        return fetch(URL_ORDER_INTERVIEW, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify(params),
        }).then(response => response.json())
    },

    becomeTeacher: (getState: GetState) => {
        return fetch(URL_BECOME_TEACHER, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify({}),
        }).then(response => response.json())
    },

    teacherList: (getState: GetState) => {
        const geo    = getState().Geo
        const params = {
            location: geo.available ? geo.location : null,
        }

        return fetch(URL_TEACHER_LIST, {
            method : 'POST',
            headers: GetHeaders(getState),
            body   : JSON.stringify(params),
        }).then(response => response.json())
    },
}

//each action should have the following signiture.
//  {
//     type: <type of action>,        type is required
//     payload: <the actual payload>  payload is optional. if you don't
//                                    have anything to send to reducer,
//                                    you don't need the payload. for example
//                                    newCounter action
//  }
export const refresh = () => {
    return (dispatch: Dispatch, getState: GetState) => {
        dispatch(onBeforeRefresh())
        Server.teacherList(getState)
            .then(json => dispatch(refreshSucceed(json)))
            .catch(error => dispatch(refreshFailed(error)))
            .done()
    }
}

export const onBeforeRefresh = () => {
    return {
        type: BEFORE_REFRESH,
    }
}

const refreshSucceed = (data) => {
    return {
        type   : REFRESH_LIST,
        payload: {
            teachers: data.teachers,
        },
    }
}

const refreshFailed = (error) => {
    return {
        type   : REFRESH_LIST_FAILED,
        payload: error,
        error  : true,
    }
}

export const pressListItem = (idTeacher: number) => {
    return {
        type: PRESS_LIST_ITEM,
        idTeacher,
    }
}

export const becomeTeacher = () => {
    return (dispatch: Dispatch, getState: GetState) => {
        dispatch(becomeTeacherRequest())
        Server.becomeTeacher(getState)
            .then(json => {
                dispatch(becomeTeacherSucceed(json))
                // dispatch(userActions.updateUser(json.user))
            })
            .catch(error => dispatch(becomeTeacherFailed(error)))
            .done()
    }
}

const becomeTeacherRequest = () => {
    return {
        type: BECOME_TEACHER_REQUEST,
    }
}

const becomeTeacherFailed = (error) => {
    return {
        type   : BECOME_TEACHER_FAILED,
        payload: error,
        error  : true,
    }
}

const becomeTeacherSucceed = () => {
    return {
        type: BECOME_TEACHER_SUCCEED,
    }
}

export const orderInterview = (idTeacher: number) => {
    return (dispatch: Dispatch, getState: GetState) => {
        dispatch(orderInterviewRequest())
        Server.orderInterview(getState, idTeacher)
            .then(json => {
                dispatch(orderInterviewSucceed(json))
                // dispatch(userActions.updateUser(json.user))
            })
            .catch(error => dispatch(orderInterviewFailed(error)))
            .done()
    }
}

const orderInterviewRequest = () => {
    return {
        type: ORDER_INTERVIEW_REQUEST,
    }
}

const orderInterviewFailed = (error) => {
    return {
        type   : ORDER_INTERVIEW_FAILED,
        payload: error,
        error  : true,
    }
}

const orderInterviewSucceed = () => {
    return {
        type: ORDER_INTERVIEW_SUCCEED,
    }
}

export const navigateTo = (routeName: string) => {
    return {
        type   : NAVIGATE_TO,
        payload: {
            routeName,
        },
    }
}
