// @flow
import {
    FB_LOGIN_BY_SERVER, FB_BEFORE_LOGIN_BY_SERVER, FB_LOGIN_NOT_FOUND,
    FB_LOGOUT, FB_CANCEL, FB_ERROR, FB_PERMISSIONS_MISSING,
    FB_LOGIN_BY_SERVER_ERROR,
} from './fb_constants'
import {URL, GetHeaders, FacebookHeaders} from '../../config'
import {AccessToken} from 'react-native-fbsdk'

import type {FbLoginData} from './reducer'
import type {GetState} from './../index'

const AUTH_URL = `${URL.BASE}/auth/login`

const Server = {
    auth: (getState: GetState, headers) => {
        return fetch(AUTH_URL, {
            method : 'GET',
            headers: GetHeaders(getState, headers),
        })
            .then(response => {
                if (!response.ok) {
                    throw response
                }
                return response.json()
            })

    }
}
// const BASE_URL    = 'http://localhost:8060'
// const REQUEST_URL = BASE_URL + '/list'

//each action should have the following signiture.
//  {
//     type: <type of action>,        type is required
//     payload: <the actual payload>  payload is optional. if you don't
//                                    have anything to send to reducer,
//                                    you don't need the payload. for example
//                                    newCounter action
//  }


export const onLogin = (error: Error, result: any) => {
    if (error) {
        return onLoginError(error)
    }

    if (result.isCancelled) {
        return onCancel()
    }

    return doLogin()
}

export const doLogin = () => {
    return (dispatch: Function) => {
        AccessToken.getCurrentAccessToken()
            .then(fbTokenData => parseFacebookTokenData(fbTokenData))
            .then(parsedTokenData => dispatch(onLoginSuccess(parsedTokenData)))
            .catch(error => dispatch(onLoginError(error)))
            .done()
    }

}

const parseFacebookTokenData = (fbTokenData: FbLoginData) => {
    return {
        token         : fbTokenData.accessToken.toString(),
        userID        : fbTokenData.userID,
        expirationTime: new Date(fbTokenData.expirationTime),
        permissions   : fbTokenData.permissions,
    }
}

const onLoginSuccess = (parsedTokenData) => {
    return (dispatch, getState) => {
        dispatch(beforeLoginByServer(parsedTokenData))
        Server.auth(getState, FacebookHeaders(parsedTokenData))
            .then(json => dispatch(onLoginByServer(json)))
            .catch(error => dispatch(onLoginError(error)))
            .done()
    }
}

const beforeLoginByServer = (facebook) => {
    return {
        type   : FB_BEFORE_LOGIN_BY_SERVER,
        payload: {
            facebook,
        },
    }
}

const onLoginError = (error) => {
    console.warn(error)
    return {
        type   : FB_LOGIN_BY_SERVER_ERROR,
        payload: error,
        error  : true,
    }
}

const onLoginByServer = (serverResponse) => {
    return {
        type   : FB_LOGIN_BY_SERVER,
        payload: {
            user: serverResponse.user,
        },
    }
}

export const onLogout = () => {
    return {
        type: FB_LOGOUT,
    }
}

export const onLoginNotFound = () => {
    return {
        type: FB_LOGIN_NOT_FOUND,
        //     articles: data.articles,
        //     loaded:   true,
    }
}

export const onError              = (error: Error) => {
    return {
        type   : FB_ERROR,
        payload: error,
        error  : true,
    }
}
export const onCancel             = () => {
    return {
        type: FB_CANCEL,
        // payload: {
        //     articles: data.articles,
        //     loaded:   true,
        // },
    }
}
export const onPermissionsMissing = (error: Error) => {
    return {
        type   : FB_PERMISSIONS_MISSING,
        payload: error,
        error  : true,
    }
}

