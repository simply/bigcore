package dev

import (
	"bigcore/radio/common/db"
	"bigcore/radio/config"
	"bigcore/radio/model/ds"
	"context"
	"github.com/pkg/errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func Fixtures(ctx context.Context) error {
	var err error

	dbInstance := db.FromContext(ctx).DB(config.FromContext(ctx).Mongo.DbName)
	clubCollection := dbInstance.C("club")
	userCollection := dbInstance.C("user")
	courseCollection := dbInstance.C("course")

	//clean
	//err = clubCollection.DropCollection()
	//if err != nil && err != mgo.ErrNotFound {
	//	return errors.Wrap(err, "")
	//}

	//err = courseCollection.DropCollection()
	//if err != nil && err != mgo.ErrNotFound {
	//	return errors.Wrap(err, "")
	//}

	_, err = userCollection.Find(bson.M{"email": "test@teacher.com"}).Apply(mgo.Change{Remove: true}, nil)

	if err != nil && errors.Cause(err) != mgo.ErrNotFound {
		return errors.Wrap(err, "")
	}

	//set up
	club1 := ds.Club{
		ID:     bson.NewObjectId(),
		Name:   "District 1",
		Status: ds.ClubStatusActive,
		Place: ds.Place{
			Street:  "District 1",
			City:    "HCMC",
			Country: "Vietnam",
			Point:   ds.NewPoint(-122.03068145, 37.33155419),
		},
		Link: "https://facebook.com/simplyenglishclub",
		Facebook: ds.ClubOnFacebook{
			IdPage: 339941526201879,
		},
	}

	err = clubCollection.Insert(&club1)
	if err != nil {
		return errors.Wrap(err, "")
	}
	club2 := ds.Club{
		ID:     bson.NewObjectId(),
		Name:   "District 2",
		Status: ds.ClubStatusActive,
		Place: ds.Place{
			Street:  "District 2",
			City:    "HCMC",
			Country: "Vietnam",
			Point:   ds.NewPoint(-122.03068145, 37.33155419),
		},
		Link: "https://facebook.com/simplyenglishclub",
		Facebook: ds.ClubOnFacebook{
			IdPage: 339941526201878,
		},
	}
	err = clubCollection.Insert(&club2)
	if err != nil {
		return errors.Wrap(err, "")
	}

	teacher1 := ds.User{
		ID:        bson.NewObjectId(),
		Email:     "test@teacher.com",
		FirstName: "Tompson",
		LastName:  "Jonson",
		Picture:   "https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/12219576_10204920795602859_8168929055570047891_n.jpg?oh=422f21fc51ca173c5d8b18038e1184bd&oe=57D8A68A",
		Description: "Be embittered for whoever balances, because each has been respected with dimension.\n" +
			"Karmas, explosion of the chaoss, and prime monkeys will always protect them.\n" +
			"In the underworld all selfs hear happiness.",
		Profile: ds.Profile{
			Teacher: ds.Teacher{
				Status: ds.TEACHER_STATUS_ACTIVE,
				Skills: []ds.TeacherSkill{
					{
						Label:   "#cute",
						Percent: 60,
					},
					{
						Label:   "#smile",
						Percent: 40,
					},
					{
						Label:   "#handsome",
						Percent: 100,
					},
				},
			},
		},
		Type: []ds.UserType{ds.USER_TYPE_TEACHER},
	}
	err = userCollection.Insert(teacher1)
	if err != nil {
		return errors.Wrap(err, "")
	}

	teacher2 := ds.User{
		ID:        bson.NewObjectId(),
		Email:     "test@teacher.com",
		FirstName: "Jefrie",
		LastName:  "Perron",
		Picture:   "https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/12143234_10154080274489044_6591441419468213667_n.jpg?oh=8a7139c4271d1c7f44fac68c84a91afd&oe=57E08B81",
		Description: "The spacecraft is more green people now than girl. galactic and mechanically ship-wide.\n" +
			"Processors are the pathways of the carnivorous starlight travel.\n" +
			"Countless space suits eat calm, neutral crews.",
		Profile: ds.Profile{
			Teacher: ds.Teacher{
				Status: ds.TEACHER_STATUS_ACTIVE,
				Skills: []ds.TeacherSkill{
					{
						Label:   "#cute",
						Percent: 100,
					},
					{
						Label:   "#smile",
						Percent: 30,
					},
					{
						Label:   "#handsome",
						Percent: 80,
					},
				},
			},
		},
		Type: []ds.UserType{ds.USER_TYPE_TEACHER},
	}
	err = userCollection.Insert(teacher2)
	if err != nil {
		return errors.Wrap(err, "")
	}

	course1 := ds.Course{
		ID:         bson.NewObjectId(),
		FksClub:    []bson.ObjectId{club1.ID},
		FksTeacher: []bson.ObjectId{teacher1.ID},
		Name:       "How to write working CV",
		Status:     ds.CourseStatusActive,
	}
	err = courseCollection.Insert(&course1)
	if err != nil {
		return errors.Wrap(err, "")
	}
	course2 := ds.Course{
		ID:     bson.NewObjectId(),
		Name:   "How to write love story",
		Status: ds.CourseStatusActive,
	}
	err = courseCollection.Insert(&course2)
	if err != nil {
		return errors.Wrap(err, "")
	}

	return nil
}
