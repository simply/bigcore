package db

import (
	"bigcore/todo/db/mongo"
	"context"
)

type OrderType struct {
	Price int
}

type orderModel struct{}

var Order = &orderModel{}

func (*orderModel) Create(ctx context.Context, order *OrderType) error {
	return mongo.Orders().Insert(order)
}

func (*orderModel) Count(ctx context.Context) (int, error) {
	return mongo.Orders().Count()
}

type userModel struct{}

var User = &userModel{}

func (*userModel) Exists(ctx context.Context, userId string) (bool, error) {
	return userId == "abc", nil
}
