/// <reference path="../typings/tsd.d.ts"/>
import { atoa } from "./atoa_method"

export function reactDragula(containers: Element[]): any {
    return dragula.apply(this, atoa(arguments)).on('cloned', cloned);

    function cloned (clone: Element) {
        rm(clone);
        atoa(clone.getElementsByTagName('*')).forEach(rm);
    }

    function rm (el) {
        el.removeAttribute('data-reactid');
    }
}
