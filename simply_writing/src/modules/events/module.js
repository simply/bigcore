import React from 'react'
import {Text, ListView} from 'react-native'
import {connect} from 'react-redux'
import * as actions from './actions'
import {Loading} from '../../components'
import List from './list'

const mapState   = state => ({
    dataSource: state.Events.dataSource,

    loaded: state.Events.loaded,
})
const mapActions = dispatch => ({
    init: () => dispatch(actions.init()),
})

@connect(mapState, mapActions)
export default class Module extends React.Component {
    static displayName = 'Events'
    static propTypes   = {
        init      : React.PropTypes.func.isRequired,
        loaded    : React.PropTypes.bool.isRequired,
        dataSource: React.PropTypes.instanceOf(ListView.DataSource).isRequired,
    }

    render() {
        const {loaded, dataSource} = this.props
        if (!loaded) {
            return <Loading />
        }

        if (dataSource.getRowCount() == 0) {
            return (
                <Text>Oopss... no coming Events...</Text>
            )
        }

        return <List dataSource={dataSource} />
    }
}
