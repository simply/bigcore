// @flow

//we are using namespaceing to prevent module's action type collision
//every module should have a unique name. the best practice is to set name
//base on module's name

//name of this modules
export const NAME: string = 'Login'

//action types
export const FB_LOGIN_BY_SERVER: string        = `${NAME}/FB_LOGIN_BY_SERVER`
export const FB_LOGOUT: string                 = `${NAME}/FB_LOGOUT`
export const FB_BEFORE_LOGIN_BY_SERVER: string = `${NAME}/FB_BEFORE_LOGIN_BY_SERVER`
export const FB_LOGIN_NOT_FOUND: string        = `${NAME}/FB_LOGIN_NOT_FOUND`
export const FB_ERROR: string                  = `${NAME}/FB_ERROR`
export const FB_LOGIN_BY_SERVER_ERROR: string  = `${NAME}/FB_LOGIN_BY_SERVER_ERROR`
export const FB_CANCEL: string                 = `${NAME}/FB_CANCEL`
export const FB_PERMISSIONS_MISSING: string    = `${NAME}/FB_PERMISSIONS_MISSING`

//as you can see above, each action is namespaced with module's name.