var path = require('path'),
    node_modules_dir = path.join(__dirname, 'node_modules'),
    babel = require("babel-loader"),
    webpack = require('webpack'),
    HtmlwebpackPlugin = require('html-webpack-plugin');

var options = {
    prerender: false //see more here https://gist.github.com/haf/f671f1113d2c5dead5a7#file-gistfile1-txt-L110
}
var deps = [
    'react-router/dist/react-router.min.js',
    //'moment/min/moment.min.js',
    //'underscore/underscore-min.js',
]

var config = {
    debug: true,
    target: options.prerender ? "node" : "web",
    entry: [
        //'webpack-dev-server/client?http://192.168.99.101:8100',
        //'webpack/hot/only-dev-server',
        "/src/ts/index.tsx"
    ],
    output: {
        path: "/out/js",
        filename: "index.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.css', '.scss', '.ts', '.tsx', '.js'],
        root: '/src/ts',
        alias: {}
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            axios: "axios",
            dragula: "dragula",
        })

        //new webpack.optimize.UglifyJsPlugin({
        // minimize: true
        // })

        //new HtmlwebpackPlugin({
        //    template: 'index.html',
        //    inject: 'body',
        //    title: 'Kanban app',
        //})
    ],

    // Source maps support (or 'inline-source-map' also works)
    devtool: 'source-map',
    module: {
        noParse: [],
        loaders: [
            {
                test: /\.json$/,
                loaders: ['json']
            },
            {
                test: /\.ts(x?)$/,
                exclude: /(node_modules|lib)/,
                loaders: ['react-hot', 'babel?presets[]=es2015', 'ts']
            },
            {
                test: /\.scss$/,
                loaders: ["style", "css?sourceMap", "sass?sourceMap"]
            },
            {
                test: /\.css$/,
                loaders: ["style", "css?sourceMap"]
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url?limit=25000'
            },
            {
                test: /\.woff$/,
                loader: 'url?limit=100000'
            }
        ]
    },
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
        //poll: true //it give use see changes on NFS, for example in Docker
    }
}

// Run through deps and extract the first part of the path,
// as that is what you use to require the actual node modules
// in your code. Then use the complete path to point to the correct
// file and make sure webpack does not try to parse it
deps.forEach(function (dep) {
    var depPath = path.resolve(node_modules_dir, dep);
    config.resolve.alias[dep.split(path.sep)[0]] = depPath;
    config.module.noParse.push(depPath);
})


module.exports = config