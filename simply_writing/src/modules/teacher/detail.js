import React from 'react'
import { StyleSheet, Text, Image, View } from 'react-native'
import { Card, SkillList } from '../../components'
import Styles from '../../styles'
import OrderInterview from './order_interview'

const styles = StyleSheet.create({
    wrapper : {
        flexDirection: 'row',
    },
    left    : {
        width      : 200,
        marginLeft : 8,
        marginRight: 8,
    },
    right   : {
        flex       : 1,
        marginRight: 8,
    },
    name    : {
        ...Styles.text.dark,
        marginBottom: 8,
        fontWeight  : 'bold',
    },
    skill   : {
        ...Styles.text.medium,
    },
    portrait: {
        width : 200,
        height: 200,
    },
})

//TODO: render "Order Interview" only if it's not student/teacher
const Detail = ({ teacher, interview, onOrderInterview }) =>
    <Card>
        <View style={styles.wrapper}>
            <View style={styles.left}>
                <Image style={styles.portrait} source={{uri: teacher.Picture}}/>
                {interview.canOrder() ?
                    <OrderInterview onPress={onOrderInterview} showLoader={interview.inProgress}/> : null}
            </View>
            <View style={styles.right}>
                <Text style={styles.name}>{teacher.FirstName} {teacher.LastName}</Text>
                <SkillList skills={teacher.Profile.Teacher.Skills}/>
            </View>
        </View>
    </Card>

Detail.propTypes = {
    teacher         : React.PropTypes.any.isRequired,
    interview       : React.PropTypes.any.isRequired,
    onOrderInterview: React.PropTypes.func.isRequired,
}

export default Detail