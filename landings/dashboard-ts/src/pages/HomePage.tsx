import { Link } from "react-router-dom";
import * as React from "react";
import { observer } from "mobx-react";

import { RouteComponentProps } from "react-router";
import * as Incremental from "./../component/incremental";
import * as Sidebar from "./../containers/Sidbar";
import * as Todo from "./../containers/ToDo";
import styled, { keyframes } from "styled-components";
import AppState from "./../AppState";

import { Container, Row, Col, Jumbotron } from "reactstrap";

const logo = require("./../logo.svg");

interface Url {
}

const spin = keyframes`
    from { transform: rotate(0deg); }
    to { transform: rotate(360deg); }
`;

const Logo = styled.img`
    animation: ${spin} infinite 20s linear;
    height: 80px;
`;

export class State {
    incrementalButton: Incremental.State = new Incremental.State();
    sidebar: Sidebar.State = new Sidebar.State();
}

interface Props extends RouteComponentProps<Url> {
    state: AppState;
}

@observer
class HomePage extends React.Component<Props, {}> {
    render() {
        const { state } = this.props;

        return (
            <Container fluid={true}>
                <Row>
                    <Col xs="4" sm="3">
                        <Sidebar.Container state={state.homePage.sidebar} />
                    </Col>
                    <Col>
                        <Jumbotron>
                            <Logo src={logo} alt="logo" />
                            <h2>Welcome to React</h2>
                            <Link to={`/`}>Home</Link>
                            <Link to={`/teachers`}>Teachers</Link>
                        </Jumbotron>

                        <div>
                            <Incremental.Btn state={state.homePage.incrementalButton} />
                            <br />
                            <Todo.List state={state} />
                        </div>

                    </Col>
                </Row>
            </Container>
        );
    }
}

export default HomePage;
