package main

func main() {
	app := NewApp()

	err := app.Init()
	if err != nil {
		panic(err)
	}
	err = app.Run()
	if err != nil {
		panic(err)
	}
}
