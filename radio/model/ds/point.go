package ds

import (
	"bigcore/radio/config"

	"gopkg.in/mgo.v2/bson"
)

type Point struct {
	isValid     bool      `bson:"bool"`
	Type        string    `bson:"type,omitempty"`
	Coordinates []float64 `bson:"coordinates,omitempty"`
}

func NewPoint(longitude config.Longitude, latitude config.Latitude) Point {
	return Point{
		Type:        "Point",
		Coordinates: []float64{float64(longitude), float64(latitude)},
		isValid:     true,
	}
}

func (this Point) IsValid() bool {
	return this.isValid
}

func (this Point) ToMap() bson.M {
	return bson.M{"type": "Point", "coordinates": this.Coordinates}
}
