/**
 * React Native Webpack Starter Kit
 * https://github.com/jhabdas/react-native-webpack-starter-kit
 */
'use strict';

import React, {AppRegistry} from 'react-native'
import Root from './src'

AppRegistry.registerComponent('simply_writing', () => Root)