import React from 'react'
import { StyleSheet, TouchableOpacity, View, Text, ListView, Image } from 'react-native'
import { Card } from '../../components'
import Styles from '../../styles'
import * as actions from './actions'
import { Loading } from '../../components'
import { connect } from 'react-redux'
import BecomeTeacher from './become_teacher'

const styles = StyleSheet.create({
    wrapper : {
        flexDirection: 'row',
    },
    left    : {
        width      : 100,
        marginLeft : 8,
        marginRight: 8,
    },
    right   : {
        flex       : 1,
        paddingLeft: 5,
    },
    name    : {
        ...Styles.text.dark,
        marginBottom: 5,
        fontWeight  : 'bold',
    },
    skill   : {
        ...Styles.text.medium,
    },
    portrait: {
        width : 100,
        height: 100,
    },
})

const ListItem = ({ teacher, onPress }) =>
    <TouchableOpacity onPress={() => onPress(teacher)}>
        <Card key={`teacher-${teacher.id}`}>
            <View style={styles.wrapper}>
                <View style={styles.left}>
                    <Image source={{uri: teacher.Picture}} style={styles.portrait}/>
                </View>
                <View>
                    <Text style={styles.name}>{teacher.FirstName} {teacher.LastName}</Text>
                    <Text style={styles.skill}>Nationality: {teacher.Nationality}</Text>
                    <Text style={styles.skill}>Focus on: {teacher.FocusOn}</Text>
                </View>
            </View>
        </Card>
    </TouchableOpacity>

ListItem.propTypes = {
    onPress: React.PropTypes.func.isRequired,
}

//TODO: after change user status, need to run reducer for Login module and update user info
const mapState   = (state) => ({
    userIsTeacher          : state.Login.user.Type.includes('teacher'),
    loaded                 : state.Teacher.loaded,
    dataSource             : state.Teacher.dataSource,
    becomeTeacherInProgress: state.Teacher.becomeTeacherInProgress,
})
const mapActions = dispatch => ({
    pressListItem        : (teacher) => dispatch(actions.pressListItem(teacher)),
    refresh              : () => dispatch(actions.refresh()),
    handlerBecomeTeacher : () => dispatch(actions.becomeTeacher()),
    handlerOrderInterview: () => dispatch(actions.orderInterview()),
})

@connect(mapState, mapActions)
class List extends React.Component {
    static propTypes = {
        onPressItem: React.PropTypes.func.isRequired,
    }

    get becomeTeacherButton() {
        const { handlerBecomeTeacher, becomeTeacherInProgress } = this.props
        return <BecomeTeacher onPress={handlerBecomeTeacher} showLoader={becomeTeacherInProgress}/>
    }

    render() {
        const { userIsTeacher, loaded, dataSource, onPressItem } = this.props
        if (!loaded) {
            return <Loading />
        }

        if (dataSource.getRowCount() == 0) {
            return (
                <Text>Oopss... no coming Events...</Text>
            )
        }

        return (
            <View>
                {!userIsTeacher ? this.becomeTeacherButton : null}
                <ListView
                    dataSource={dataSource}
                    renderRow={(teacher) => <ListItem onPress={onPressItem} teacher={teacher}/>}
                />
            </View>
        )
    }
}

export default List