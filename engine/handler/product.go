package handler

import (
	"bigcore/engine/proto/engine"
	"fmt"

	"github.com/lazada/awg"
	//	"github.com/Masterminds/squirrel"
	"context"

	"github.com/dropbox/godropbox/errors"
	"google.golang.org/grpc/grpclog"
	"gopkg.in/gorp.v2"
	"gopkg.in/olivere/elastic.v6"
)

type ProductService struct {
	db       *gorp.DbMap
	elastic  *elastic.Client
	updateCh chan *engine.Product
}

const (
	spaceNo = uint32(512)
	indexNo = uint32(0)
)

func NewProductService(db *gorp.DbMap, es *elastic.Client) *ProductService {
	productService := &ProductService{
		db:       db,
		elastic:  es,
		updateCh: make(chan *engine.Product, 30000),
	}

	go productService.savePool()

	return productService
}

func (this *ProductService) savePool() {
	//TODO: different pool for db and es
	//TODO: fault tolerant pool
	//TODO: flush after timeout
	//TODO: find some library for it?
	//TODO: we don't need distribution here, will distribute on balancer

	wg := awg.AdvancedWaitGroup{}
	for i := 0; i < 10; i++ {
		wg.Add(func() error {
			buf := make([]*engine.Product, 10, 10)
			i := 0
			for product := range this.updateCh {
				buf[i] = product
				i++
				if i == 10 {
					err := this.saveInternal(buf)
					if err != nil {
						grpclog.Println(err.Error())
						return err
					}
					i = 0
				}
			}

			return nil
		})
	}

	err := wg.SetStopOnError(true).Start().GetLastError()
	if err == nil {
		//TODO: log me
	} else {
		//TODO: restart me
	}
}

func (this *ProductService) saveInternal(products []*engine.Product) error {
	var err error

	bulkService := this.elastic.Bulk().Index("test").Type("product")
	for _, product := range products {
		bulkService.Add(elastic.NewBulkIndexRequest().Doc(product).Id(fmt.Sprintf("%020d", product.Id)))
	}

	_, err = bulkService.Do()
	if err != nil {
		return err
	}

	return nil
	/*
	   //TODO: if exists then update
	   //TODO: here must be robot indexator, or it must be internal API.

	   psql := squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)
	   sqb := psql.Insert("test").Columns("title")
	   for _, product := range products {
	   	sqb = sqb.Values(product.Title)
	   }
	   sql, args, err := sqb.ToSql()
	   if err != nil {
	   	return err
	   }

	   _, err = this.db.Exec(sql, args...)
	   if err != nil {
	   	return err
	   }

	   return nil
	*/
}

func (this *ProductService) Mget(ctx context.Context, request *engine.MgetRequest) (*engine.MgetResponse, error) {
	reply := &engine.MgetResponse{
		Hits: make([]*engine.Hit, len(request.Ids), len(request.Ids)),
	}

	return reply, nil
	/*
		if len(request.Ids) == 0 {
			return &engine.MgetResponse{}, nil
		}

		type Product struct {
			Id    uint64 `db:"id"`
			Title string `db:"title"`
		}
		var products []Product

		psql := squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)
		sql, args, err := psql.Select().From("test").Columns("id", "title").Where(squirrel.Eq{"id": request.Ids}).ToSql()
		if err != nil {
			return &engine.MgetResponse{}, errors.Wrap(err, "")
		}

		_, err = this.db.Select(&products, sql, args...)
		if err != nil {
			return &engine.MgetResponse{}, errors.Wrap(err, "")
		}

		for i, p := range products {
			reply.Hits[i] = &engine.Hit{
				Id:    p.Id,
				Title: p.Title,
			}
		}
		return reply, nil
	*/
}

func (this *ProductService) Save(ctx context.Context, product *engine.Product) (*engine.SaveReply, error) {
	err := this.saveInternal([]*engine.Product{product})
	if err != nil {
		grpclog.Println(errors.Wrap(err, "").Error())
		return &engine.SaveReply{}, err
	}

	//	this.updateCh <- product

	return &engine.SaveReply{}, nil
}

func (this *ProductService) CreateDB(ctx context.Context, account *engine.Account) (*engine.CreateDBReply, error) {
	exists, err := this.elastic.IndexExists(account.DbName).Do()
	if err != nil {
		return &engine.CreateDBReply{}, err
	}

	if exists {
		return &engine.CreateDBReply{
			Success:       false,
			AlreadyExists: true,
		}, nil
	}

	settings := `
{
"mappings":{
	"_default_": {
		"_all": {
			"enabled": false
		  },
		"_source": {
			"enabled": false
		},
		"_timestamp": {
			"enabled": false
		},
		"_ttl": {
			"enabled": false
		}
	},
	"test":{
		"properties":{
			"id":{
				"type":"integer",
				"store": "true"
			},
			"title":{
				"type":"string",
				"store": "false"
			},
			"suggest_field":{
				"type":"completion",
				"payloads":true
			}
		}
	}
}
}
`

	_, err = this.elastic.CreateIndex(account.DbName).Body(settings).Do()
	if err != nil {
		return &engine.CreateDBReply{}, err
	}

	_, err = this.db.Exec(`CREATE TABLE ` + account.DbName + ` (
		id SERIAL PRIMARY KEY,
		title text
	)`)
	if err != nil {
		fmt.Println(errors.Wrap(err, "").Error())
		return &engine.CreateDBReply{}, err
	}

	return &engine.CreateDBReply{
		Success: true,
	}, nil
}
