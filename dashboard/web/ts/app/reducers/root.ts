import {combineReducers} from "redux"

import * as ws from "./ws"
import * as migration from "./migration"
import * as search from "../../search/reducers/search"

export class State {
    ws: ws.State
    migration: migration.State
    search: search.State
}

export default combineReducers({
    migration: migration.Reducer,
    ws:        ws.Reducer,
    search:    search.Reducer
})