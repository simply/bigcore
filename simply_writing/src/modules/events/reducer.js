import {ListView} from 'react-native'
import {handleActions} from 'redux-actions'
import {UPDATE_LIST, UPDATE_LIST_FAILED, BEFORE_REFRESH} from './constants'

const initialState = {
    loaded    : false,
    events    : [],
    dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
}

//you can do better here, I was just showing that you need to make a new copy
//of state. It is ok to deep copy of state. It will prevent unseen bugs in the future
//for better performance you can use immutableJS

//handleActions is a helper function to instead of using a switch case statement,
//we just use the regular map with function state attach to it.

export default handleActions({
    [UPDATE_LIST]       : (state, {payload}) => {
        payload.events = payload.events.map((event) => {
            event.StartTimeParsed = formatDate(event.StartTime)
            return event
        })
        return {
            ...state,
            loaded    : true,
            events    : payload.events,
            dataSource: state.dataSource.cloneWithRows(payload.events),
        }
    },
    [UPDATE_LIST_FAILED]: (state) => {
        return state
    },
    [BEFORE_REFRESH]    : (state) => {
        return state
    },
}, initialState)

const monthNames = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
]

const formatTwoDigits = (number) => (number < 10 ? '0' : '') + number
const formatDate = (time) => {
    const date = new Date(time)
    return {
        month  : monthNames[date.getMonth()],
        day    : date.getDay(),
        hours  : formatTwoDigits(date.getHours()),
        minutes: formatTwoDigits(date.getMinutes()),
    }
}
