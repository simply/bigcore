package handler

import (
	"bigcore/radio/model/ds"
	"bigcore/radio/model/user"
	"bigcore/radio/transfer/orders"
	"bigcore/radio/transfer/radio"
	"context"
)

type TeacherBecomeRequest struct {
	request
}
type TeacherBecomeResponse struct {
	Success bool     `json:"success"`
	User    *ds.User `json:"user"`
}

/*
func TeacherBecome(ctx context.Context, request *TeacherBecomeRequest, response *TeacherBecomeResponse) error {
	var err error
	userId := request.Ctx.Get("user").(*ds.UserService).ID.Hex()

	response.UserService, err = user.NewTeacherModel(ctx).Become(userId)
	if err != nil {
		return err
	}
	response.Success = true

	return nil
}
*/

type Teacher struct {
	ordersServer orders.OrdersClient
	model        *user.TeacherModel
}

func NewTeacher(ordersServer orders.OrdersClient, model *user.TeacherModel) *Teacher {
	return &Teacher{
		ordersServer: ordersServer,
		model:        model,
	}
}

func (this *Teacher) List(ctx context.Context, request *radio.TeacherListRequest) (*radio.TeacherListResponse, error) {
	var err error

	_, err = this.ordersServer.ListByUserId(context.Background(), &orders.ListByUserIdRequest{UserId: "iii"})
	if err != nil {
		return nil, err
	}

	limit := 10
	if request.Limit != 0 {
		limit = int(request.Limit)
	}

	response := &radio.TeacherListResponse{}
	users, err := this.model.FindAll(int(request.Page), limit)
	if err != nil {
		return nil, err
	}

	response.Teachers = make([]*radio.User, len(users), len(users))
	for i, item := range users {
		response.Teachers[i] = &radio.User{
			Id:        item.ID.Hex(),
			FirstName: item.FirstName,
		}
	}

	return response, nil
}
