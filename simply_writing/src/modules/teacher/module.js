import React from 'react'
import { ListView, View, Text, Navigator, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import List from './list'
import Detail from './detail'
import * as actions from './actions'
import autobind from 'autobind-decorator'


//TODO: button back must be global
const Profile = ({ onPressBack, teacher, interview, onOrderInterview }) =>
    <View>
        <TouchableOpacity onPress={onPressBack}>
            <View>
                <Text>Back</Text>
            </View>
        </TouchableOpacity>

        <Detail teacher={teacher}
                onOrderInterview={onOrderInterview}
                interview={interview}
        />
    </View>

Profile.propTypes = {
    teacher         : React.PropTypes.any.isRequired,
    interview       : React.PropTypes.any.isRequired,
    onPressBack     : React.PropTypes.func.isRequired,
    onOrderInterview: React.PropTypes.func.isRequired,
}

const mapState   = (state) => ({
    detailViewTeacher      : state.Teacher.teachers[state.Teacher.detailViewTeacherIndex],
    interview              : {
        inProgress: state.Teacher.orderInterviewInProgress,
        canOrder  : () => state.Login.user.Profile.Guest.PendingInterviewsWithTeachers.findIndex((idTeacher) => idTeacher == state.Teacher.teachers[state.Teacher.detailViewTeacherIndex].id) == -1,
    },
    userIsTeacher          : state.Login.user.Type.includes('teacher'),
    loaded                 : state.Teacher.loaded,
    currentScreen          : state.Teacher.currentScreen,
    dataSource             : state.Teacher.dataSource,
    becomeTeacherInProgress: state.Teacher.becomeTeacherInProgress,

})
const mapActions = dispatch => ({
    pressListItem        : (idTeacher) => dispatch(actions.pressListItem(idTeacher)),
    refresh              : () => dispatch(actions.refresh()),
    handlerBecomeTeacher : () => dispatch(actions.becomeTeacher()),
    handlerOrderInterview: (idTeacher) => dispatch(actions.orderInterview(idTeacher)),
    navigateTo           : (routeName) => dispatch(actions.navigateTo(routeName)),
})

@connect(mapState, mapActions)
export default class Module extends React.Component {
    static displayName = 'TeachersModule'
    static propTypes   = {
        pressListItem          : React.PropTypes.func.isRequired,
        refresh                : React.PropTypes.func.isRequired,
        handlerBecomeTeacher   : React.PropTypes.func.isRequired,
        handlerOrderInterview  : React.PropTypes.func.isRequired,
        becomeTeacherInProgress: React.PropTypes.bool.isRequired,
        interview              : React.PropTypes.any.isRequired,
        userIsTeacher          : React.PropTypes.bool.isRequired,
        loaded                 : React.PropTypes.bool.isRequired,
        navigateTo             : React.PropTypes.func.isRequired,
        currentScreen          : React.PropTypes.string.isRequired,
        dataSource             : React.PropTypes.instanceOf(ListView.DataSource).isRequired,
    }

    @autobind
    renderList() {
        //TODO: fast click back kills app
        const { navigateTo } = this.props
        const onPressItem    = (teacher) => {
            navigateTo('detail')
            this.props.pressListItem(teacher.id)
            this.refs.navigator.push({ name: 'detail' })
        }

        return <List onPressItem={onPressItem}/>
    }

    @autobind
    renderTeacherProfile() {
        const { detailViewTeacher, interview, handlerOrderInterview, navigateTo } = this.props

        const onOrderInterview = () => {
            handlerOrderInterview(detailViewTeacher.id)
        }

        const onPressBack = () => {
            navigateTo('list')
            this.refs.navigator.pop()
        }

        return <Profile
            interview={interview}
            onPressBack={onPressBack} teacher={detailViewTeacher}
            onOrderInterview={onOrderInterview}
        />
    }

    @autobind
    renderScene(route) {
        if (route.name === 'list') {
            return this.renderList()
        } else if (route.name === 'detail') {
            return this.renderTeacherProfile()
        }
    }

    render() {
        const { currentScreen } = this.props
        return (
            <Navigator
                initialRoute={{name: currentScreen}}
                ref="navigator"
                renderScene={this.renderScene}
            />
        )
    }
}
