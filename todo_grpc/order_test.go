package todo_test

import (
	"bigcore/todo"
	"bigcore/todo/app"
	"bigcore/todo/db"
	"bigcore/todo/db/mongo"
	"context"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ------------------ Interfaces compatibility tests --------------------------

var _ todo.OrdersServer = todo.OrderService

// before/after package tests pattern
func TestMain(m *testing.M) {
	// Set Up for any tests ....
	if !testing.Short() {
		// Set Up for integration tests ....
		app.New().MustInit()
	}

	exitCode := m.Run()

	// Tear Down ....
	os.Exit(exitCode) // run tests in package
}

// ------------------ Unit tests ----------------------------------------------

type orderListValidateTestCase func(t *testing.T, ctx context.Context, user *MockIUser, order *MockIOrder)

var orderListValidateTestCases = map[string]orderListValidateTestCase{
	"passed": func(t *testing.T, ctx context.Context, user *MockIUser, order *MockIOrder) {
		request := &todo.OrderListRequest{
			UserId: "asdf",
		}

		user.EXPECT().Exists(gomock.Any(), request.UserId).Return(true, nil)

		err := request.Validate(ctx, user, order)
		// True(t, ok)
		Nil(t, err)
	},

	"user id is empty": func(t *testing.T, ctx context.Context, user *MockIUser, order *MockIOrder) {
		request := &todo.OrderListRequest{
			UserId: "",
		}

		err := request.Validate(ctx, user, order)
		// False(t, ok)
		True(t, todo.IsValidationError(err))
	},

	"user is not sexists": func(t *testing.T, ctx context.Context, user *MockIUser, order *MockIOrder) {
		request := &todo.OrderListRequest{
			UserId: "abc",
		}

		user.EXPECT().Exists(gomock.Any(), request.UserId).Return(false, nil)

		err := request.Validate(ctx, user, order)
		// False(t, ok)
		True(t, todo.IsValidationError(err))
	},
}

func _TestOrderListValidate(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	for name, test := range orderListValidateTestCases {
		t.Run(name, func(t *testing.T) {
			t.Parallel()
			test(t, context.Background(), NewMockIUser(mockCtrl), NewMockIOrder(mockCtrl))
		})
	}
}

// ------------------ Integration tests -----------------------------------------

type listIntegrationTestCase func(t *testing.T, ctx context.Context)

var listIntegrationTestCases = map[string]listIntegrationTestCase{
	"correct user": func(t *testing.T, ctx context.Context) {
		resp, err := todo.OrderService.List(ctx, &todo.OrderListRequest{
			UserId: "1",
		})

		Nil(t, err)
		True(t, len(resp.Orders) > 0)
		count, err := db.Order.Count(ctx)
		Nil(t, err)
		True(t, count == 2)
	},

	"negative user fail": func(t *testing.T, ctx context.Context) {
		_, err := todo.OrderService.List(ctx, &todo.OrderListRequest{
			UserId: "-1",
		})

		NotNil(t, err)
	},

	"non-exists user": func(t *testing.T, ctx context.Context) {
		_, err := todo.OrderService.List(ctx, &todo.OrderListRequest{
			UserId: "999999999999999",
		})

		NotNil(t, err)
	},
}

func _TestListIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	mongo.Orders().DropCollection()
	db.Order.Create(context.Background(), &db.OrderType{
		Price: 1,
	})

	t.Run("Suite", func(t *testing.T) {
		for name, test := range listIntegrationTestCases {
			t.Run(name, func(t *testing.T) {
				t.Parallel()

				test(t, context.Background())
			})
		}
	})
}

var testCases = []struct {
	name             string
	request          *todo.OrderListRequest
	expectError      codes.Code
	reply            *todo.OrderListReply
	amountOrdersInDb int
}{
	{
		name: "correct user",
		request: &todo.OrderListRequest{
			UserId: "abc",
		},
		expectError:      codes.OK,
		reply:            nil,
		amountOrdersInDb: 2,
	},
	{
		name: "negative user fail",
		request: &todo.OrderListRequest{
			UserId: "-1",
		},
		expectError:      codes.PermissionDenied,
		reply:            nil,
		amountOrdersInDb: 0,
	},
	{
		name: "non-exists user",
		request: &todo.OrderListRequest{
			UserId: "999999999999999",
		},
		expectError:      codes.PermissionDenied,
		reply:            nil,
		amountOrdersInDb: 0,
	},
}

func IsGrpcErrorCode(t *testing.T, expectedCode codes.Code, err error) {
	s, ok := status.FromError(err)
	if !ok {
		t.Fatal("expected grpc error, got: %+v", err)
	}

	Equal(t, expectedCode.String(), s.Code().String())
}

func TestListIntegration2(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var err error
			ctx := context.Background()
			mongo.Orders().DropCollection()

			_, err = todo.OrderService.List(ctx, tc.request)

			IsGrpcErrorCode(t, tc.expectError, err)

			// True(t, len(resp.Orders) > 0)
			count, err := db.Order.Count(ctx)
			Nil(t, err)
			Equal(t, tc.amountOrdersInDb, count)
		})
	}
}
