provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "web" {
  image = "ubuntu-17-10-x64"
  name = "web"
  region = "sgp1"
  size = "1gb"
  ssh_keys = [
    5126629]

  provisioner "remote-exec" {
    script = "install-docker.sh"
  }

  connection {
    type = "ssh"
    user = "root"
    host = "${digitalocean_droplet.web.ipv4_address}"
    private_key = "${file(var.do_ssh_key)}"
  }
}

output "ip" {
  value = "tcp://${digitalocean_droplet.web.ipv4_address}:2376/"
}


provider "docker" {
  host = "tcp://${digitalocean_droplet.web.ipv4_address}:2376/"
}


data "docker_registry_image" "nginx" {
  name = "nginx:1-alpine"
}

resource "docker_image" "nginx" {
  name          = "${data.docker_registry_image.nginx.name}"
  pull_triggers = ["${data.docker_registry_image.nginx.sha256_digest}"]
}

resource "docker_container" "nginx-server" {
  name = "nginx-server"
  image = "${docker_image.nginx.latest}"
  ports {
    internal = 80
  }
  volumes {
    container_path  = "/usr/share/nginx/html"
    host_path = "/home/scrapbook/tutorial/www"
    read_only = true
  }
}