import * as React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
// import TeacherPage from "./pages/TeacherPage";
import AppState from "./AppState";

const state = new AppState();

const App = () => (
    <Router>
        <div>
            <Route
                exact={true}
                path="/"
                render={(props) => (
                    <HomePage {...props} state={state} />
                )}
            />
            {/* <Route
                exact={true}
                path="/teachers"
                render={(props) => (
                    <TeacherPage {...props} state={state} />
                )}
            />
            <Route
                exact={true}
                path="/teacher/:id"
                render={(props) => (
                    <TeacherPage {...props} state={state} />
                )}
            /> */}
        </div>
    </Router>
);

export default App;
