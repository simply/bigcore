package ds

type Place struct {
	Name    string `bson:"name,omitempty"`
	Street  string `bson:"street,omitempty"`
	City    string `bson:"city,omitempty"`
	Country string `bson:"country,omitempty"`
	Point   Point  `bson:"point,omitempty"`
}
