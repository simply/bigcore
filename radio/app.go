package radio

import (
	"bigcore/radio/config"
	"bigcore/radio/worker"

	"bigcore/radio/common"

	"bigcore/radio/common/db"
	"fmt"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"github.com/pkg/errors"
	"google.golang.org/grpc/grpclog"
	"gopkg.in/mgo.v2"

	"bigcore/radio/transfer/radio"

	"bigcore/radio/handler"
	"bigcore/radio/model/user"
	"bigcore/radio/transfer/orders"
	"context"
	"log"
	"net/http"
	"os"
	"runtime/debug"
	"time"

	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func init() {
	grpclog.SetLogger(log.New(os.Stdout, "user_server: ", log.LstdFlags))
}

type App struct {
	onCloseStack []func()

	Workers Workers
	Config  config.Config
	Timer   time.Time

	DB *mgo.Session

	Handlers Handlers
	Models   Models
	Clients  Clients
	Servers  Servers
}

type Clients struct {
	Order orders.OrdersClient
}

type Servers struct {
	Web *grpcweb.WrappedGrpcServer
}

type Handlers struct {
	Teacher *handler.Teacher
}

type Models struct {
	user    *user.Model
	teacher *user.TeacherModel
}

type Workers struct {
	EventsFetcher *worker.EventsFetcherWorker
}

func NewApp() *App {
	// Add the stack hook.
	return &App{
		Timer: time.Now(),
	}
}

func (this *App) Init() error {
	this.Config = config.Get()

	return common.UntilError(
		this.InitClients,
		this.InitDb,
		this.InitClasses,
		this.InitServers,
		//this.InitWorkers,
	)
}

func (this *App) InitClasses() error {

	this.Models = Models{
		teacher: user.NewTeacherModel(this.DB.DB(this.Config.Mongo.DbName)),
	}
	this.Handlers = Handlers{
		Teacher: handler.NewTeacher(this.Clients.Order, this.Models.teacher),
	}

	//Workers
	//this.Workers = Workers{}
	//this.Workers.EventsFetcher = worker.NewEventsFetcherWorker(this.Ctx)

	//user.New(this.Ctx).EnsureIndices()
	//event.New(this.Ctx).EnsureIndices()
	//club.New(this.Ctx).EnsureIndices()
	//course.New(this.Ctx).EnsureIndices()

	if this.Config.Env == config.ENV_DEV {
		//common.Must(dev.Fixtures(this.Ctx))
	}

	return nil
}

func (this *App) InitWorkers() error {
	go this.Workers.EventsFetcher.Do()
	this.Workers.EventsFetcher.Jobs <- &worker.EventsFetcherJob{
		PageId: this.Config.Facebook.Simply.PageId,
	}

	return nil
}

func (this *App) InitClients() error {
	conn, err := grpc.Dial(":8061", grpc.WithInsecure())
	if err != nil {
		return errors.Wrap(err, "can't connect to OrdersServer")
	}
	this.onClose(func() { conn.Close() })

	this.Clients.Order = orders.NewOrdersClient(conn)

	return nil
}

func (this *App) InitDb() error {
	session, err := mgo.Dial(this.Config.Mongo.Host)
	if err != nil {
		return errors.Wrap(err, "can't connect to mongo")
	}

	session.SetMode(mgo.Eventual, true)
	this.DB = session

	this.onClose(func() {
		session.Close()
	})
	return nil
}

func (this *App) InitServers() error {

	addDbToCtx := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		ctx = db.NewContext(ctx, this.DB)
		return handler(ctx, req)
	}
	addConfigToCtx := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		ctx = config.NewContext(ctx, this.Config)
		return handler(ctx, req)
	}

	recoveryOpts := grpc_recovery.WithRecoveryHandler(func(p interface{}) (err error) {
		grpclog.Printf("%s\n%s\n", p, debug.Stack())
		return status.Error(codes.Internal, "Internal Service Error")
	})

	logging := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		response, err := handler(ctx, req)
		if err != nil {
			common.LogError(err)
		}
		return response, err
	}

	server := grpc.NewServer(
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
			grpc_recovery.StreamServerInterceptor(recoveryOpts),
		)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_recovery.UnaryServerInterceptor(recoveryOpts),
			logging,
			addDbToCtx,
			addConfigToCtx,
		)),
	)

	radio.RegisterTeacherServer(server, this.Handlers.Teacher)

	grpcwebOpts := grpcweb.WithOriginFunc(func(origin string) bool { return true })
	this.Servers.Web = grpcweb.WrapServer(server, grpcwebOpts)

	return nil
}

func (this *App) Run() error {
	addr := fmt.Sprintf("%s:%d", this.Config.Http.Host, this.Config.Http.Port)
	httpServer := http.Server{
		Addr: addr,
		Handler: http.HandlerFunc(func(resp http.ResponseWriter, req *http.Request) {
			this.Servers.Web.ServeHTTP(resp, req)
		}),
	}

	grpclog.Println("Started: " + addr)

	if err := httpServer.ListenAndServe(); err != nil {
		grpclog.Fatalf("failed starting http server: %v", err)
	}

	return nil
}

func (this *App) onClose(cb func()) {
	this.onCloseStack = append(this.onCloseStack, cb)
}

func (this *App) Close() {
	for _, cb := range this.onCloseStack {
		cb()
	}
}
