package user

import (
	"bigcore/radio/common/db"

	"bigcore/radio/model/ds"

	"bigcore/radio/model/user/facebook"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type query struct {
	*db.Query
}

func (this *query) applyOrderInterviewWithTeacher(idTeacher string, result *ds.User) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{
		Update: bson.M{
			"$addToSet": bson.M{
				"profile.guest.pending_interviews_with_teachers": idTeacher,
			},
		},
		ReturnNew: true,
	}, result)
}

func (this *query) applyOrderInterviewInClub(idClub string, result *ds.User) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{
		Update: bson.M{
			"$addToSet": bson.M{
				"profile.guest.pending_interviews_with_clubs": idClub,
			},
		},
		ReturnNew: true,
	}, result)
}

func (this *query) applyFacebookOAuth(data ds.OAuthFacebook) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{Update: bson.M{"$set": bson.M{
		"o_auth.facebook": data,
	}}}, nil)
}

func (this *query) applyFacebookProfile(result *facebook.Result) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{Update: bson.M{"$set": bson.M{
		"profile.facebook": result,
	}}}, nil)
}

func (this *query) applySubscribeToCourse(course *ds.AppliedCourse, result *ds.User) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{
		Update: bson.M{
			"$push": bson.M{
				"profile.student.applied_courses": course,
			},
		},
		ReturnNew: true,
	}, result)
}
