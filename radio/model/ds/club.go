package ds

import "gopkg.in/mgo.v2/bson"

type ClubStatus string

const (
	ClubStatusActive   = ClubStatus("active")
	ClubStatusInactive = ClubStatus("inactive")
)

type ClubOnFacebook struct {
	IdPage uint64 `bson:"id_page,omitempty"`
}

type Club struct {
	ID       bson.ObjectId  `bson:"_id,omitempty" json:"id"`
	Name     string         `bson:"name,omitempty"`
	Status   ClubStatus     `bson:"status,omitempty"`
	Link     string         `bson:"link,omitempty"`
	Address  string         `bson:"address,omitempty"`
	Place    Place          `bson:"place,omitempty"`
	Facebook ClubOnFacebook `bson:"facebook,omitempty"`
}
