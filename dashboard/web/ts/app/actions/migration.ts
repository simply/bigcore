/// <reference path="../../typings/tsd.d.ts"/>

export const MIGRATION_START         = "MIGRATION_START"
export const MIGRATION_START_REQUEST = "MIGRATION_START_REQUEST"
export const MIGRATION_RECIEVE_START = "MIGRATION_RECIEVE_START"
export const MIGRATION_PROGRESS      = "MIGRATION_PROGRESS"
export const MIGRATION_FINISH        = "MIGRATION_FINISH"

import * as redux from 'redux'
import * as migration from '../../dal/client/migration'

export function migrationStart(migrationClient: migration.Client): any {
    return (dispatch: redux.Dispatch, getStore: Function): any => {
        dispatch(migrationStartRequest())

        migrationClient.onProgress(onMigrationProgress(dispatch))

        var request: migration.StartRequest = {
            Start: true
        }

        migrationClient.start(request).then(onMigrationStart(dispatch))

        return
    }
}

function onMigrationProgress(dispatch: redux.Dispatch): migration.ProgressResolve {
    return (response: migration.ProgressResponse): void => {
        dispatch(migrationProgress(response))
    }
}

function onMigrationStart(dispatch: redux.Dispatch): migration.StartResolve {
    return (response: migration.StartResponse): void => {
        dispatch(migrationRecieveStart())
    }
}

export function migrationStartRequest(): any {
    return {
        type: MIGRATION_START_REQUEST
    }
}

function migrationRecieveStart(): any {
    return {
        type: MIGRATION_RECIEVE_START
    }
}

export function migrationProgress(progress: migration.ProgressResponse): any {
    return {
        type:     MIGRATION_PROGRESS,
        payload: progress
    }
}

export function migrationFinish(text: string): any {
    return {
        type: MIGRATION_FINISH,
        payload: text
    }
}
