package event

import (
	"bigcore/radio/model/ds"
	"context"
	"gopkg.in/mgo.v2/bson"
)

type Model struct {
	collection *collection
}

func New(ctx context.Context) *Model {
	return &Model{
		collection: newCollection(ctx),
	}
}

func (this *Model) EnsureIndices() error {
	return this.collection.EnsureIndices()
}

func (this *Model) find(scopes ...interface{}) *query {
	return &query{
		Query: this.collection.Find(scopes...),
	}
}

func (this *Model) UpdateFromFacebook(events []*ds.Event) error {
	for _, item := range events {
		exists, err := this.facebookIdExists(item.IdEventOnFacebook)
		if err != nil {
			return err
		}

		if exists {
			this.updateByFacebookId(item.IdEventOnFacebook, item)
			if err != nil {
				return err
			}
		} else {
			err = this.create(item)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (this *Model) facebookIdExists(idEventOnFacebook string) (bool, error) {
	n, err := this.find(byFbId(idEventOnFacebook)).Count()
	return n > 0, err
}

func (this *Model) create(event *ds.Event) error {
	event.ID = bson.NewObjectId()
	return this.collection.Insert(event)
}

func (this *Model) updateByFacebookId(idEventOnFacebook string, event *ds.Event) error {
	_, err := this.find(byFbId(idEventOnFacebook)).applyFacebookId(event)
	return err
}

func (this *Model) FindAll(point ds.Point) ([]ds.Event, error) {
	result := make([]ds.Event, 0, 0)
	//TODO: check if Place.Street is empty, then fetch Club and get place from there

	//TODO: filter finished
	//err := this.Find(near(point), activeEvent).All(&result)
	err := this.find().All(&result)
	return result, err
}
