package event

import (
	"time"

	"bigcore/radio/model/ds"
	"bigcore/radio/model/event/facebook"

	"context"
	"github.com/pkg/errors"
)

type FetcherModel struct {
	ctx      context.Context
	eventDao *facebook.EventDao
}

func NewEventFetcherModel(ctx context.Context) *FetcherModel {
	model := &FetcherModel{
		ctx:      ctx,
		eventDao: facebook.NewEventDao(ctx),
	}

	return model
}

func (this *FetcherModel) ParseTime(timeString string) (time.Time, error) {
	parsed, err := time.Parse("2006-01-02T15:04:05+0700", timeString)
	if err != nil {
		return time.Now(), errors.Wrap(err, "")
	}
	return parsed, nil
}

func (this *FetcherModel) FetchAll(pageId string) ([]ds.FbEvent, error) {
	list, err := this.eventDao.List(pageId)
	if err != nil {
		return nil, err
	}

	return list.Data, nil

	//TODO: this look buggy, fix it
	result := make([]ds.FbEvent, 0)
	for _, item := range list.Data {
		if item.EndTime.IsZero() {
			continue
		}

		if item.EndTime.After(time.Now()) {
			result = append(result, item)
		}
	}

	return result, nil
}
