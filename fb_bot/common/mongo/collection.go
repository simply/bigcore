package mongo

import (
	"runtime"

	"bigcore/fb_bot/config"
	"github.com/pkg/errors"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Collection struct {
	*mgo.Collection
	dbName         string
	collectionName string
}

func NewCollection(collectionName string) *Collection {
	instance := &Collection{
		dbName:         config.Get().Mongo.DbName,
		collectionName: collectionName,
	}

	defaultSession, closer := Get()
	runtime.SetFinalizer(defaultSession, closer)

	instance.Collection = defaultSession.DB(instance.dbName).C(collectionName)

	return instance
}

func (this *Collection) c() (*mgo.Collection, func()) {
	session, closer := Get()
	return session.DB(this.dbName).C(this.collectionName), func() {
		closer(session)
	}
}

func (this *Collection) Find(scopes ...interface{}) *Query {
	query := bson.M{}
	for _, scope := range scopes {
		if scope == nil {
			continue
		}

		if sc, ok := scope.(bson.M); ok {
			for k, v := range sc {
				query[k] = v
			}
		} else {
			panic(errors.Errorf("can't cast to bson.M: %#v", scope))
		}
	}
	collection, closer := this.c()
	return &Query{
		Query:  collection.Find(query),
		closer: closer,
	}
}

func (this *Collection) FindId(id interface{}) *Query {
	collection, closer := this.c()
	return &Query{
		Query:  collection.FindId(id),
		closer: closer,
	}
}

func (this *Collection) EnsureIndex(index mgo.Index) error {
	return wrap(this.Collection.EnsureIndex(index))
}

func (this *Collection) Insert(docs ...interface{}) error {
	collection, closer := this.c()
	defer closer()

	return wrap(collection.Insert(docs...))
}

func (this *Collection) Update(selector interface{}, update interface{}) error {
	collection, closer := this.c()
	defer closer()
	return wrap(collection.Update(selector, update))
}

func (this *Collection) UpdateId(id interface{}, update interface{}) error {
	collection, closer := this.c()
	defer closer()
	return wrap(collection.UpdateId(id, update))
}

func (this *Collection) UpdateAll(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	collection, closer := this.c()
	defer closer()
	info, err = collection.UpdateAll(selector, update)
	return info, wrap(err)
}

func (this *Collection) Upsert(selector interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	collection, closer := this.c()
	defer closer()
	info, err = collection.Upsert(selector, update)
	return info, wrap(err)
}

func (this *Collection) UpsertId(id interface{}, update interface{}) (info *mgo.ChangeInfo, err error) {
	collection, closer := this.c()
	defer closer()
	info, err = collection.UpsertId(id, update)
	return info, wrap(err)
}

func (this *Collection) RemoveId(id interface{}) error {
	collection, closer := this.c()
	defer closer()
	return wrap(collection.RemoveId(id))
}

func (this *Collection) Remove(selector interface{}) error {
	collection, closer := this.c()
	defer closer()
	return wrap(collection.Remove(selector))
}

func (this *Collection) RemoveAll(selector interface{}) (info *mgo.ChangeInfo, err error) {
	collection, closer := this.c()
	defer closer()
	info, err = collection.RemoveAll(selector)
	return info, wrap(err)
}

func (this *Collection) DropCollection() error {
	collection, closer := this.c()
	defer closer()
	return wrap(collection.DropCollection())
}
