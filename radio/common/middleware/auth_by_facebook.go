package middleware

import (
	"bigcore/radio/model/ds"

	"bigcore/radio/model/user"

)

type IUserIdentityByFacebook interface {
	FindByFacebookId(fbId uint64) (*ds.User, error)
	SetOAuth(idUser string, data ds.OAuthFacebook) error
	Register(dataFromClient user.AuthByFacebookDataFromClient) (*ds.User, error)
	Login(dataFromClient user.AuthByFacebookDataFromClient, user *ds.User) error
}

type (
	// BasicAuthConfig defines the config for HTTP basic auth middleware.
	AuthConfig struct {
		// Validator is a function to validate basic auth credentials.
		Validator AuthValidator
	}

	// AuthValidator defines a function to validate basic auth credentials.
	AuthValidator func(user.AuthByFacebookDataFromClient) (*ds.User, error)
)

const (
	barer = "Barer"
)
