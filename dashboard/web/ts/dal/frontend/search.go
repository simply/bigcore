package frontend

type SearchRequest struct {
	Query string `json:"query,omitempty"`
}
