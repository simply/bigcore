// @flow

import Login from './login'
import type { State as LoginState } from './login/reducer'
import App from './app'
import Events from './events'
import Club from './club'
import Teacher from './teacher'
import type { State as TeacherState } from './teacher/reducer'
import Course from './course'
import Geo from './geo'

//in this section keep importing your modules
export type State = {
    Login:LoginState,
    Teacher:TeacherState,
}

//and exporting them here
export {
    Login,
    App,
    Events,
    Course,
    Club,
    Teacher,
    Geo,
}