import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as fbActions from './fb_actions'
import {LoginButton} from 'react-native-fbsdk'

const mapState   = state => ({
    authenticated: state.Login.authenticated,
    user         : state.Login.user,
    facebook     : state.Login.facebook,
})
const mapActions = dispatch => ({
    onLogin : (error: Error, data) => dispatch(fbActions.onLogin(error, data)),
    onLogout: () => dispatch(fbActions.onLogout()),
})

@connect(mapState, mapActions)
export default class Login extends Component {
    static displayName = 'Login'
    static propTypes   = {
        onLogin : React.PropTypes.func.isRequired,
        onLogout: React.PropTypes.func.isRequired,
    }

    render() {
        const {onLogin, onLogout} = this.props

        const fbPropperties = {
            readPermissions     : ['email', 'user_friends'],
            loginBehaviorIOS    : 'native',
            loginBehaviorAndroid: 'native_with_fallback',
            onLoginFinished     : onLogin,
            onLogoutFinished    : onLogout,
        }
        return <LoginButton {...fbPropperties} />
    }
}