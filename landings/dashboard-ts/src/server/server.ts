import * as jspb from "google-protobuf";
import { BrowserHeaders, grpc, Code } from "grpc-web-client";
import Config from "../config";

function callServer<TReq extends jspb.Message, TRes extends jspb.Message, M extends grpc.MethodDefinition<TReq, TRes>>(
    methodDescriptor: M, request: TReq
): Promise<TRes> {
    return _call<TReq, TRes, M>(methodDescriptor, request);
}

function _call<TReq extends jspb.Message, TRes extends jspb.Message, M extends grpc.MethodDefinition<TReq, TRes>>(
    methodDescriptor: M, request: TReq
): Promise<TRes> {
    return new Promise<TRes>(function (resolve, reject) {
        grpc.invoke(methodDescriptor, {
            request: request,
            host: Config.serverUrl,
            onMessage: (message: TRes) => {
                resolve(message);
            },
            onEnd: (code: Code, msg: string | undefined, trailers: BrowserHeaders) => {
                if (code !== Code.OK) {
                    let err = code + " ";
                    if (msg !== undefined) {
                        err += msg;
                    }

                    reject(err + trailers);
                }
            }

        });
    });

}

export default callServer
