package mongo_test

import (
	"bigcore/todo/db/mongo"
	"context"
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	. "github.com/stretchr/testify/assert"
)

// ------------------ Unit tests ----------------------------------------------

func IsEmpty(t *testing.T, errorCh <-chan error) {
	select {
	case err := <-errorCh:
		Failf(t, "Channel must be empty", "Received value %+v", err)
	default:
		return
	}
}

func WaitFor(t *testing.T, errorCh <-chan error, expectedErr error) {
	timeout := 100 * time.Millisecond
	select {
	case err := <-errorCh:
		Equal(t, err, expectedErr)
	case <-time.After(timeout):
		Failf(t, "Timeout", "Expected error: %s", expectedErr)
	}
}

func IsClosed(t *testing.T, ch <-chan error) {
	if _, ok := <-ch; ok {
		Fail(t, "Internal channel must be closed")
	}
}

var testCases = []struct {
	name      string
	pingValue error
	openValue error
}{
	{
		name:      "positive",
		pingValue: nil,
		openValue: nil,
	},
	{
		name:      "ping failed",
		pingValue: errors.New("timeout"),
		openValue: nil,
	},
	{
		name:      "ping failed, open failed",
		pingValue: errors.New("timeout"),
		openValue: errors.New("Can't open"),
	},
}

func TestExample(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	heartBeat := make(chan error)
	opener := NewMockOpenCloser(mockCtrl)
	ctx := context.Background()

	// first open
	reopenCh := mongo.AutoReopen(ctx, opener, heartBeat)

	for i := 0; i < 5; i++ { // ensure that internal state is correct after few disconnect
		for _, cc := range testCases {
			c := cc
			t.Run(cc.name, func(t *testing.T) {
				if c.pingValue != nil {
					opener.EXPECT().Close(ctx).Times(1)
					opener.EXPECT().Open(ctx).Times(1).Return(c.openValue)
				}

				heartBeat <- c.pingValue
				WaitFor(t, reopenCh, c.pingValue)

				if c.pingValue != nil {
					WaitFor(t, reopenCh, c.openValue)
				}
				IsEmpty(t, reopenCh)
			})
		}
	}

	// close in defer
	opener.EXPECT().Close(ctx).Times(1)
	close(heartBeat)
	IsClosed(t, reopenCh)
}
