import React from 'react'
import {StyleSheet, View, Text, ListView} from 'react-native'
import {Card, TextButton} from '../../components'
import {connect} from 'react-redux'
import * as actions from './actions'

//TODO: Use ListView with refresh
const styles = StyleSheet.create({
    data       : {
        flexDirection: 'row',
    },
    date       : {
        width: 70,
    },
    day        : {
        textAlign : 'center',
        fontSize  : 18,
        fontWeight: 'bold',
    },
    month      : {
        textAlign : 'center',
        fontSize  : 16,
        fontWeight: 'bold',
    },
    time       : {
        textAlign : 'center',
        fontSize  : 16,
        fontWeight: 'bold',
    },
    details    : {
        flex        : 1,
        paddingRight: 6,
    },
    title      : {},
    description: {
        fontSize   : 10,
        paddingTop : 15,
        paddingLeft: 15,
    },
    address    : {
        fontSize: 10,
        color   : 'rgba(0,0,0,0.4)',
    },
    listView   : {
        paddingTop     : 20,
        backgroundColor: '#F5FCFF',
    },
    cover      : {
        marginTop: 5,
        width    : 400,
        height   : 200,
    },
})

const ApplyBtn = ({onPress, id, applyInProgress}) =>
    <TextButton icon="heart-o" text="Get Course"
                onPress={() => onPress(id)} showLoader={applyInProgress}/>

ApplyBtn.propTypes = {
    id             : React.PropTypes.string.isRequired,
    applyInProgress: React.PropTypes.bool.isRequired,
    onPress        : React.PropTypes.func.isRequired,
}

const ListItem = ({course, appliedCourses, onPress, applyInProgress}) => {
    const canApply = appliedCourses.reduce((acc, it) => acc || course.id == it, true)

    return (
        <Card key={`course-${course.id}`}>
            <View style={styles.data}>
                <Text style={styles.day}>{course.name}</Text>
                {canApply ?
                    <ApplyBtn id={course.id} onPress={onPress} applyInProgress={applyInProgress}/> :
                    <Text>Applied</Text>
                }
            </View>
        </Card>
    )
}

ListItem.propTypes = {
    course         : React.PropTypes.any.isRequired,
    applyInProgress: React.PropTypes.bool.isRequired,
    onPress        : React.PropTypes.func.isRequired,
    appliedCourses : React.PropTypes.arrayOf(React.PropTypes.number).isRequired,
}

//TODO: applyInProgress is global, must dedicated to course
const mapState   = state => ({
    dataSource       : state.Course.dataSource,
    applyInProgressId: state.Course.applyInProgressId,
    appliedCourses   : state.Login.user.Profile.Student.AppliedCourses.map(it => it.Id),
})
const mapActions = dispatch => ({
    refresh    : () => dispatch(actions.refresh()),
})

@connect(mapState, mapActions)
export default class List extends React.Component {
    static propTypes = {
        dataSource       : React.PropTypes.instanceOf(ListView.DataSource).isRequired,
        onPressItem      : React.PropTypes.func.isRequired,
        applyInProgressId: React.PropTypes.string.isRequired,
        appliedCourses   : React.PropTypes.arrayOf(React.PropTypes.number).isRequired,
    }

    render() {
        const {dataSource, appliedCourses, onPressItem, applyInProgressId} = this.props
        return (
            <ListView
                dataSource={dataSource}
                renderRow={(course) =>
                    <ListItem course={course}
                        appliedCourses={appliedCourses} onPress={onPressItem}
                        applyInProgress={applyInProgressId==course.id}
                    />}
            />

        )
    }
}

