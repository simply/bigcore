// @flow

import { ListView } from 'react-native'
import { handleActions } from 'redux-actions'
import {
    REFRESH_LIST, REFRESH_LIST_FAILED, BEFORE_REFRESH,
    PRESS_LIST_ITEM, NAVIGATE_TO,
    BECOME_TEACHER_REQUEST, BECOME_TEACHER_SUCCEED, BECOME_TEACHER_FAILED,
    ORDER_INTERVIEW_REQUEST, ORDER_INTERVIEW_SUCCEED, ORDER_INTERVIEW_FAILED,
} from './constants'

export type State = {
    loaded                  : bool,
    becomeTeacherInProgress : bool,
    orderInterviewInProgress: bool,
    currentScreen           : string,
    teachers                : Array<any>,
    detailViewTeacherIndex  : number,
    dataSource              : any,
}

const initialState = {
    loaded                  : false,
    becomeTeacherInProgress : false,
    orderInterviewInProgress: false,
    currentScreen           : 'list',
    teachers                : [],
    detailViewTeacherIndex  : -1,
    dataSource              : new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 }),
}

//you can do better here, I was just showing that you need to make a new copy
//of state. It is ok to deep copy of state. It will prevent unseen bugs in the future
//for better performance you can use immutableJS

//handleActions is a helper function to instead of using a switch case statement,
//we just use the regular map with function state attach to it.

export default handleActions({
    [REFRESH_LIST]           : (state: State, { payload }): State => {
        return {
            ...state,
            loaded    : true,
            teachers  : payload.teachers,
            dataSource: state.dataSource.cloneWithRows(payload.teachers),
        }
    },
    [REFRESH_LIST_FAILED]    : (state: State): State => {
        return state
    },
    [BEFORE_REFRESH]         : (state: State): State => {
        return state
    },
    [BECOME_TEACHER_REQUEST] : (state: State): State => {
        return {
            ...state,
            becomeTeacherInProgress: true,
        }
    },
    [BECOME_TEACHER_SUCCEED] : (state: State): State => {
        return {
            ...state,
            becomeTeacherInProgress: false,
        }
    },
    [BECOME_TEACHER_FAILED]  : (state: State): State => {
        return {
            ...state,
            becomeTeacherInProgress: false,
        }
    },
    [ORDER_INTERVIEW_REQUEST]: (state: State): State => {
        return {
            ...state,
            orderInterviewInProgress: true,
        }
    },
    [ORDER_INTERVIEW_SUCCEED]: (state: State): State => {
        return {
            ...state,
            orderInterviewInProgress: false,
        }
    },
    [ORDER_INTERVIEW_FAILED] : (state: State): State => {
        return {
            ...state,
            orderInterviewInProgress: false,
        }
    },
    [PRESS_LIST_ITEM]        : (state: State, { idTeacher }): State => {
        return {
            ...state,
            detailViewTeacherIndex: state.teachers.findIndex((teacher) => teacher.id == idTeacher),
        }
    },
    [NAVIGATE_TO]            : (state: State, { payload }): State => {
        return {
            ...state,
            currentScreen: payload.routeName,
        }
    },
}, initialState)