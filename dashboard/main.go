package main

func main() {

	appInstance := NewApp()
	err := appInstance.Init()
	if err != nil {
		panic(err)
	}
	err = appInstance.Run()
	if err != nil {
		panic(err)
	}
}
