package event

import (
	"bigcore/radio/common/db"
	"bigcore/radio/model/ds"

	"time"

	"context"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type collection struct {
	*db.Collection
}

func newCollection(ctx context.Context) *collection {
	return &collection{
		Collection: db.NewCollection(ctx, "event"),
	}
}

func (this *collection) EnsureIndices() error {
	var err error

	err = this.EnsureIndex(mgo.Index{
		Key:        []string{"id_event"},
		Unique:     true,
		Background: true,
	})
	if err != nil {
		return err
	}

	err = this.EnsureIndex(mgo.Index{
		Key:        []string{"$2dsphere:place.point"},
		Background: true,
	})
	if err != nil {
		return err
	}

	err = this.EnsureIndex(mgo.Index{
		Key:        []string{"end_time", "start_time"},
		Background: true,
	})
	if err != nil {
		return err
	}

	return nil
}

type query struct {
	*db.Query
}

func near(point ds.Point) bson.M {
	return bson.M{
		"place.point": bson.M{
			"$nearSphere": bson.M{
				"$geometry": point.ToMap(),
			},
		},
	}
}

var active = bson.M{"status": ds.EventStatusActive}

var notFinished = bson.M{
	"end_time": bson.M{
		"$gte": time.Now(),
	},
}

func byFbId(idEventOnFacebook string) bson.M {
	return bson.M{"id_event_on_facebook": idEventOnFacebook}
}

func (this *query) applyFacebookId(event *ds.Event) (*mgo.ChangeInfo, error) {
	return this.Apply(mgo.Change{Update: bson.M{"$set": event}}, nil)
}
