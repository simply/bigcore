package user

import (
	"bigcore/radio/model/ds"
	"gopkg.in/mgo.v2/bson"
)

func byId(userId string) bson.M {
	return bson.M{"_id": bson.ObjectIdHex(userId)}
}

func byEmail(email string) bson.M {
	return bson.M{"email": email}
}

func byFbId(userIdOnFacebook uint64) bson.M {
	return bson.M{
		"o_auth.facebook.id_user_on_facebook": userIdOnFacebook,
	}
}

var notTeacher = bson.M{
	"type": bson.M{"$nin": []ds.UserType{ds.USER_TYPE_TEACHER}},
}

var activeTeacher = bson.M{
	"type": ds.USER_TYPE_TEACHER,
	"profile.teacher.status": ds.TEACHER_STATUS_ACTIVE,
}
