package club

import (
	"bigcore/radio/common/db"
	"bigcore/radio/model/ds"

	"context"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type collection struct {
	*db.Collection
}

func newCollection(ctx context.Context) *collection {
	return &collection{
		Collection: db.NewCollection(ctx, "club"),
	}
}

func (this *collection) EnsureIndices() error {
	var err error

	if err != nil {
		return err
	}

	err = this.EnsureIndex(mgo.Index{
		Key:        []string{"status"},
		Background: true,
	})
	if err != nil {
		return err
	}

	return nil
}

type query struct {
	*db.Query
}

var active = bson.M{"status": ds.ClubStatusActive}

func byFbPage(pageId string) bson.M {
	return bson.M{"facebook.id_page": pageId}
}

func near(point ds.Point) bson.M {
	return bson.M{
		"place.point": bson.M{
			"$nearSphere": bson.M{
				"$geometry": point.ToMap(),
			},
		},
	}
}

func byId(id string) bson.M {
	return bson.M{"_id": bson.ObjectIdHex(id)}
}
