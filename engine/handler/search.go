package handler

import (
	"bigcore/engine/proto/engine"

	"strconv"

	"context"

	"github.com/dropbox/godropbox/errors"
	"gopkg.in/gorp.v2"
	elastic "gopkg.in/olivere/elastic.v6"
)

type SearchService struct {
	db      *gorp.DbMap
	elastic *elastic.Client
}

func NewSearchService(db *gorp.DbMap, es *elastic.Client) *SearchService {
	return &SearchService{
		db:      db,
		elastic: es,
	}
}

func (this *SearchService) Search(ctx context.Context, request *engine.SearchRequest) (*engine.SearchReply, error) {

	termQuery := elastic.NewTermQuery("title", request.Query)
	scoreSorter := elastic.NewScoreSort()
	search := this.elastic.Search("test").
		Fields().
		Query(termQuery).
		SortBy(scoreSorter)

	r, err := search.Do()
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	if r.Error != nil {
		return nil, errors.New(r.Error.Reason)
	}

	reply := &engine.SearchReply{
		Ids: make([]int64, len(r.Hits.Hits), len(r.Hits.Hits)),
	}
	for i, doc := range r.Hits.Hits {
		reply.Ids[i], err = strconv.ParseInt(doc.Id, 10, 0)
		if err != nil {
			return nil, errors.Wrap(err, "")
		}
	}

	return reply, nil
}
