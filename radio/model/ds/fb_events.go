package ds

import (
	"time"

	"strings"

	"bigcore/radio/config"

	"github.com/pkg/errors"
	"gopkg.in/mgo.v2/bson"
)

type FbEventsResponse struct {
	Data []FbEvent
}

type FbEvent struct {
	Id          string
	Name        string
	Description string
	StartTime   FacebookTime //TODO: look like i need to fix timezone https://docs.mongodb.com/manual/tutorial/model-time-data/
	EndTime     FacebookTime
	Cover       FbCover
	Type        string
	Place       struct {
		Name     string
		Location struct {
			Street    string
			City      string
			Country   string
			Longitude config.Longitude
			Latitude  config.Latitude
		}
	}
}

type FbCover struct {
	Id      string
	OffsetX int
	OffsetY int
	Source  string
}

type FacebookTime struct {
	time.Time
}

type FacebookTimeFormat string

const FACEBOOK_TIME_FORMAT = FacebookTimeFormat("2006-01-02T15:04:05Z0700")

// UnmarshalJSON sets *m to a copy of data.
func (this *FacebookTime) UnmarshalJSON(b []byte) error {
	var err error
	str := strings.Trim(string(b), `"`)
	if str == "" {
		return nil
	}

	this.Time, err = ParseFacebookTime(FACEBOOK_TIME_FORMAT, str)
	return err
}

func ParseFacebookTime(format FacebookTimeFormat, str string) (time.Time, error) {
	timer, err := time.Parse(string(format), str)
	if err != nil {
		return time.Time{}, errors.Wrapf(err, "original string: `%s`", str)
	}

	return timer, nil
}

type EventStatus uint8

const (
	EventStatusActive   = EventStatus(1)
	EventStatusInactive = EventStatus(0)
)

type Event struct {
	ID                bson.ObjectId `bson:"_id,omitempty"`
	IdEventOnFacebook string        `bson:"id_event_on_facebook,omitempty"`
	FkPage            string        `bson:"fk_page,omitempty"`
	Name              string        `bson:"name,omitempty"`
	Cover             string        `bson:"cover,omitempty"`
	Description       string        `bson:"description,omitempty"`
	Status            EventStatus   `bson:"status,omitempty"`
	EndTime           time.Time     `bson:"end_time,omitempty"`
	StartTime         time.Time     `bson:"start_time,omitempty"`
	Place             Place         `bson:"place,omitempty"`
}

//TODO: link event with club
func (this *Event) FromFbEvent(fbEvent FbEvent) {
	this.IdEventOnFacebook = fbEvent.Id
	this.Name = fbEvent.Name
	this.Description = fbEvent.Description
	this.StartTime = fbEvent.StartTime.Time
	this.EndTime = fbEvent.EndTime.Time
	this.Cover = fbEvent.Cover.Source
	this.Place.Name = fbEvent.Place.Name
	this.Place.City = fbEvent.Place.Location.City
	this.Place.Street = fbEvent.Place.Location.Street
	this.Place.Point = NewPoint(fbEvent.Place.Location.Longitude, fbEvent.Place.Location.Latitude)
}
