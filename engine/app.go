package main

import (
	"google.golang.org/grpc"
	"log"

	"bigcore/engine/config"
	"bigcore/engine/handler"

	//	"github.com/olivere/elastic"
	"encoding/json"
	"fmt"
	"net"

	"bigcore/engine/proto/engine"
	"database/sql"
	"github.com/dropbox/godropbox/errors"
	_ "github.com/lib/pq"
	"gopkg.in/gorp.v2"
	"gopkg.in/olivere/elastic.v6"

	"context"
	"sync"
)

type App struct {
	Config  *config.Config
	Db      *gorp.DbMap
	Elastic *elastic.Client
}

func NewApp() *App {
	return &App{}
}

func (this *App) Init() error {
	var err error

	err = this.InitConfig()
	if err != nil {
		return err
	}

	err = this.InitEs()
	if err != nil {
		return err
	}

	err = this.InitAs()
	if err != nil {
		return err
	}

	err = this.InitDb()
	if err != nil {
		return err
	}
	return nil
}

func (this *App) InitConfig() error {
	var err error
	this.Config, err = config.NewConf()
	if err != nil {
		return err
	}

	return nil
}

type Source struct {
	Id int64
}
type Simple struct {
	Id      int64
	Sources []Source
}
type Config struct {
	Id      int64
	Simples []Simple
}

func (this *App) InitAs() error {
	if err != nil {
		return err
	}
	this.As = as

	udf := []byte(`
	local function map_record(rec)
      -- Add name and age to returned map.
      -- Could add other record bins here as well.
      -- This code shows different data access to record bins

      out = map {}
	  bins = record.bin_names(rec)
	  for i, bin_name in ipairs(bins) do
		out[bin_name] = rec[bin_name]
	  end
	  warn("%s", out)

      return out
    end

	function my_stream_udf(stream)
	  return stream : map(map_record)
	end`)
	err = <-t.OnComplete()
	if err != nil {
		return err
	}

	// setup statement

	recordset, err := this.As.Query(nil, stm)
	if err != nil {
		return err
	}

	for rec := range recordset.Records {
		fmt.Println(rec.Bins)
	}
	fmt.Println("Success!")

	for i := int64(0); i < 1000*1000*100; i++ {
		if err != nil {
			return err
		}
		simples := make([]Simple, 2, 2)
		simples[0] = Simple{
			Id:      i,
			Sources: make([]Source, 2, 2),
		}
		simples[0].Sources[0] = Source{
			Id: i,
		}
		simples[0].Sources[1] = Source{
			Id: i,
		}
		simples[1] = Simple{
			Id:      i,
			Sources: make([]Source, 2, 2),
		}
		simples[1].Sources[0] = Source{
			Id: i,
		}
		simples[1].Sources[1] = Source{
			Id: i,
		}

		err = this.As.Put(this.As.DefaultWritePolicy, key, data)
		if err != nil {
			return err
		}
	}

	return nil
}
func (this *App) InitEs() error {
	nodes := make([]string, len(this.Config.Elastic.Pool), len(this.Config.Elastic.Pool))
	for i, node := range this.Config.Elastic.Pool {
		nodes[i] = fmt.Sprintf("http://%s:%d", node.Host, node.Port)
	}

	client, err := elastic.NewClient(
		elastic.SetURL(nodes...),
		//		elastic.SetErrorLog(this.StdLogger),
		elastic.SetSniff(false),
	)
	if err != nil {
		return err
	}

	p, _ := client.BulkProcessor().Do()
	for i := 0; i < 100000; i++ {
		p.Add(elastic.NewBulkIndexRequest().Doc(map[string]string{"name": "alex"}))
	}

	this.Elastic = client

	return nil
}

func (this *App) InitDb() error {
	dbConfig := this.Config.Db.Write[0]
	dsl := fmt.Sprintf("postgres://%s:%s@%s:%d?sslmode=disable", dbConfig.User, dbConfig.Pass, dbConfig.Host, dbConfig.Port)
	db, err := sql.Open("postgres", dsl)
	if err != nil {
		return errors.New("Error: The data source arguments are not valid")
	}

	err = db.Ping()
	if err != nil {
		return errors.Wrap(err, "")
	}

	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.PostgresDialect{}}

	this.Db = dbmap

	this.Db.Db.SetMaxIdleConns(250)
	this.Db.Db.SetMaxOpenConns(250)

	return nil
}

type JsonCodec struct{}

// json is a Codec. It is the default codec for gRPC.
func (JsonCodec) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (JsonCodec) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (JsonCodec) String() string {
	return "json"
}

func (this *App) Run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	bind := fmt.Sprintf("%s:%d", this.Config.Rpc.Host, this.Config.Rpc.Port)
	lis, err := net.Listen("tcp", bind)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	opts := []grpc.ServerOption{
	//grpc.CustomCodec(JsonCodec{}),
	}

	grpcServer := grpc.NewServer(opts...)
	engine.RegisterMigrationServer(grpcServer, handler.NewMigration())
	engine.RegisterSearchServer(grpcServer, handler.NewSearchService(this.Db, this.Elastic))
	engine.RegisterProductServiceServer(grpcServer, handler.NewProductService(this.Db, this.Elastic, this.As))

	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	return nil
}

func (this *App) Close() {
	log.Println("Bye!")
}
