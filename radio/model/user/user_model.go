package user

import (
	"bigcore/radio/common"
	"bigcore/radio/common/db"
	"bigcore/radio/model/ds"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type collection struct {
	*db.Collection
}

func NewCollection(dbConnection *mgo.Database) *collection {
	return &collection{
		Collection: db.NewCollection2(dbConnection, "user"),
	}
}

func (this *collection) ensureIndices() error {
	err := this.EnsureIndex(mgo.Index{
		Key:        []string{"type"},
		Background: true,
	})
	if err != nil {
		return err
	}

	err = this.EnsureIndex(mgo.Index{
		Key:        []string{"status"},
		Background: true,
	})
	if err != nil {
		return err
	}

	return nil
}

type Model struct {
	collection *collection
}

//func New(ctx context.Context) *Model {
//	return &Model{
//		collection: newCollection(ctx),
//	}
//}

func (this *Model) find(scopes ...interface{}) *query {
	return &query{
		Query: this.collection.Find(scopes...),
	}
}
func (this *Model) EnsureIndices() {
	this.collection.Find(byEmail("www.pismeco@gmail.com")).Apply(mgo.Change{Update: bson.M{"$pull": bson.M{"type": ds.USER_TYPE_TEACHER}}}, nil)
	common.Must(this.collection.ensureIndices())
}

func (this *Model) FindById(userId string) (*ds.User, error) {
	instance := &ds.User{}
	err := this.find(byId(userId)).One(instance)
	return instance, err
}

func (this *Model) OrderInterviewWithTeacher(idUser string, idTeacher string) (*ds.User, error) {
	newUser := &ds.User{}
	_, err := this.find(byId(idUser)).applyOrderInterviewWithTeacher(idTeacher, newUser)
	return newUser, err
}

func (this *Model) OrderInterviewWithClub(idUser string, idClub string) (*ds.User, error) {
	newUser := &ds.User{}
	_, err := this.find(byId(idUser)).applyOrderInterviewInClub(idClub, newUser)
	return newUser, err
}

func (this *Model) SubscribeToCourse(idUser string, course *ds.AppliedCourse) ([]ds.AppliedCourse, error) {
	newUser := &ds.User{}
	_, err := this.find(byId(idUser)).applySubscribeToCourse(course, newUser)
	return newUser.Profile.Student.AppliedCourses, err
}
