import { WS_CONNECT_START, WS_CONNECTED, WS_DISCONNECTED } from '../actions/ws'

import * as migration from '../../dal/client/migration'
import * as search from '../../dal/client/search'

export class State {
    migrationClient: migration.Client
    searchClient: search.Client
}

export function Reducer(state: State = new State, action: any = false): State {
    switch (action.type) {
        case WS_CONNECT_START:
            return state
        case WS_CONNECTED:
            return Object.assign(new State, state, {
                migrationClient: action.payload.migrationClient,
                searchClient:    action.payload.searchClient
            })
        case WS_DISCONNECTED:
            return state
        default:
            return state
    }
}
