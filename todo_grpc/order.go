package todo

import (
	"bigcore/todo/db"
	"errors"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"context"
)

type orderServiceType struct{}

var OrderService = &orderServiceType{}

type ValidationError string

func (e ValidationError) Error() string { return string(e) }

func IsValidationError(err error) bool {
	switch err.(type) {
	case ValidationError:
		return true
	}
	return false
}

type permitionErrors error

var (
	msgUserIdRequired = permitionErrors(errors.New("user_id can't be empty"))
	msgUserNotExists  = permitionErrors(errors.New("requested user doesn't exists"))
)

func (this *OrderListRequest) Validate(ctx context.Context, user IUser, order IOrder) error {
	if this.UserId == "" {
		return msgUserIdRequired
	}

	{
		ok, err := user.Exists(ctx, this.UserId)
		if err != nil {
			return err
		}

		if !ok {
			return msgUserNotExists
		}
	}

	return nil
}

func (*orderServiceType) Create(ctx context.Context, request *OrderCreateRequest) (*OrderCreateReply, error) {
	reply := &OrderCreateReply{}
	return reply, nil
}

func (*orderServiceType) List(ctx context.Context, request *OrderListRequest) (*OrderListReply, error) {
	if err := request.Validate(ctx, db.User, db.Order); err != nil {
		if castedErr, ok := err.(permitionErrors); ok {
			return nil, status.New(codes.PermissionDenied, castedErr.Error()).Err()
		}

		return nil, err
	}

	reply := &OrderListReply{}
	reply.Orders = make([]*Order, 1, 1)
	reply.Orders[0] = &Order{
		Id:     "s",
		FkUser: "d",
	}

	for _, order := range getFakeOrders() {
		err := db.Order.Create(ctx, order)
		if err != nil {
			return nil, err
		}
	}
	return reply, nil
}

func getFakeOrders() []*db.OrderType {
	return []*db.OrderType{{Price: 1}, {Price: 2}}
}
