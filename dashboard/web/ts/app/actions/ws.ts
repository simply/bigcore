/// <reference path="../../typings/tsd.d.ts"/>

export const WS_CONNECT_START              = "WS_CONNECT_START"
export const WS_CONNECTED    = "WS_CONNECTED"
export const WS_DISCONNECTED = "WS_DISCONNECTED"

import * as migration from '../../dal/client/migration'
import * as search from '../../dal/client/search'
import * as searchActions from '../../search/actions/search'
import * as root from '../reducers/root'

import * as redux from 'redux'

export function Connect(): any {
    return (dispatch: redux.Dispatch, getStore: Function): any => {
        dispatch(wsConnectStart())
        dispatch(wsConnected())
    }
}

export function wsConnectStart(): any {
    return {
        type: WS_CONNECT_START,
        payload: {}
    }
}

export function wsConnected(): any {

    return {
        type: WS_CONNECTED,
        payload: {
            migrationClient: new migration.Client(),
            searchClient: new search.Client()
        }
    }
}

function wsDisconnected(reason: string, details: any): any {
    console.log("WS Connection lost: ", reason, details)

    return {
        type: WS_DISCONNECTED,
        payload: {
            migrationClient: null
        }
    }
}
