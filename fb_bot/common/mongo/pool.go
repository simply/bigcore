package mongo

import (
	"sync"

	"github.com/dropbox/godropbox/errors"
	"gopkg.in/mgo.v2"
)

func wrap(err error) errors.DropboxError {
	if err != nil {
		return errors.Wrap(err, "")
	}
	return nil
}
