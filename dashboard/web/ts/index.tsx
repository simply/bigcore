/// <reference path="typings/tsd.d.ts"/>

import * as React from 'react'
import * as ReactDOM from 'react-dom'

import { createStore, applyMiddleware, Store, compose, combineReducers } from 'redux'
import { Provider, connect } from 'react-redux'
import thunk from 'redux-thunk'

import App from './app/containers/app'
import * as migration from './dal/client/migration'
import AppReducer from "./app/reducers/root"

// Redux DevTools store enhancers
//import { devTools, persistState } from 'redux-devtools'
// React components for Redux DevTools
//import { DevTools, DebugPanel, LogMonitor } from 'redux-devtools/lib/react'
import * as ws from "./app/actions/ws"

const finalCreateStore = compose(
    applyMiddleware(thunk)
    // Provides support for DevTools:
    //devTools(),
    // Lets you write ?debug_session=<name> in address bar to persist debug sessions
    //persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
)(createStore)

let store     = finalCreateStore(AppReducer, {})

store.dispatch(ws.Connect())

/*
var rootInstance = ReactDOM.render(
    <div>
        <Provider store={store}>
            <App />
        </Provider>
        <DebugPanel top right bottom>
            <DevTools store={store} monitor={LogMonitor}/>
        </DebugPanel>
    </div>,
    document.getElementById('app')
)
*/
var rootInstance = ReactDOM.render(
    <div>
        <Provider store={store}>
            <App />
        </Provider>
    </div>,
    document.getElementById('app')
)


interface IHotModule {
    hot?: { accept: (path: string, callback: () => void) => void }
}

declare const module: IHotModule

// Then just copy and paste this part at the bottom of
// the file
if (module.hot) {
    module.hot.accept('./app/containers/app', () => {
        console.log('My dependency changed!')
        const nextRootReducer: any = App;
        store.replaceReducer(nextRootReducer)
        console.log('My dependency changed!')
    })
}
