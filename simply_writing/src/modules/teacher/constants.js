// @flow

//name of this modules
export const NAME: string = 'Teacher'

//action types
export const INIT: string                    = `${NAME}/INIT`
export const NAVIGATE_TO: string             = `${NAME}/NAVIGATE_TO`
export const REFRESH_LIST: string            = `${NAME}/REFRESH_LIST`
export const REFRESH_LIST_FAILED: string     = `${NAME}/REFRESH_LIST_FAILED`
export const BEFORE_REFRESH: string          = `${NAME}/BEFORE_REFRESH`
export const PRESS_LIST_ITEM: string         = `${NAME}/PRESS_LIST_ITEM`
export const BECOME_TEACHER_REQUEST: string  = `${NAME}/BECOME_TEACHER_REQUEST`
export const BECOME_TEACHER_SUCCEED: string  = `${NAME}/BECOME_TEACHER_SUCCEED`
export const BECOME_TEACHER_FAILED: string   = `${NAME}/BECOME_TEACHER_FAILED`
export const ORDER_INTERVIEW_REQUEST: string = `${NAME}/ORDER_INTERVIEW_REQUEST`
export const ORDER_INTERVIEW_SUCCEED: string = `${NAME}/ORDER_INTERVIEW_SUCCEED`
export const ORDER_INTERVIEW_FAILED: string  = `${NAME}/ORDER_INTERVIEW_FAILED`

//as you can see above, each action is namespaced with module's name.