import React from 'react'
import {StyleSheet, View, Text, Animated} from 'react-native'
import Styles from '../styles'

// facebook colors http://www.color-hex.com/color-palette/185
// Animated way https://github.com/wwayne/react-native-nba-app/blob/master/app/components/player/PlayerLog.js
const styles = StyleSheet.create({
    item : {
        flexDirection: 'column',
        marginBottom : 8,
    },
    title: {
        ...Styles.text.dark,
    },
    bar  : {
        backgroundColor: 'rgb(59,89,152)',
        borderRadius   : 5,
        height         : 8,
    },
})

const Skill = ({width, label}) =>
    <View style={styles.item}>
        <Text style={styles.title}>{label}</Text>
        <Animated.View style={[styles.bar, {width}]}/>
    </View>

Skill.propTypes = {
    width: React.PropTypes.objectOf(React.PropTypes.any).isRequired,
    label: React.PropTypes.string.isRequired,
}

export default Skill