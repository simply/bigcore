package course

import (
	"bigcore/radio/common/db"
	"bigcore/radio/model/ds"

	"context"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type collection struct {
	*db.Collection
}

func (this *collection) ensureIndices() error {
	var err error
	indices := []mgo.Index{
		{
			Key:        []string{"fk_club"},
			Background: true,
		},
		{
			Key:        []string{"fks_teacher"},
			Background: true,
		},
		{
			Key:        []string{"status"},
			Background: true,
		},
	}
	for _, index := range indices {
		err = this.EnsureIndex(index)
		if err != nil {
			return err
		}
	}

	return nil
}

type query struct {
	*db.Query
}

func newCollection(ctx context.Context) *collection {
	return &collection{
		Collection: db.NewCollection(ctx, "course"),
	}
}

var active = bson.M{"status": ds.CourseStatusActive}

func byId(id string) bson.M {
	return bson.M{"_id": bson.ObjectIdHex(id)}
}
