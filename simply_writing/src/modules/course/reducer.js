import {ListView} from 'react-native'
import {handleActions} from 'redux-actions'
import {
    UPDATE_LIST, UPDATE_LIST_FAILED, BEFORE_REFRESH,
    APPLY_TO_COURSE_REQUEST, APPLY_TO_COURSE_SUCCESS, APPLY_TO_COURSE_FAIL,
} from './constants'

const initialState = {
    loaded           : false,
    courses          : [],
    applyInProgressId: '',
    currentScreen    : 'list',
    dataSource       : new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
}

//you can do better here, I was just showing that you need to make a new copy
//of state. It is ok to deep copy of state. It will prevent unseen bugs in the future
//for better performance you can use immutableJS

//handleActions is a helper function to instead of using a switch case statement,
//we just use the regular map with function state attach to it.

export default handleActions({
    [UPDATE_LIST]            : (state, {payload}) => {
        return {
            ...state,
            loaded    : true,
            courses   : payload.courses,
            dataSource: state.dataSource.cloneWithRows(payload.courses),
        }
    },
    [UPDATE_LIST_FAILED]     : (state) => {
        return state
    },
    [BEFORE_REFRESH]         : (state) => {
        return state
    },
    [APPLY_TO_COURSE_REQUEST]: (state, {payload}) => {
        return {
            ...state,
            applyInProgressId: payload.idCourse,
        }
    },
    [APPLY_TO_COURSE_SUCCESS]: (state) => {
        return {
            ...state,
            applyInProgressId: '',
        }
    },
    [APPLY_TO_COURSE_FAIL]   : (state) => {
        return {
            ...state,
            applyInProgressId: '',
        }
    },
}, initialState)

